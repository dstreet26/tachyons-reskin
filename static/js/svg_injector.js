let XMLHttpRequest = window.XMLHttpRequest;

const svgElements = document.querySelectorAll("img.svg");
svgElements.forEach(el => {
  const attributes = [];

  for (let i = 0, atts = el.attributes, n = atts.length; i < n; i++) {
    const att = atts[i];
    attributes.push({
      key: att.nodeName,
      value: att.nodeValue
    });
  }

  const imgUrl = el.getAttribute("src");

  const request = new XMLHttpRequest();
  request.open("GET", imgUrl, true);
  request.onload = () => {
    const svg = request.responseXML.querySelector("svg");

    attributes.forEach(attribute => {
      svg.setAttribute(attribute.key, attribute.value);
    });

    if (el.parentNode) {
      el.parentNode.replaceChild(svg, el);
    }
  };
  request.send();
});
