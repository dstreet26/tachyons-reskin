var canvas = document.getElementById("canvas");
var ctx = canvas.getContext("2d");
var video = document.getElementById("video");

// set canvas size = video size when known
video.addEventListener("loadedmetadata", function() {
  canvas.width = video.videoWidth;
  canvas.height = video.videoHeight;
});

video.addEventListener("play", function() {
  var $this = this; //cache
  (function loop() {
    if (!$this.paused && !$this.ended) {
        // const hey = ctx.getImageData(10,10,10,10)
        // console.log(hey)
      ctx.drawImage($this, 0, 0);
    //   ctx.drawImage()
    ctx.fillStyle = "rgba(255,0,0,.4)";
    ctx.fillRect(20,20,20,80);
      setTimeout(loop, 1000 / 30); // drawing at 30fps
    }
  })();
}, 0);

// setTimeout(function(){
//     document.getElementById('video').play();
// },1000)

// document.addEventListener('DOMContentLoaded', function(){
//     var video = document.getElementById('video');
//     var canvas = document.getElementById('c');
//     var context = canvas.getContext('2d');
//     var back = document.createElement('canvas');
//     var backcontext = back.getContext('2d');

//     var cw,ch;

//     video.addEventListener('play', function(){
//         cw = video.clientWidth;
//         ch = video.clientHeight;
//         canvas.width = cw;
//         canvas.height = ch;
//         back.width = cw;
//         back.height = ch;
//         draw(video,context,backcontext,cw,ch);
//     },false);

// },false);

// function draw(video,context,backcontext,w,h) {
//     if(video.paused || video.ended) return false;
//     // backcontext.crossOrigin = "Anonymous";
//     // backcontext.setAttribute('crossOrigin', '');
//     // First, draw it into the backing canvas
//     backcontext.drawImage(video,0,0,w,h);
//     console.log(backcontext)
//     // Grab the pixel data from the backing canvas
//     var idata = backcontext.getImageData(0,0,w,h);
//     var data = idata.data;
//     // Loop through the pixels, turning them grayscale
//     for(var i = 0; i < data.length; i+=4) {
//         var r = data[i];
//         var g = data[i+1];
//         var b = data[i+2];
//         var brightness = (3*r+4*g+b)>>>3;
//         data[i] = brightness;
//         data[i+1] = brightness;
//         data[i+2] = brightness;
//     }
//     idata.data = data;
//     // Draw the pixels onto the visible canvas
//     context.putImageData(idata,0,0);
//     // Start over!
//     setTimeout(function(){ draw(video,context,backcontext,w,h); }, 0);
// }

// // $(function() {
// //     var canvas = document.getElementById('canvas');
// //     var ctx = canvas.getContext('2d');
// //     var video = document.getElementById('video');
  
// //     video.addEventListener('play', function() {
// //       self.width = self.video.videoWidth / 2;
// //           self.height = self.video.videoHeight / 2;
// //       var $this = this; //cache
      
// //       (function loop() {
// //         if (!$this.paused && !$this.ended) {
// //           ctx.drawImage($this, 0, 0);
// //           setTimeout(loop, 1000 / 30); // drawing at 30fps
// //         }
// //       })();
// //     }, 0);
// //   });