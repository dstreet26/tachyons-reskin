
# Quickstart

Requires [Node](https://nodejs.org/en/)

```
git clone https://gitlab.com/dstreet26/tachyons-reskin.git
cd tachyons-reskin
yarn
npm run start:dev
```

# Info

- The name of this project will probably change to `elm-ga`

- Tachyons has been stripped of media-queries

- No flexbox is used for layout

- There are no mouse events, only a few keyboard ones (← → ↑ ↓ ⏎ ↺)

- [Zippers](https://en.wikipedia.org/wiki/Zipper_(data_structure)) are used a lot

- Main.elm is too big



# NPM Commands

Running


```
start:dev
start:prod
```

Code Quality

```
format -- Run Elm and Js formatters
lint:js:fix -- Run Eslint with auto-fix
lint:elm -- Open Elm-Analyse Dashboard
lint:elm:fix -- Run a fork of Elm-Analyse that fixes some errors (might require `npm run setup`)
review:fix -- run all elm-review modules and apply fixes
xref -- view cross-references to un-reachable code
```

# Todo

- [x] Search
- [x] My List
- [ ] My Station
- [ ] Re-do Profile (Settings)
- [x] Horizontal and Vertical Scrolling
- [x] Page History (using a stack)
- [x] RemoteData type
- [ ] Improve on-screen debugging
- [ ] Improve on-screen errors
- [ ] Remove similar colors and font-sizes from CSS
- [ ] Image asset caching with indexdb+ports?
- [x] Video Playback for MP4's and HLS with controls and CC

# Code Cleanup / Bugs
- [ ] Split update function
- [ ] Split keyhandler
- [x] PBS Logo
- [ ] Circle crop on old TV's
- [x] Text truncation
- [ ] De-duplicate dimensions (CSS and Elm)
- [ ] Replace Tachyons with px-based library or make one
- [x] Elm Analyse