import { Elm } from '../elm/Main.elm'
import Hls from 'hls.js'
import Cookies from 'js-cookie'

const hlsConfig = { maxBufferLength: 20, maxMaxBufferLength: 20 }
const hls = new Hls(hlsConfig)
hls.on(Hls.Events.MEDIA_ATTACHED, function (event, data) {
  elmApp.ports.mediaAttached.send(null)
})
hls.on(Hls.Events.MANIFEST_PARSED, function (event, data) {
  elmApp.ports.manifestParsed.send(data)
})
hls.on(Hls.Events.ERROR, function (event, data) {
  console.log('hls error', event, data)
})

//Lock down keys
window.addEventListener(
  'keydown',
  function (e) {
    // space and arrow keys
    if ([32, 37, 38, 39, 40, 13, 8].indexOf(e.keyCode) > -1) {
      e.preventDefault()
    }
  },

  false
)

//Initialize Elm with flags, then set up port subscriptions
var deviceIdKey = 'elm-ga-deviceId'
var userIdKey = 'elm-ga-userId'

var elmApp = Elm.Main.init({
  node: document.getElementById('app'),
  flags: {
    deviceId: Cookies.get(deviceIdKey) || null,
    userId: Cookies.get(userIdKey) || null,
    deviceType: getDeviceType(),
    buildNumber: 'e1',
  },
})

elmApp.ports.setDeviceId.subscribe(function (deviceId) {
  console.log('port setDeviceId', deviceId)
  Cookies.set(deviceIdKey, deviceId, { expires: 365 })
})
// elmApp.ports.setUserId.subscribe(function(userId) {
//   console.log("port setUserId", userId);
//   localStorage.setItem(userIdKey, userId);
// });

var video = null
var tracks = null
var track = null

elmApp.ports.videoEventStream.subscribe(function (event) {
  switch (event.kind) {
    case 'setup':
      video = document.getElementById('media-video')
      hls.attachMedia(video)
      console.log(video)
      break
    case 'loadmanifest':
      hls.loadSource(event.url)
      break
    case 'setlevel':
      console.log('setlevel', event.level)
      hls.currentLevel = event.level
      break
    case 'detachmedia':
      console.log('detachMedia')
      hls.detachMedia()
      break
    case 'pause':
      video.pause()
      console.log('Pausing')
      break
    case 'play':
      console.log('Request to play')
      video.play()
      break
    case 'stop':
      video.pause()
      video.currentTime = 0
      console.log('Stopping')
      break
    case 'setplaybackspeed':
      video.playbackRate = event.speed
      console.log('Setting playback', event.speed)
      break
    case 'restart':
      video.currentTime = 0
      console.log('Restarting')
      break

    case 'seekto':
      video.currentTime = event.position
      console.log('Seeked to ' + event.position)
      break
    case 'enablecaptions':
      // TODO: assuming one and only one track
      tracks = video.textTracks
      track = tracks['0']
      track.mode = 'showing'
      console.log('Enabling Track')
      console.log(track)

      break
    case 'disablecaptions':
      // TODO: assuming one and only one track
      tracks = video.textTracks
      // for (var i = 0; i < tracks.length; i++) {
      //   track = tracks[i];
      //   track.mode = "hidden";
      //   console.log("Disabling Track " + track);
      // }
      track = tracks['0']
      track.mode = 'hidden'
      console.log('Disabling Track')
      console.log(track)
      break
    default:
      console.log('Unexpected event:')
      console.log(event)
  }
})

function compareUserAgent(input) {
  return navigator.userAgent.indexOf(input) !== -1
}
function isRuntime2017() {
  return compareUserAgent('Tizen 3.0')
}
function getDeviceType() {
  if (window.VIZIO) {
    return 'VIZIO'
  } else if (isRuntime2017()) {
    return 'tizen2017'
  } else if (window.webapis) {
    return 'tizen'
  } else {
    return 'other'
  }
}
