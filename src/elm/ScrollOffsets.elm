module ScrollOffsets exposing (calcRowScrollDifference)

import Data.Main exposing (AlternateDirection(..))
import Data.Row exposing (Row(..))
import Svg.Attributes exposing (direction)


calcRowScrollDifference : AlternateDirection -> Row -> Row -> Int
calcRowScrollDifference direction currentRow nextRow =
    case currentRow of
        HeroButtonRow _ ->
            case nextRow of
                HeroButtonRow _ ->
                    0

                SecondaryButtonRow _ ->
                    0

                FeaturedVideoRow _ ->
                    0

                BrowseButtonRow _ ->
                    850

                VideoItemRow _ ->
                    350

                ShowItemRow _ ->
                    380

                MultipleItemRow _ ->
                    340

        SecondaryButtonRow _ ->
            case nextRow of
                HeroButtonRow _ ->
                    0

                SecondaryButtonRow _ ->
                    0

                FeaturedVideoRow _ ->
                    0

                BrowseButtonRow _ ->
                    850

                VideoItemRow _ ->
                    350

                ShowItemRow _ ->
                    380

                MultipleItemRow _ ->
                    340

        FeaturedVideoRow _ ->
            case nextRow of
                HeroButtonRow _ ->
                    0

                SecondaryButtonRow _ ->
                    0

                FeaturedVideoRow _ ->
                    0

                BrowseButtonRow _ ->
                    850

                VideoItemRow _ ->
                    350

                ShowItemRow _ ->
                    380

                MultipleItemRow _ ->
                    340

        BrowseButtonRow _ ->
            case nextRow of
                HeroButtonRow _ ->
                    0

                SecondaryButtonRow _ ->
                    0

                FeaturedVideoRow _ ->
                    case direction of
                        Up ->
                            850

                        Down ->
                            0

                BrowseButtonRow _ ->
                    850

                VideoItemRow _ ->
                    120

                ShowItemRow _ ->
                    380

                MultipleItemRow _ ->
                    340

        VideoItemRow _ ->
            case nextRow of
                HeroButtonRow _ ->
                    0

                SecondaryButtonRow _ ->
                    0

                FeaturedVideoRow _ ->
                    0

                BrowseButtonRow _ ->
                    case direction of
                        Up ->
                            120

                        Down ->
                            0

                VideoItemRow _ ->
                    470

                ShowItemRow _ ->
                    case direction of
                        Up ->
                            555

                        Down ->
                            490

                MultipleItemRow _ ->
                    470

        ShowItemRow _ ->
            case nextRow of
                HeroButtonRow _ ->
                    0

                SecondaryButtonRow _ ->
                    0

                FeaturedVideoRow _ ->
                    0

                BrowseButtonRow _ ->
                    850

                VideoItemRow _ ->
                    case direction of
                        Up ->
                            490

                        Down ->
                            555

                ShowItemRow _ ->
                    380

                MultipleItemRow _ ->
                    490

        MultipleItemRow _ ->
            case nextRow of
                HeroButtonRow _ ->
                    0

                SecondaryButtonRow _ ->
                    0

                FeaturedVideoRow _ ->
                    0

                BrowseButtonRow _ ->
                    850

                VideoItemRow _ ->
                    350

                ShowItemRow _ ->
                    380

                MultipleItemRow _ ->
                    340
