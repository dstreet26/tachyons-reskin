module Data.Parent exposing (EpisodeParent, Parent(..), SeasonParent, SeasonShow, ShowParent, SpecialParent, decodeParent)

import Data.Franchise exposing (Franchise, decodeFranchise)
import Json.Decode exposing (Decoder, bool, fail, field, index, int, map, maybe, string, succeed)
import Json.Decode.Pipeline exposing (required, resolve, optional)


type Parent
    = Episode EpisodeParent
    | Season SeasonParent
    | Show ShowParent
    | Special SpecialParent


type alias EpisodeParent =
    { cid : String
    , slug : String
    , ordinal : Int
    , title : String
    , season : SeasonParent
    }


type alias SeasonParent =
    { cid : String
    , ordinal : Int
    , title : String
    , show : SeasonShow
    }


type alias ShowParent =
    { displayEpisodeNumber : Bool
    , cid : String
    , title : String
    , franchise : Maybe Franchise
    , trackingGaEvent : String
    , slug : String
    , audienceScope : String
    , trackingGaPage : String
    , seasonsCount : Int
    , funderMessage : String
    }


type alias SpecialParent =
    { cid : String
    , slug : String
    , show : SeasonShow
    , title : String
    }


type alias SeasonShow =
    { displayEpisodeNumber : Bool
    , audienceScope : String
    , trackingGaEvent : String
    , cid : String
    , funderMessage : String
    , slug : String
    , franchise : Maybe Franchise
    , title : String
    , seasonsCount : Int
    , trackingGaPage : String
    }


decodeParent : Decoder Parent
decodeParent =
    let
        toDecoder resource_type =
            case resource_type of
                "episode" ->
                    map Episode decodeEpisodeParent

                "season" ->
                    map Season decodeSeasonParent

                "show" ->
                    map Show decodeShowParent

                "special" ->
                    map Special decodeSpecialParent

                _ ->
                    fail "failed on resource_type"
    in
    succeed toDecoder
        |> required "resource_type" string
        |> resolve


decodeEpisodeParent : Decoder EpisodeParent
decodeEpisodeParent =
    succeed EpisodeParent
        |> required "cid" string
        |> required "slug" string
        |> required "ordinal" int
        |> required "title" string
        |> required "season" decodeSeasonParent


decodeSeasonParent : Decoder SeasonParent
decodeSeasonParent =
    succeed SeasonParent
        |> required "cid" string
        |> required "ordinal" int
        |> required "title" string
        |> required "show" decodeSeasonShow


decodeShowParent : Decoder ShowParent
decodeShowParent =
    succeed ShowParent
        |> required "display_episode_number" bool
        |> required "cid" string
        |> required "title" string
        |> required "franchise" (maybe decodeFranchise)
        |> required "tracking_ga_event" string
        |> required "slug" string
        |> required "audience" decodeAudienceScope
        |> required "tracking_ga_page" string
        |> required "seasons_count" int
        |> optional "funder_message" string ""


decodeSpecialParent : Decoder SpecialParent
decodeSpecialParent =
    succeed SpecialParent
        |> required "cid" string
        |> required "slug" string
        |> required "show" decodeSeasonShow
        |> required "title" string


decodeSeasonShow : Decoder SeasonShow
decodeSeasonShow =
    succeed SeasonShow
        |> required "display_episode_number" bool
        |> required "audience" decodeAudienceScope
        |> required "tracking_ga_event" string
        |> required "cid" string
        |> optional "funder_message" string ""
        |> required "slug" string
        |> required "franchise" (maybe decodeFranchise)
        |> required "title" string
        |> required "seasons_count" int
        |> required "tracking_ga_page" string



-- TODO: consider moving


decodeAudienceScope : Decoder String
decodeAudienceScope =
    index 0 (field "scope" string)
