module Data.ShowsPage exposing (ShowCategory, ShowPageData, ShowPageLocation(..))

import Data.ContentResponse exposing (ShowContent)
import List.Zipper exposing (Zipper)


type alias ShowCategory =
    { title : String
    , items : Zipper ShowContent
    }


type alias ShowPageData =
    { showCategories : Zipper ShowCategory
    , focusLocation : ShowPageLocation
    }


type ShowPageLocation
    = Categories
    | Items
