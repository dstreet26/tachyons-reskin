module Data.MyList exposing (MyListData, MyListFocusLocation(..))

import Data.Row exposing (RowHolder)
import RemoteData exposing (WebData)


type alias MyListData =
    { favoriteShows : WebData RowHolder
    , watchlist : WebData RowHolder
    , history : WebData RowHolder
    , focusLocation : MyListFocusLocation
    , scrollPosition : Int
    }


type MyListFocusLocation
    = FavoriteShows
    | Watchlist
    | History
