module Data.Device exposing (DeviceId, DeviceType(..))


type DeviceType
    = VIZIO
    | Tizen
    | Tizen2017
    | Other


type alias DeviceId =
    String
