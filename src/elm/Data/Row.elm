module Data.Row exposing (ButtonItem, HeroButton, HeroButtonItem(..), Row(..), RowHolder, collectionsToRowHolderFeaturedVideos, collectionsToRowHolderResumeWatching, collectionsToRowHolderTheRest, contentResponseToShowRowBySlugWithTitle, contentResponseToVideoRowBySlugWithTitle, dummyFeaturedVideosRow, dummyShowItemRow, mapCollectionToRowHolder, mapToRowHolders)

import Data.ContentResponse
    exposing
        ( Collection(..)
        , ContentResponse
        , MultipleContent(..)
        , ShowContent
        , VideoContent
        , bySlug
        , dummyShowContent
        , dummyShowItemList
        , dummyVideoContent
        , dummyVideoItemList
        )
import List exposing (filterMap, take)
import List.Extra exposing (filterNot, find)
import List.Zipper exposing (Zipper, fromList, singleton, withDefault)


type alias RowHolder =
    { row : Row
    , title : Maybe String
    , horizontalScrollPosition : Maybe Int
    }


type Row
    = HeroButtonRow (Zipper HeroButtonItem)
    | SecondaryButtonRow (Zipper HeroButton)
    | FeaturedVideoRow (Zipper VideoContent)
    | BrowseButtonRow (Zipper ButtonItem)
    | VideoItemRow (Zipper VideoContent)
    | ShowItemRow (Zipper ShowContent)
    | MultipleItemRow (Zipper MultipleContent)


type HeroButtonItem
    = Play HeroButton
    | Add HeroButton


type alias HeroButton =
    { icon : String
    , text : String

    -- , slug : String
    }


type alias ButtonItem =
    { title : String
    }


dummyShowItemRow : RowHolder
dummyShowItemRow =
    { row = ShowItemRow (dummyShowItemList |> fromList |> withDefault dummyShowContent)
    , title = Nothing
    , horizontalScrollPosition = Nothing
    }


dummyVideoItemRow : RowHolder
dummyVideoItemRow =
    { row = VideoItemRow (dummyVideoItemList |> fromList |> withDefault dummyVideoContent)
    , title = Nothing
    , horizontalScrollPosition = Nothing
    }


dummyFeaturedVideosRow : RowHolder
dummyFeaturedVideosRow =
    { row =
        FeaturedVideoRow
            (singleton
                dummyVideoContent
            )
    , title = Nothing
    , horizontalScrollPosition = Nothing
    }


mapCollectionToRowHolder : Collection -> Maybe RowHolder
mapCollectionToRowHolder collection =
    case collection of
        InlineShow x ->
            Just
                { title = Just x.title
                , row =
                    ShowItemRow
                        (x.content
                            |> fromList
                            |> withDefault dummyShowContent
                        )
                , horizontalScrollPosition = Just 0
                }

        InlineVideo x ->
            Just
                { title = Just x.title
                , row =
                    VideoItemRow
                        (x.content
                            |> fromList
                            |> withDefault
                                dummyVideoContent
                        )
                , horizontalScrollPosition = Just 0
                }

        InlineSeason _ ->
            -- TODO: SeasonsRowHolder
            Nothing

        InlineMultiple x ->
            Just
                { title = Just x.title
                , row =
                    MultipleItemRow
                        (x.content
                            |> fromList
                            |> withDefault
                                (VideoM dummyVideoContent)
                        )
                , horizontalScrollPosition = Just 0
                }

        StubShow _ ->
            Nothing

        StubVideo _ ->
            Nothing

        StubSeason _ ->
            Nothing

        StubMultiple _ ->
            Nothing


collectionsToRowHolderFeaturedVideos : List Collection -> RowHolder
collectionsToRowHolderFeaturedVideos collections =
    let
        found =
            find
                (\x ->
                    case x of
                        InlineVideo y ->
                            y.slug == "featured-videos"

                        _ ->
                            False
                )
                collections
    in
    case found of
        Nothing ->
            dummyFeaturedVideosRow

        Just x ->
            case x of
                InlineVideo inlineVideoData ->
                    { title = Just inlineVideoData.title
                    , row =
                        FeaturedVideoRow
                            (inlineVideoData.content
                                |> take 4
                                |> fromList
                                |> withDefault
                                    dummyVideoContent
                            )
                    , horizontalScrollPosition = Just 0
                    }

                _ ->
                    dummyFeaturedVideosRow


contentResponseToShowRowBySlugWithTitle : String -> String -> ContentResponse -> RowHolder
contentResponseToShowRowBySlugWithTitle slug title contentResponse =
    let
        found =
            find
                (\x ->
                    case x of
                        InlineShow y ->
                            y.slug == slug

                        _ ->
                            False
                )
                contentResponse.collections
    in
    case found of
        Nothing ->
            dummyShowItemRow

        Just x ->
            case x of
                InlineShow inlineShowData ->
                    { title = Just title
                    , row =
                        ShowItemRow
                            (inlineShowData.content
                                |> fromList
                                |> withDefault
                                    dummyShowContent
                            )
                    , horizontalScrollPosition = Just 0
                    }

                _ ->
                    dummyShowItemRow


contentResponseToVideoRowBySlugWithTitle : String -> String -> ContentResponse -> RowHolder
contentResponseToVideoRowBySlugWithTitle slug title contentResponse =
    let
        found =
            find
                (\x ->
                    case x of
                        InlineVideo y ->
                            y.slug == slug

                        _ ->
                            False
                )
                contentResponse.collections
    in
    case found of
        Nothing ->
            dummyVideoItemRow

        Just x ->
            case x of
                InlineVideo inlineVideoData ->
                    { title = Just title
                    , row =
                        VideoItemRow
                            (inlineVideoData.content
                                |> fromList
                                |> withDefault
                                    dummyVideoContent
                            )
                    , horizontalScrollPosition = Just 0
                    }

                _ ->
                    dummyVideoItemRow


collectionsToRowHolderResumeWatching : List Collection -> Maybe RowHolder
collectionsToRowHolderResumeWatching collections =
    let
        found =
            find
                (\x ->
                    case x of
                        InlineVideo y ->
                            y.slug == "resume-watching"

                        InlineShow y ->
                            y.slug == "resume-watching"

                        InlineSeason y ->
                            y.slug == "resume-watching"

                        _ ->
                            False
                )
                collections
    in
    case found of
        Just collection ->
            case collection of
                InlineVideo inlineVideoData ->
                    Just
                        { title = Just inlineVideoData.title
                        , row =
                            VideoItemRow
                                (inlineVideoData.content
                                    |> fromList
                                    |> withDefault
                                        dummyVideoContent
                                )
                        , horizontalScrollPosition = Just 0
                        }

                InlineShow inlineShowData ->
                    Just
                        { title = Just inlineShowData.title
                        , row =
                            ShowItemRow
                                (inlineShowData.content
                                    |> fromList
                                    |> withDefault
                                        dummyShowContent
                                )
                        , horizontalScrollPosition = Just 0
                        }

                _ ->
                    Nothing

        _ ->
            Nothing


collectionsToRowHolderTheRest : List Collection -> List RowHolder
collectionsToRowHolderTheRest collections =
    let
        filtered =
            collections
                |> filterNot (bySlug "resume-watching")
                |> filterNot (bySlug "featured-videos")
    in
    filterMap mapCollectionToRowHolder filtered


mapToRowHolders : List Collection -> List RowHolder
mapToRowHolders collections =
    filterMap mapCollectionToRowHolder collections
