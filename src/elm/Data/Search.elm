module Data.Search exposing (SearchButton(..), SearchPageData, SearchPageLocation(..))

import Data.Row exposing (RowHolder)
import List.Zipper exposing (Zipper)


type alias SearchPageData =
    { query : String
    , focusLocation : SearchPageLocation
    , buttons : Zipper SearchButton
    , rows : Maybe (Zipper RowHolder)
    }


type SearchPageLocation
    = EnteringQuery
    | BrowsingContent


type SearchButton
    = Letter Char
    | Clear
    | Back
    | ManualSearch
