module Data.DetailsPage exposing (DetailsPage(..), ShowDetailsPageData, SubNav(..), VideoDetailsFocusLocation(..), VideoDetailsPageData, secondaryButtons)

import Data.ContentResponse exposing (MultipleContent)
import Data.Row exposing (HeroButton, HeroButtonItem, RowHolder)
import Data.ShowResource exposing (ShowResource)
import Data.VideoResource exposing (VideoResource)
import List.Zipper exposing (Zipper, fromCons)



-- TODO: add scrollposition to each page


type DetailsPage
    = VideoDetailsPage VideoDetailsPageData
    | ShowDetailsPage ShowDetailsPageData


type alias VideoDetailsPageData =
    { resource : VideoResource
    , subNavs : Zipper SubNav
    , heroButtonRow : Zipper HeroButtonItem
    , secondaryButtonRow : Zipper HeroButton
    , focusLocation : VideoDetailsFocusLocation
    }


type VideoDetailsFocusLocation
    = MainButtons
    | SecondaryButtons
    | SubNavNav
    | SubNav


type SubNav
    = Details
    | MultipleColumn String (Zipper MultipleContent)


secondaryButtons : Zipper HeroButton
secondaryButtons =
    fromCons
        { text = "Preview", icon = "preview" }
        [ { text = "iTunes", icon = "star" }
        , { text = "Go To Show", icon = "layers" }
        ]


type alias ShowDetailsPageData =
    { resource : ShowResource
    , rows : Zipper RowHolder
    }
