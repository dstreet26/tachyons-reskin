module Data.VideoType exposing (VideoType(..), decodeVideoType)

import Json.Decode exposing (Decoder, andThen, fail, string, succeed)


type VideoType
    = Clip
    | FullLength
    | Preview


decodeVideoType : Decoder VideoType
decodeVideoType =
    string
        |> andThen
            (\str ->
                case str of
                    "full_length" ->
                        succeed FullLength

                    "preview" ->
                        succeed Preview

                    "clip" ->
                        succeed Clip

                    _ ->
                        fail "failed on video_type"
            )
