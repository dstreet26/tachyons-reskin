module Data.Settings exposing (AboutOptions, CurrentStationOptions, LoggedInOptions, SettingCardType(..), SettingsCard, StationItemOptions)


type alias CurrentStationOptions =
    { imageSource : String
    }


type alias StationItemOptions =
    { name : String
    , isActivated : Bool
    }


type alias LoggedInOptions =
    { email : String
    }


type alias AboutOptions =
    { title : String
    }


type alias SettingsCard =
    { settingCardType : SettingCardType
    }


type SettingCardType
    = CurrentStation CurrentStationOptions
    | StationItem StationItemOptions
    | LoggedIn LoggedInOptions
    | About AboutOptions
