module Data.FocusLocation exposing (FocusLocation(..))

-- This could be extended for modals/popups, maybe for video menus


type FocusLocation
    = Sidebar
    | PageContent
