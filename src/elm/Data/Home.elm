module Data.Home exposing (HomeData)

import Data.Row exposing (RowHolder)
import List.Zipper exposing (Zipper)


type alias HomeData =
    { rows : Zipper RowHolder
    , scrollPosition : Int
    }
