module Data.VideoResource exposing (Chapter, ClosedCaption, Link, RelatedLink, VideoProfile, VideoResource, VideoResourceFlags, VideoResourceImages, decodeVideoResource)

import Data.Parent exposing (Parent, decodeParent)
import Data.VideoType exposing (VideoType, decodeVideoType)
import Json.Decode exposing (Decoder, bool, float, int, list, maybe, string, succeed)
import Json.Decode.Extra exposing (datetime)
import Json.Decode.Pipeline exposing (required)
import Time exposing (Posix)
import Json.Decode.Pipeline exposing (optional)



-- curl 'http://content.services.pbs.org/v3/samsungtv/screens/video-assets/nova-building-the-great-cathedrals/?user_id=eab31436-576b-42bc-a24f-92b4d04c97b1&nocache=1557706743055' -H 'Accept: application/json, text/plain, */*' -H 'Referer: http://otter-tizen-ga-prod.pbs.org/video-details/https%3A%2F%2Fcontent.services.pbs.org%2Fv3%2Fsamsungtv%2Fscreens%2Fvideo-assets%2Fnova-building-the-great-cathedrals%2F%3Fuser_id%3Deab31436-576b-42bc-a24f-92b4d04c97b1' -H 'X-PBS-PlatformVersion: 3.0.0' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36' -H 'Authorization: Basic a2lkc3NhbXN1bmd0djpCQ1hiQTV2SXhqOFNGNnY=' -H 'Origin: http://otter-tizen-ga-prod.pbs.org' -H 'Content-Type: application/json;charset=utf-8' --compressed


type alias VideoResource =
    { closedCaptions : List ClosedCaption
    , descriptionShort : String
    , cid : String
    , chapters : List Chapter
    , slug : String
    , legacyTpMediaId : Int
    , duration : Int
    , links : List Link
    , descriptionLong : String
    , expireDate : Maybe Posix
    , availableDate : String
    , flags : VideoResourceFlags
    , titleSortable : String
    , itemType : String
    , premiereDate : Posix
    , encoreDate : Posix
    , mp4Videos : List VideoProfile
    , availability : String
    , relatedLinks : List RelatedLink
    , images : VideoResourceImages
    , funderMessage : String
    , url : String
    , isExcludedFromDfp : Bool
    , secondsWatched : Maybe Int
    , title : String

    -- , relatedPromos : List ComplexType
    , videoType : VideoType
    , hlsVideos : List VideoProfile
    , contentRating : String
    , parent : Maybe Parent
    }


type alias VideoResourceFlags =
    { isPlayable : Bool
    , isExpiringSoon : Bool
    , isPlayableExplanation : Maybe Bool
    , canEmbedPlayer : Bool
    , isNew : Bool
    , isFullyWatched : Maybe Bool
    , isInWatchlist : Maybe Bool
    , hasCaptions : Bool
    , isMvod : Bool
    }


type alias VideoResourceImages =
    { assetMezzanine16x9 : String
    }


type alias ClosedCaption =
    { url : String
    , profile : String
    }


type alias Chapter =
    { title : String
    , duration : Int
    , start : Float
    , end : Float
    }


type alias Link =
    { updatedAt : String
    , value : String
    , profile : String
    }


type alias VideoProfile =
    { url : String
    , profile : String
    }



-- TODO: move to testing data
-- mp4-16x9-720p
-- mp4-2500k-16x9


type alias RelatedLink =
    { title : String
    , descriptionShort : String
    , cid : String
    , imageUrl : Maybe String
    , itemType : String
    , externalUrl : String
    }


decodeRelatedLink : Decoder RelatedLink
decodeRelatedLink =
    succeed RelatedLink
        |> required "title" string
        |> required "description_short" string
        |> required "cid" string
        |> required "image_url" (maybe string)
        |> required "item_type" string
        |> required "external_url" string


decodeVideoProfile : Decoder VideoProfile
decodeVideoProfile =
    succeed VideoProfile
        |> required "url" string
        |> required "profile" string


decodeLink : Decoder Link
decodeLink =
    succeed Link
        |> required "updated_at" string
        |> required "value" string
        |> required "profile" string


decodeChapter : Decoder Chapter
decodeChapter =
    succeed Chapter
        |> required "title" string
        |> required "duration" int
        |> required "start" float
        |> required "end" float


decodeClosedCaption : Decoder ClosedCaption
decodeClosedCaption =
    succeed ClosedCaption
        |> required "url" string
        |> required "profile" string


decodeVideoResource : Decoder VideoResource
decodeVideoResource =
    succeed VideoResource
        |> required "closed_captions" (list decodeClosedCaption)
        |> required "description_short" string
        |> required "cid" string
        |> required "chapters" (list decodeChapter)
        |> required "slug" string
        |> required "legacy_tp_media_id" int
        |> required "duration" int
        |> required "links" (list decodeLink)
        |> required "description_long" string
        |> required "expire_date" (maybe datetime)
        |> required "available_date" string
        |> required "flags" decodeVideoResourceFlags
        |> required "title_sortable" string
        |> required "item_type" string
        |> required "premiere_date" datetime
        |> required "encore_date" datetime
        |> required "mp4_videos" (list decodeVideoProfile)
        |> required "availability" string
        |> required "related_links" (list decodeRelatedLink)
        |> required "images" decodeVideoResourceImages
        |> optional "funder_message" string ""
        |> required "url" string
        |> required "is_excluded_from_dfp" bool
        |> required "seconds_watched" (maybe int)
        |> required "title" string
        -- |> required "related_promos" (list decodeComplexType)
        |> required "video_type" decodeVideoType
        -- |> required "parent" (decodeVideoResourceParent)
        |> required "hls_videos" (list decodeVideoProfile)
        |> required "content_rating" string
        |> required "parent" (maybe decodeParent)


decodeVideoResourceFlags : Decoder VideoResourceFlags
decodeVideoResourceFlags =
    succeed VideoResourceFlags
        |> required "is_playable" bool
        |> required "is_expiring_soon" bool
        |> required "is_playable_explanation" (maybe bool)
        |> required "can_embed_player" bool
        |> required "is_new" bool
        |> required "is_fully_watched" (maybe bool)
        |> required "is_in_watchlist" (maybe bool)
        |> required "has_captions" bool
        |> required "is_mvod" bool


decodeVideoResourceImages : Decoder VideoResourceImages
decodeVideoResourceImages =
    succeed VideoResourceImages
        |> required "asset-mezzanine-16x9" string
