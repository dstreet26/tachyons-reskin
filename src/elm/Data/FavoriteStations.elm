module Data.FavoriteStations exposing (FavoriteStations, FavoriteStationsCollection, FavoriteStationsCollectionContent, FavoriteStationsCollectionContentImages, decodeFavoriteStations)

import Json.Decode exposing (Decoder, bool, int, list, maybe, string, succeed)
import Json.Decode.Pipeline exposing (optional, required)



-- curl 'https://content.services.pbs.org/v3/samsungtv/FavoriteStations/eab31436-576b-42bc-a24f-92b4d04c97b1/collections/favorite-stations/?nocache=1550499238060&nocache=1550499238060'    -H 'Authorization: Basic a2lkc3NhbXN1bmd0djpCQ1hiQTV2SXhqOFNGNnY=' -H 'X-PBS-PlatformVersion: 3.0.0'


type alias FavoriteStations =
    { collections : List FavoriteStationsCollection
    }


type alias FavoriteStationsCollection =
    { title : String
    , itemType : String
    , collectionType : String
    , slug : String
    , url : String
    , hasFavoriteStationsData : Bool
    , content : List FavoriteStationsCollectionContent
    }


type alias FavoriteStationsCollectionContent =
    { websiteUrl : String
    , pageTracking : String
    , itemType : String
    , timezone : String
    , secondaryTimezone : Maybe String
    , passportLearnMoreUrl : String
    , callSign : String
    , passportUrl : String
    , scheduleUrl : String
    , images : FavoriteStationsCollectionContentImages
    , title : String
    , primetimeStart : Int
    , donateUrl : String
    , facebookUrl : String
    , eventTracking : String
    , twitterUrl : String
    , cid : String
    , shortCommonName : String
    , mvodEnabled : Bool
    , kidsLiveStreamUrl : String
    }


type alias FavoriteStationsCollectionContentImages =
    { whiteCobrandedLogo : String
    , colorCobrandedLogo : String
    , stationScenery11 : Maybe String
    }


decodeFavoriteStations : Decoder FavoriteStations
decodeFavoriteStations =
    succeed FavoriteStations
        |> required "collections" (list decodeFavoriteStationsCollection)


decodeFavoriteStationsCollectionContent : Decoder FavoriteStationsCollectionContent
decodeFavoriteStationsCollectionContent =
    succeed FavoriteStationsCollectionContent
        |> required "website_url" string
        |> required "page_tracking" string
        |> required "item_type" string
        |> required "timezone" string
        |> required "secondary_timezone" (maybe string)
        |> required "passport_learn_more_url" string
        |> required "call_sign" string
        |> required "passport_url" string
        |> required "schedule_url" string
        |> required "images" decodeFavoriteStationsCollectionContentImages
        |> required "title" string
        |> required "primetime_start" int
        |> required "donate_url" string
        |> required "facebook_url" string
        |> required "event_tracking" string
        |> required "twitter_url" string
        |> required "cid" string
        |> required "short_common_name" string
        |> required "mvod_enabled" bool
        |> required "kids_live_stream_url" string


decodeFavoriteStationsCollectionContentImages : Decoder FavoriteStationsCollectionContentImages
decodeFavoriteStationsCollectionContentImages =
    succeed FavoriteStationsCollectionContentImages
        |> required "white-cobranded-logo" string
        |> required "color-cobranded-logo" string
        |> optional "station-scenery-11" (maybe string) Nothing


decodeFavoriteStationsCollection : Decoder FavoriteStationsCollection
decodeFavoriteStationsCollection =
    succeed FavoriteStationsCollection
        |> required "title" string
        |> required "item_type" string
        |> required "collection_type" string
        |> required "slug" string
        |> required "url" string
        |> required "has_profile_data" bool
        |> required "content" (list decodeFavoriteStationsCollectionContent)
