module Data.Video exposing (CaptionStyles, CaptionStylingFocusLocation(..), ModalFocusLocation(..), PlayerState(..), VideoElementMsg(..), VideoEvent(..), VideoFocusLocation(..), VideoModel, VideoQualityAndPlaybackFocusLocation(..), decodeDuration, decodePosition, pushVideoEvent, uiTimeout, videoUpdate)

import Bounce exposing (Bounce)
import Data.HlsManifest exposing (HlsState, Level)
import Data.VideoResource exposing (ClosedCaption)
import Json.Decode as Decode exposing (Decoder, field, map)
import Json.Encode as Encode exposing (Value, object, string)
import List.Zipper exposing (Zipper)
import Ports exposing (videoEventStream)


uiTimeout =
    Bounce.delay 1000 FadeUI



-- Some code taken from https://github.com/geppettodivacin/elm-video/blob/master/src/media-player.elm


type alias VideoModel =
    { playerState : PlayerState
    , focusLocation : VideoFocusLocation
    , closedCaptions : Bool
    , percentLoaded : Float
    , percentBuffered : Float
    , position : Float
    , duration : Float
    , videoTitle : String
    , videoDescription : String
    , videoSource : String
    , captionSource : List ClosedCaption
    , showUi : Bool
    , bounce : Bounce
    , modalFocusLocation : ModalFocusLocation
    , captionStylingFocusLocation : CaptionStylingFocusLocation
    , videoQualityAndPlaybackFocusLocation : VideoQualityAndPlaybackFocusLocation
    , captionStyles : CaptionStyles
    , playbackSpeed : Zipper Float
    , levels : Zipper Level
    , hlsState : HlsState
    }


type alias CaptionStyles =
    { fontSize : Zipper String
    , fontStyle : Zipper String
    , fgColor : Zipper String
    , bgColor : Zipper String
    , windowColor : Zipper String
    , fgOpacity : Zipper String
    , bgOpacity : Zipper String
    , windowOpacity : Zipper String
    , textShadow : Zipper String
    }


type VideoFocusLocation
    = Slider
    | NextVideoButton
    | OptionsButton
    | PlayPauseButton
    | RestartButton
    | OptionsModal
    | OptionsModalBody


type ModalFocusLocation
    = VideoQualityAndPlayback
    | AudioTracks
    | Captions
    | Styling


type CaptionStylingFocusLocation
    = FontSize
    | FontStyle
    | FgColor
    | BgColor
    | WindowColor
    | FgOpacity
    | BgOpacity
    | WindowOpacity
    | TextShadow


type VideoQualityAndPlaybackFocusLocation
    = Quality
    | Speed



-- TODO: replace textshadow with edgecolor and edgetype


type PlayerState
    = Buffering
    | VideoLoading
    | Paused
    | Playing


type VideoElementMsg
    = NowPlaying
    | NowPaused
    | NowAtPosition Float
    | NowHasDuration Float
    | LoadStart
    | CanPlayThrough
    | FadeUI



-- | SeekToClicked Float


videoUpdate : VideoElementMsg -> VideoModel -> ( VideoModel, Cmd VideoElementMsg )
videoUpdate msg model =
    case msg of
        CanPlayThrough ->
            ( { model | playerState = Paused }, pushVideoEvent Play )

        LoadStart ->
            ( model, Cmd.none )

        NowPlaying ->
            ( { model | playerState = Playing, showUi = True, bounce = Bounce.push model.bounce }, uiTimeout )

        NowPaused ->
            ( { model | playerState = Paused }, Cmd.none )

        NowAtPosition position ->
            ( { model | position = position, playerState = Playing }
            , Cmd.none
            )

        NowHasDuration duration ->
            ( { model | duration = duration }
            , Cmd.none
            )

        FadeUI ->
            let
                newBounce =
                    Bounce.pop model.bounce
            in
            ( { model
                | showUi =
                    Bounce.steady newBounce
                , bounce = newBounce
              }
            , Cmd.none
            )


eventTarget : Decoder a -> Decoder a
eventTarget decoder =
    field "target" decoder


decodePosition : Decoder VideoElementMsg
decodePosition =
    eventTarget <|
        map NowAtPosition
            (field "currentTime" Decode.float)


decodeDuration : Decoder VideoElementMsg
decodeDuration =
    eventTarget <|
        map NowHasDuration
            (field "duration" Decode.float)


type VideoEvent
    = DetachMedia
    | LoadManifest String
    | Pause
    | Play
    | SeekTo Float
    | SetLevel Int
    | SetPlaybackSpeed Float
    | Setup


pushVideoEvent : VideoEvent -> Cmd msg
pushVideoEvent event =
    event
        |> encodeVideoEvent
        |> videoEventStream


encodeVideoEvent : VideoEvent -> Value
encodeVideoEvent event =
    case event of
        DetachMedia ->
            object
                [ ( "kind", string "detachmedia" )
                ]

        LoadManifest url ->
            object
                [ ( "kind", string "loadmanifest" )
                , ( "url", string url )
                ]

        Pause ->
            object
                [ ( "kind", string "pause" ) ]

        Play ->
            object
                [ ( "kind", string "play" ) ]

        SeekTo position ->
            object
                [ ( "kind", string "seekto" )
                , ( "position", Encode.float position )
                ]

        SetLevel level ->
            object
                [ ( "kind", string "setlevel" )
                , ( "level", Encode.int level )
                ]

        SetPlaybackSpeed speed ->
            object
                [ ( "kind", string "setplaybackspeed" )
                , ( "speed", Encode.float speed )
                ]

        Setup ->
            object
                [ ( "kind", string "setup" ) ]



-- FIXME: this is not working on localhost (tried crossorigin=anonymous and changing http to https)
