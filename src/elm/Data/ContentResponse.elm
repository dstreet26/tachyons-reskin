module Data.ContentResponse exposing (Collection(..), ContentFlags, ContentResponse, InlineMultipleData, InlineSeasonData, InlineShowData, InlineVideoData, MultipleContent(..), Resource(..), SeasonContent, SeasonFlags, ShowContent, StubData, VideoContent, bySlug, decodeContentResponse, dummyShowContent, dummyShowItemList, dummyVideoContent, dummyVideoItemList, isStub)

import Data.Parent exposing (Parent(..), SeasonShow, decodeParent)
import Data.ShowResource exposing (ShowResource, decodeShowResource)
import Data.VideoResource exposing (VideoResource, decodeVideoResource)
import Data.VideoType exposing (VideoType(..), decodeVideoType)
import Json.Decode exposing (Decoder, bool, fail, int, list, map, maybe, string, succeed)
import Json.Decode.Extra exposing (datetime)
import Json.Decode.Pipeline exposing (optional, optionalAt, required, resolve)
import Time exposing (Posix, millisToPosix)



-- curl 'https://content.services.pbs.org/v3/samsungtv/screens/shows/?station_id=bb77b7f7-78ec-4134-aa62-3902aeca6169' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36' -H 'Referer: http://localhost:2019/' -H 'Origin: http://localhost:2019' -H 'X-PBS-PlatformVersion: 3.0.0' -H 'Authorization: Basic a2lkc3NhbXN1bmd0djpCQ1hiQTV2SXhqOFNGNnY=' --compressed
-- curl 'https://content.services.pbs.org/v3/samsungtv/screens/home/?station_id=bb77b7f7-78ec-4134-aa62-3902aeca6169' -H 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.131 Safari/537.36' -H 'Referer: http://localhost:2019/' -H 'Origin: http://localhost:2019' -H 'X-PBS-PlatformVersion: 3.0.0' -H 'Authorization: Basic a2lkc3NhbXN1bmd0djpCQ1hiQTV2SXhqOFNGNnY=' --compressed


type alias ContentResponse =
    { collections : List Collection
    , resource : Maybe Resource
    }


type Resource
    = VideoR VideoResource
    | ShowR ShowResource


type Collection
    = InlineSeason InlineSeasonData
    | InlineShow InlineShowData
    | InlineVideo InlineVideoData
    | InlineMultiple InlineMultipleData
    | StubSeason StubData
    | StubShow StubData
    | StubVideo StubData
    | StubMultiple StubData


type alias InlineSeasonData =
    { title : String
    , content : List SeasonContent
    , slug : String
    }


type alias InlineShowData =
    { title : String
    , content : List ShowContent
    , slug : String
    }


type alias InlineVideoData =
    { title : String
    , content : List VideoContent
    , slug : String
    }


type alias InlineMultipleData =
    { title : String
    , content : List MultipleContent
    , slug : String
    }


type MultipleContent
    = ShowM ShowContent
    | VideoM VideoContent


type alias StubSeasonData =
    { title : String
    , slug : String
    }


type alias StubData =
    { title : String
    , slug : String
    }


bySlug : String -> Collection -> Bool
bySlug slug collection =
    case collection of
        InlineShow y ->
            y.slug == slug

        InlineVideo y ->
            y.slug == slug

        InlineSeason y ->
            y.slug == slug

        InlineMultiple y ->
            y.slug == slug

        StubShow y ->
            y.slug == slug

        StubVideo y ->
            y.slug == slug

        StubSeason y ->
            y.slug == slug

        StubMultiple y ->
            y.slug == slug


isStub : Collection -> Bool
isStub collection =
    case collection of
        InlineShow _ ->
            False

        InlineVideo _ ->
            False

        InlineSeason _ ->
            False

        InlineMultiple _ ->
            False

        StubShow _ ->
            True

        StubVideo _ ->
            True

        StubSeason _ ->
            True

        StubMultiple _ ->
            True


type alias SeasonContent =
    { title : String
    , ordinal : Int
    , flags : SeasonFlags
    }


type alias ShowContent =
    { title : String
    , imageUrl : String
    , slug : String
    }


type alias VideoContent =
    { title : String
    , imageUrl : String
    , slug : String
    , secondsWatched : Maybe Int
    , expireDate : Maybe Posix
    , titleSortable : String
    , duration : Int
    , premiereDate : Posix
    , descriptionLong : String
    , descriptionShort : String
    , videoType : VideoType
    , parent : Parent
    , flags : ContentFlags
    }


type alias ContentFlags =
    { hasCaptions : Bool
    , isExpiringSoon : Bool
    , isFullyWatched : Maybe Bool
    , isMvod : Bool
    , isNew : Bool
    }


type alias SeasonFlags =
    { hasAssets : Bool
    , hasEpisodes : Bool
    }


decodeContentResponse : Decoder ContentResponse
decodeContentResponse =
    succeed ContentResponse
        |> required "collections" (list decodeContentResponseCollection)
        |> optional "resource" (maybe decodeResource) Nothing


decodeResource : Decoder Resource
decodeResource =
    let
        toDecoder item_type =
            case item_type of
                "video" ->
                    map VideoR decodeVideoResource

                "show" ->
                    map ShowR decodeShowResource

                _ ->
                    fail "failed on item_type 1"
    in
    succeed toDecoder
        |> required "item_type" string
        |> resolve


decodeMultipleContent : Decoder MultipleContent
decodeMultipleContent =
    let
        toDecoder item_type =
            case item_type of
                "video" ->
                    map VideoM decodeVideoContent

                "show" ->
                    map ShowM decodeShowContent

                _ ->
                    fail "failed on item_type 2"
    in
    succeed toDecoder
        |> required "item_type" string
        |> resolve


decodeContentResponseCollection : Decoder Collection
decodeContentResponseCollection =
    let
        toDecoder item_type collection_type =
            case item_type of
                "season" ->
                    case collection_type of
                        "inline" ->
                            map InlineSeason decodeInlineSeason

                        "stub" ->
                            map StubSeason decodeStub

                        _ ->
                            fail "failed on item_type 3"

                "show" ->
                    case collection_type of
                        "inline" ->
                            map InlineShow decodeInlineShow

                        "stub" ->
                            map StubShow decodeStub

                        _ ->
                            fail "failed on item_type 4"

                "video" ->
                    case collection_type of
                        "inline" ->
                            map InlineVideo decodeInlineVideo

                        "stub" ->
                            map StubVideo decodeStub

                        _ ->
                            fail "failed on item_type 5"

                "multiple" ->
                    --TODO: figure out if multiple will ever return something other than video
                    case collection_type of
                        "inline" ->
                            map InlineMultiple decodeInlineMultiple

                        "stub" ->
                            map StubVideo decodeStub

                        _ ->
                            fail "failed on item_type 6"

                "skylab-collection" ->
                    case collection_type of
                        "inline" ->
                            map StubShow decodeStub

                        "stub" ->
                            map StubVideo decodeStub

                        _ ->
                            fail "failed on item_type 7"

                _ ->
                    fail "failed on item_type 8"
    in
    succeed toDecoder
        |> required "item_type" string
        |> required "collection_type" string
        |> resolve


decodeInlineSeason : Decoder InlineSeasonData
decodeInlineSeason =
    succeed InlineSeasonData
        |> required "title" string
        |> required "content" (list decodeSeasonContent)
        |> required "slug" string


decodeInlineShow : Decoder InlineShowData
decodeInlineShow =
    succeed InlineShowData
        |> required "title" string
        |> required "content" (list decodeShowContent)
        |> required "slug" string


decodeInlineVideo : Decoder InlineVideoData
decodeInlineVideo =
    succeed InlineVideoData
        |> required "title" string
        |> required "content" (list decodeVideoContent)
        |> required "slug" string


decodeInlineMultiple : Decoder InlineMultipleData
decodeInlineMultiple =
    succeed InlineMultipleData
        |> required "title" string
        |> required "content" (list decodeMultipleContent)
        |> required "slug" string


decodeStub : Decoder StubData
decodeStub =
    succeed StubSeasonData
        |> required "title" string
        |> required "slug" string


decodeSeasonContent : Decoder SeasonContent
decodeSeasonContent =
    succeed SeasonContent
        |> required "title" string
        |> required "ordinal" int
        |> required "flags" decodeSeasonFlags


decodeShowContent : Decoder ShowContent
decodeShowContent =
    succeed ShowContent
        |> required "title" string
        |> optionalAt [ "images", "show-poster2x3" ] string ""
        |> required "slug" string


decodeVideoContent : Decoder VideoContent
decodeVideoContent =
    succeed VideoContent
        |> required "title" string
        |> optionalAt [ "images", "asset-mezzanine-16x9" ] string ""
        |> required "slug" string
        |> required "seconds_watched" (maybe int)
        |> required "expire_date" (maybe datetime)
        |> required "title_sortable" string
        |> required "duration" int
        |> required "premiere_date" datetime
        |> required "description_long" string
        |> required "description_short" string
        |> required "video_type" decodeVideoType
        |> required "parent" decodeParent
        |> required "flags" decodeContentFlags


decodeContentFlags : Decoder ContentFlags
decodeContentFlags =
    succeed ContentFlags
        |> required "has_captions" bool
        |> required "is_expiring_soon" bool
        |> required "is_fully_watched" (maybe bool)
        |> required "is_mvod" bool
        |> required "is_new" bool


decodeSeasonFlags : Decoder SeasonFlags
decodeSeasonFlags =
    succeed SeasonFlags
        |> required "has_assets" bool
        |> required "has_episodes" bool


dummyVideoContent : VideoContent
dummyVideoContent =
    { title = "dummy_videocontent"
    , imageUrl = "dummy_imageUrl"
    , slug = "dummy_slug"
    , secondsWatched = Nothing
    , expireDate = Nothing
    , titleSortable = "dummy_titleSortable"
    , duration = 0
    , premiereDate = millisToPosix 0
    , descriptionLong = "dummy_descriptionLong"
    , descriptionShort = "dummy_descriptionShort"
    , videoType = dummyVideoType
    , parent = dummyParent
    , flags = dummyFlags
    }


dummyFlags : ContentFlags
dummyFlags =
    { hasCaptions = False
    , isExpiringSoon = False
    , isFullyWatched = Nothing
    , isMvod = False
    , isNew = False
    }


dummyVideoType : VideoType
dummyVideoType =
    Preview


dummyParent : Parent
dummyParent =
    Special
        { cid = "dummy_cid"
        , slug = "dummy_slug"
        , show = dummySeasonShow
        , title = "dummy_parent"
        }


dummySeasonShow : SeasonShow
dummySeasonShow =
    { displayEpisodeNumber = True
    , audienceScope = "dummy_audienceScope"
    , trackingGaEvent = "dummy_trackingGaEvent"
    , cid = "dummy_cid"
    , funderMessage = "dummy_funderMessage"
    , slug = "dummy_slug"
    , franchise = Nothing
    , title = "dummy_seasonshow"
    , seasonsCount = 0
    , trackingGaPage = "dummy_trackingGaPage"
    }


dummyShowContent : ShowContent
dummyShowContent =
    { title = "", imageUrl = "", slug = "" }


dummyShowItemList : List ShowContent
dummyShowItemList =
    [ dummyShowContent, dummyShowContent ]


dummyVideoItemList : List VideoContent
dummyVideoItemList =
    [ dummyVideoContent, dummyVideoContent ]
