module Data.ShowResource exposing (Link, ResourceFranchise, ResourceFranchiseImages, ResourceGenre, ResourceImages, ShowResource, decodeShowResource)

import Json.Decode exposing (Decoder, int, list, maybe, string, succeed)
import Json.Decode.Extra exposing (datetime)
import Json.Decode.Pipeline exposing (required)
import Time exposing (Posix)
import Json.Decode.Pipeline exposing (optional)


type alias ShowResource =
    { trackingGaPage : String
    , hashtag : String
    , url : String
    , descriptionLong : String
    , genre : ResourceGenre
    , premiereDate : Maybe Posix
    , itemType : String
    , nolaRoot : String
    , seasonsCount : Int
    , slug : String
    , titleSortable : String

    -- , audienceScope : List String
    , franchise : Maybe ResourceFranchise
    , descriptionShort : String
    , trackingGaEvent : String
    , links : List Link
    , title : String
    , images : ResourceImages
    , cid : String
    , funderMessage : String
    }


type alias Link =
    { updatedAt : String
    , value : String
    , profile : String
    }


decodeLink : Decoder Link
decodeLink =
    succeed Link
        |> required "updated_at" string
        |> required "value" string
        |> required "profile" string


type alias ResourceGenre =
    { slug : String
    , title : String
    , cid : String
    , url : String
    }


type alias ResourceFranchiseImages =
    { franchisePoster2x3 : String
    , franchiseWhiteLogo : String
    , franchiseMezzanine16x9 : String
    , franchiseBackgroundImage : String
    , franchiseShowcase : String
    , franchiseBanner : String
    , whiteLogo41 : String
    , franchiseStackImage : String
    , franchiseLogo : String
    }


type alias ResourceFranchise =
    { slug : String
    , images : ResourceFranchiseImages
    , descriptionLong : String
    , url : String
    , title : String
    , cid : String
    }


type alias ResourceImages =
    { showMezzanine16x9 : String

    -- , whiteLogo41 : String
    -- , colorLogo41 : String
    -- , blackLogo41 : String
    , showPoster2x3 : String
    }


decodeShowResource : Decoder ShowResource
decodeShowResource =
    succeed ShowResource
        |> required "tracking_ga_page" string
        |> required "hashtag" string
        |> required "url" string
        |> required "description_long" string
        |> required "genre" decodeResourceGenre
        |> required "premiere_date" (maybe datetime)
        |> required "item_type" string
        |> required "nola_root" string
        |> required "seasons_count" int
        |> required "slug" string
        |> required "title_sortable" string
        -- |> required "audience" (list decodeAudienceScope)
        |> required "franchise" (maybe decodeResourceFranchise)
        |> required "description_short" string
        |> required "tracking_ga_event" string
        |> required "links" (list decodeLink)
        |> required "title" string
        |> required "images" decodeResourceImages
        |> required "cid" string
        |> optional "funder_message" string ""


decodeResourceGenre : Decoder ResourceGenre
decodeResourceGenre =
    succeed ResourceGenre
        |> required "slug" string
        |> required "title" string
        |> required "cid" string
        |> required "url" string


decodeResourceFranchiseImages : Decoder ResourceFranchiseImages
decodeResourceFranchiseImages =
    succeed ResourceFranchiseImages
        |> required "franchise-poster2x3" string
        |> required "franchise-white-logo" string
        |> required "franchise-mezzanine16x9" string
        |> required "franchise-background-image" string
        |> required "franchise-showcase" string
        |> required "franchise-banner" string
        |> required "white-logo-41" string
        |> required "franchise-stack-image" string
        |> required "franchise-logo" string


decodeResourceFranchise : Decoder ResourceFranchise
decodeResourceFranchise =
    succeed ResourceFranchise
        |> required "slug" string
        |> required "images" decodeResourceFranchiseImages
        |> required "description_long" string
        |> required "url" string
        |> required "title" string
        |> required "cid" string


decodeResourceImages : Decoder ResourceImages
decodeResourceImages =
    succeed ResourceImages
        |> required "show-mezzanine16x9" string
        -- |> required "white-logo-41" (string)
        -- |> required "color-logo-41" (string)
        -- |> required "black-logo-41" (string)
        |> required "show-poster2x3" string
