module Data.MainView exposing (MainViewModel, MainViewMsg(..), MainViewSidebarItem, Page(..), SettingsPageData, mainViewUpdate)

import Data.DetailsPage exposing (DetailsPage)
import Data.Device exposing (DeviceId, DeviceType)
import Data.FavoriteStations exposing (FavoriteStationsCollectionContent)
import Data.FocusLocation exposing (FocusLocation)
import Data.Home exposing (HomeData)
import Data.MyList exposing (MyListData)
import Data.Profile exposing (ProfileResource)
import Data.Search exposing (SearchPageData)
import Data.Settings exposing (SettingsCard)
import Data.ShowsPage exposing (ShowPageData)
import List.Zipper exposing (Zipper)
import RemoteData exposing (WebData)
import Stack exposing (Stack)


type alias MainViewSidebarItem =
    { title : String
    , iconUrl : String
    }


type alias MainViewModel =
    { homeData : HomeData
    , showsPageData : ShowPageData
    , searchPageData : SearchPageData
    , myListData : MyListData
    , settingsData : SettingsPageData
    , focusLocation : FocusLocation
    , stack : Stack DetailsPage
    , cat : WebData String
    , currentPage : Page
    , previousPage : Page
    , station : FavoriteStationsCollectionContent
    , profile : ProfileResource
    , deviceId : DeviceId
    , deviceType : DeviceType
    }


type MainViewMsg
    = NoOp


type Page
    = Home
    | Shows
    | Search
    | MyList
    | MyStation
    | Profile
    | Cats
    | Stack


mainViewUpdate : MainViewMsg -> MainViewModel -> ( MainViewModel, Cmd MainViewMsg )
mainViewUpdate msg model =
    ( model, Cmd.none )


type alias SettingsPageData =
    Zipper SettingsCard



-- TODO:
-- About
-- PrivacyPolicy
-- Terms of Service
-- Closed Caption Support
-- ChangeStation
-- PassportUpsell
-- Logout
