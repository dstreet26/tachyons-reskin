module Data.ErrorCodes exposing (ErrorCode)


type alias ErrorCode =
    ( Int, String )
