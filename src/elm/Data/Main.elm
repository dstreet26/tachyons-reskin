module Data.Main exposing (AlternateDirection(..), Model, PlaceholderDirection(..), ViewModel(..))

import Browser.Navigation exposing (Key)
import Data.ActivationCode exposing (AuthCode)
import Data.ContentResponse exposing (ContentResponse)
import Data.Device exposing (DeviceId, DeviceType)
import Data.FavoriteStations exposing (FavoriteStations)
import Data.MainView exposing (MainViewModel)
import Data.Profile exposing (ProfileResponse)
import Data.Video exposing (VideoModel)
import RemoteData exposing (WebData)
import Route exposing (Route)


type ViewModel
    = Splash (Maybe DeviceId)
    | Activation DeviceId (WebData AuthCode)
    | ActivationCode DeviceId (WebData AuthCode)
    | LoadingMain DeviceId (WebData FavoriteStations) (WebData ProfileResponse)
    | MainView MainViewModel
    | LoadingShow MainViewModel (WebData ContentResponse)
    | LoadingVideoDetails MainViewModel (WebData ContentResponse)
    | LoadingVideoPlayback MainViewModel (WebData ContentResponse)
    | Video MainViewModel VideoModel


type alias Model =
    { urlKey : Key
    , initialRoute : Maybe Route
    , deviceType : DeviceType
    , currentPage : ViewModel
    , error : String
    }


type PlaceholderDirection
    = Right
    | Left


type AlternateDirection
    = Up
    | Down
