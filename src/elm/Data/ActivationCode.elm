module Data.ActivationCode exposing (ActivationCodeStatusResponse, ActivationCodeStatusResponseDevice, AuthCode, decodeActivationCodeStatusResponse, decodeAuthCodeResponse)

import Json.Decode exposing (Decoder, int, maybe, string, succeed)
import Json.Decode.Pipeline exposing (required)


type alias AuthCode =
    { errorCode : Int
    , authCode : String
    , errorMessage : String
    }


decodeAuthCodeResponse : Decoder AuthCode
decodeAuthCodeResponse =
    succeed AuthCode
        |> required "errorCode" int
        |> required "authCode" string
        |> required "errorMessage" string


type alias ActivationCodeStatusResponse =
    { errorCode : Int
    , device : Maybe ActivationCodeStatusResponseDevice
    , errorMessage : String
    }


type alias ActivationCodeStatusResponseDevice =
    { idS : String
    , uID : String
    }


decodeActivationCodeStatusResponse : Decoder ActivationCodeStatusResponse
decodeActivationCodeStatusResponse =
    succeed ActivationCodeStatusResponse
        |> required "errorCode" int
        |> required "device" (maybe decodeActivationCodeStatusResponseDevice)
        |> required "errorMessage" string


decodeActivationCodeStatusResponseDevice : Decoder ActivationCodeStatusResponseDevice
decodeActivationCodeStatusResponseDevice =
    succeed ActivationCodeStatusResponseDevice
        |> required "id_s" string
        |> required "UID" string
