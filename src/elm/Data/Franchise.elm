module Data.Franchise exposing (Franchise, decodeFranchise)

import Json.Decode exposing (Decoder, string, succeed)
import Json.Decode.Pipeline exposing (required)
import Json.Decode.Pipeline exposing (optional)


type alias Franchise =
    { funderMessage : String
    , cid : String
    , slug : String
    , title : String
    }


decodeFranchise : Decoder Franchise
decodeFranchise =
    succeed Franchise
        |> optional "funder_message" string ""
        |> required "cid" string
        |> required "slug" string
        |> required "title" string
