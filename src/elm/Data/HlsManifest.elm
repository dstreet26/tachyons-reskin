module Data.HlsManifest exposing
    ( HlsManifest
    , HlsState(..)
    , Ing
    , Level
    , LevelAttrs
    , Parsing
    , Stats
    , SubtitleTrack
    , SubtitleTrackAttrs
    , dummyLevel
    , hlsManifest
    )

import Json.Decode as Decode
import Json.Decode.Pipeline as Jpipe


type HlsState
    = Uninitialized
    | Attached
    | Parsed HlsManifest


type alias HlsManifest =
    { levels : List Level
    , audioTracks : List Decode.Value
    , subtitleTracks : List SubtitleTrack
    , firstLevel : Int
    , stats : Stats
    , audio : Bool
    , video : Bool
    , altAudio : Bool
    }


dummyLevel =
    { attrs = dummyLevelAttrs
    , audioCodec = ""
    , bitrate = 0
    , codecSet = ""
    , height = 0
    , id = 0
    , videoCodec = ""
    , width = 0
    , unknownCodecs = []
    , fragmentError = 0
    , loadError = 0
    , realBitrate = 0
    , textGroupIDS = []
    , url = []
    , urlID = 0
    }


type alias Level =
    { attrs : LevelAttrs
    , audioCodec : String
    , bitrate : Int
    , codecSet : String
    , height : Int
    , id : Int
    , videoCodec : String
    , width : Int
    , unknownCodecs : List Decode.Value
    , fragmentError : Int
    , loadError : Int
    , realBitrate : Int
    , textGroupIDS : List String
    , url : List String
    , urlID : Int
    }


type alias LevelAttrs =
    { bandwidth : String
    , averageBandwidth : String
    , resolution : String
    , codecs : String
    , subtitles : String
    }


dummyLevelAttrs =
    { bandwidth = ""
    , averageBandwidth = ""
    , resolution = "auto"
    , codecs = ""
    , subtitles = ""
    }


type alias Stats =
    { aborted : Bool
    , loaded : Int
    , retry : Int
    , total : Int
    , chunkCount : Int
    , bwEstimate : Int
    , loading : Ing
    , parsing : Parsing
    , buffering : Ing
    }


type alias Ing =
    { start : Float
    , first : Float
    , end : Float
    }


type alias Parsing =
    { start : Float
    , end : Int
    }


type alias SubtitleTrack =
    { attrs : SubtitleTrackAttrs
    , bitrate : Int
    , id : Int
    , groupID : String
    , name : String
    , subtitleTrackType : String
    , default : Bool
    , autoselect : Bool
    , forced : Bool
    , lang : String
    , url : String
    }


type alias SubtitleTrackAttrs =
    { uri : String
    , attrsTYPE : String
    , groupID : String
    , language : String
    , name : String
    , default : String
    , autoselect : String
    , forced : String
    , characteristics : String
    }


hlsManifest : Decode.Decoder HlsManifest
hlsManifest =
    Decode.succeed HlsManifest
        |> Jpipe.required "levels" (Decode.list level)
        |> Jpipe.required "audioTracks" (Decode.list Decode.value)
        |> Jpipe.required "subtitleTracks" (Decode.list subtitleTrack)
        |> Jpipe.required "firstLevel" Decode.int
        |> Jpipe.required "stats" stats
        |> Jpipe.required "audio" Decode.bool
        |> Jpipe.required "video" Decode.bool
        |> Jpipe.required "altAudio" Decode.bool


level : Decode.Decoder Level
level =
    Decode.succeed Level
        |> Jpipe.required "attrs" levelAttrs
        |> Jpipe.required "audioCodec" Decode.string
        |> Jpipe.required "bitrate" Decode.int
        |> Jpipe.required "codecSet" Decode.string
        |> Jpipe.required "height" Decode.int
        |> Jpipe.required "id" Decode.int
        |> Jpipe.required "videoCodec" Decode.string
        |> Jpipe.required "width" Decode.int
        |> Jpipe.required "unknownCodecs" (Decode.list Decode.value)
        |> Jpipe.required "fragmentError" Decode.int
        |> Jpipe.required "loadError" Decode.int
        |> Jpipe.required "realBitrate" Decode.int
        |> Jpipe.required "textGroupIds" (Decode.list Decode.string)
        |> Jpipe.required "url" (Decode.list Decode.string)
        |> Jpipe.required "_urlId" Decode.int


levelAttrs : Decode.Decoder LevelAttrs
levelAttrs =
    Decode.succeed LevelAttrs
        |> Jpipe.required "BANDWIDTH" Decode.string
        |> Jpipe.required "AVERAGE-BANDWIDTH" Decode.string
        |> Jpipe.required "RESOLUTION" Decode.string
        |> Jpipe.required "CODECS" Decode.string
        |> Jpipe.required "SUBTITLES" Decode.string


stats : Decode.Decoder Stats
stats =
    Decode.succeed Stats
        |> Jpipe.required "aborted" Decode.bool
        |> Jpipe.required "loaded" Decode.int
        |> Jpipe.required "retry" Decode.int
        |> Jpipe.required "total" Decode.int
        |> Jpipe.required "chunkCount" Decode.int
        |> Jpipe.required "bwEstimate" Decode.int
        |> Jpipe.required "loading" ing
        |> Jpipe.required "parsing" parsing
        |> Jpipe.required "buffering" ing


ing : Decode.Decoder Ing
ing =
    Decode.succeed Ing
        |> Jpipe.required "start" Decode.float
        |> Jpipe.required "first" Decode.float
        |> Jpipe.required "end" Decode.float


parsing : Decode.Decoder Parsing
parsing =
    Decode.succeed Parsing
        |> Jpipe.required "start" Decode.float
        |> Jpipe.required "end" Decode.int


subtitleTrack : Decode.Decoder SubtitleTrack
subtitleTrack =
    Decode.succeed SubtitleTrack
        |> Jpipe.required "attrs" subtitleTrackAttrs
        |> Jpipe.required "bitrate" Decode.int
        |> Jpipe.required "id" Decode.int
        |> Jpipe.required "groupId" Decode.string
        |> Jpipe.required "name" Decode.string
        |> Jpipe.required "type" Decode.string
        |> Jpipe.required "default" Decode.bool
        |> Jpipe.required "autoselect" Decode.bool
        |> Jpipe.required "forced" Decode.bool
        |> Jpipe.required "lang" Decode.string
        |> Jpipe.required "url" Decode.string


subtitleTrackAttrs : Decode.Decoder SubtitleTrackAttrs
subtitleTrackAttrs =
    Decode.succeed SubtitleTrackAttrs
        |> Jpipe.required "URI" Decode.string
        |> Jpipe.required "TYPE" Decode.string
        |> Jpipe.required "GROUP-ID" Decode.string
        |> Jpipe.required "LANGUAGE" Decode.string
        |> Jpipe.required "NAME" Decode.string
        |> Jpipe.required "DEFAULT" Decode.string
        |> Jpipe.required "AUTOSELECT" Decode.string
        |> Jpipe.required "FORCED" Decode.string
        |> Jpipe.required "CHARACTERISTICS" Decode.string
