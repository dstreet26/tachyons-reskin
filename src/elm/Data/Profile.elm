module Data.Profile exposing (ProfileResource, ProfileResponse, decodeProfile)

import Json.Decode exposing (Decoder, bool, list, maybe, string, succeed)
import Json.Decode.Pipeline exposing (required)



-- curl 'https://content.services.pbs.org/v3/samsungtv/profile/eab31436-576b-42bc-a24f-92b4d04c97b1/?nocache=1551041406912' -H 'User-Agent: Mozilla/5.0 (Macintosh; Intel Mac OS X 10.13; rv:66.0) Gecko/20100101 Firefox/66.0' -H 'Accept: application/json, text/plain, */*' -H 'Accept-Language: en-US,en;q=0.5' --compressed -H 'Referer: https://otter-tizen-ga-prod.pbs.org/home' -H 'Authorization: Basic a2lkc3NhbXN1bmd0djpCQ1hiQTV2SXhqOFNGNnY=' -H 'X-PBS-PlatformVersion: 3.0.0' -H 'Origin: https://otter-tizen-ga-prod.pbs.org' -H 'Connection: keep-alive'


type alias ProfileResponse =
    { resource : ProfileResource
    }


type alias ProfileResource =
    { loginProviders : List String
    , thumbnailUrl : Maybe String
    , itemType : String
    , vppaAccepted : Bool
    , provider : String
    , lastName : String
    , email : String
    , birthDate : Maybe String
    , pid : String
    , hasMemberships : Bool
    , vppaStatus : String
    , firstName : String
    , vppaLastUpdated : String
    }


decodeProfile : Decoder ProfileResponse
decodeProfile =
    succeed ProfileResponse
        |> required "resource" decodeProfileResource


decodeProfileResource : Decoder ProfileResource
decodeProfileResource =
    succeed ProfileResource
        |> required "login_providers" (list string)
        |> required "thumbnail_url" (maybe string)
        |> required "item_type" string
        |> required "vppa_accepted" bool
        |> required "provider" string
        |> required "last_name" string
        |> required "email" string
        |> required "birth_date" (maybe string)
        |> required "pid" string
        |> required "has_memberships" bool
        |> required "vppa_status" string
        |> required "first_name" string
        |> required "vppa_last_updated" string
