module DemoData exposing (ProgramFlags, homeBrowseButtons, homeHeroButtons, initialSettingsCards, playWatchButtons)

import Data.Row exposing (HeroButtonItem(..), Row(..), RowHolder)
import Data.Settings exposing (SettingCardType(..), SettingsCard)
import List.Zipper exposing (Zipper, fromCons)


type alias ProgramFlags =
    { deviceId : Maybe String
    , userId : Maybe String
    , deviceType : String
    , buildNumber : Maybe String
    }


initialSettingsCards : Zipper SettingsCard
initialSettingsCards =
    let
        currentStation =
            { settingCardType = CurrentStation { imageSource = "https://image.pbs.org/stations/weta-white-logo-JQgQDpP.png" }
            }

        stationItem =
            { settingCardType = StationItem { name = "KOTH", isActivated = True }
            }

        loggedInItem =
            { settingCardType = LoggedIn { email = "withpassport1@gmail.com" }
            }

        pp =
            { settingCardType = About { title = "Privacy Policy" } }

        tos =
            { settingCardType = About { title = "Terms Of Service" } }

        cc =
            { settingCardType = About { title = "Closed Caption Support" } }
    in
    fromCons currentStation [ stationItem, loggedInItem, pp, tos, cc ]


homeHeroButtons : RowHolder
homeHeroButtons =
    { row = HeroButtonRow playWatchButtons
    , title = Nothing
    , horizontalScrollPosition = Nothing
    }


playWatchButtons : Zipper HeroButtonItem
playWatchButtons =
    fromCons (Play { text = "Watch", icon = "play" }) [ Add { text = "Add", icon = "plus" } ]


homeBrowseButtons : RowHolder
homeBrowseButtons =
    { row =
        BrowseButtonRow
            (fromCons
                { title = "Local" }
                [ { title = "Drama" }
                , { title = "History" }
                , { title = "Food" }
                , { title = "Culture" }
                , { title = "News and Public Affairs" }
                , { title = "Home & How-To" }
                ]
            )
    , title = Just "Browse Shows:"
    , horizontalScrollPosition = Nothing
    }
