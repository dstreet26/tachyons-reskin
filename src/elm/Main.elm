module Main exposing (main)

import Browser exposing (Document, UrlRequest)
import Browser.Events
import Browser.Navigation exposing (Key)
import Data.ContentResponse
    exposing
        ( MultipleContent(..)
        )
import Data.DetailsPage exposing (DetailsPage(..), SubNav(..), VideoDetailsFocusLocation(..))
import Data.Device exposing (DeviceType(..))
import Data.FavoriteStations exposing (FavoriteStationsCollectionContent)
import Data.FocusLocation exposing (FocusLocation(..))
import Data.HlsManifest exposing (..)
import Data.Main exposing (Model, PlaceholderDirection(..), ViewModel(..))
import Data.MainView
    exposing
        ( MainViewModel
        , Page(..)
        , mainViewUpdate
        )
import Data.MyList exposing (MyListFocusLocation(..))
import Data.Profile exposing (ProfileResource)
import Data.Row exposing (Row(..), RowHolder)
import Data.Search exposing (..)
import Data.ShowsPage exposing (..)
import Data.Video
    exposing
        ( PlayerState(..)
        , VideoElementMsg(..)
        , VideoEvent(..)
        , VideoFocusLocation(..)
        , decodeDuration
        , decodePosition
        , pushVideoEvent
        , videoUpdate
        )
import DemoData exposing (ProgramFlags)
import Html exposing (Attribute, Html, div, text, video)
import Html.Attributes as Attr exposing (class, id, style)
import Html.Events exposing (on)
import Json.Decode as Decode exposing (Decoder, decodeValue, errorToString, field, string, succeed)
import List exposing (head)
import List.Zipper exposing (current)
import Msg exposing (..)
import Page.DetailsPage exposing (responseToDetailsPage)
import Page.MainView
    exposing
        ( getCurrentFeaturedVideo
        , incomingSearchData
        , initializeEverything
        , navigatePageContentHorizontal
        , navigatePageContentVertical
        , navigateSidebarContent
        , pushStack
        )
import Page.MyList exposing (..)
import Page.Search exposing (..)
import Page.Video
    exposing
        ( handleVideoPlayerEnter
        , handleVideoPlayerHorizontal
        , handleVideoPlayerVertical
        , initializeLevels
        , initializeVideoPlaybackModel
        )
import Platform.Cmd exposing (Cmd)
import Ports
import Process
import Random
import RemoteData exposing (RemoteData(..))
import Requests exposing (..)
import Route exposing (Route(..), route)
import Stack
import Task exposing (perform)
import Url exposing (Url)
import Url.Parser exposing (parse)
import Uuid exposing (uuidGenerator)
import Views.ActivationCode exposing (viewActivationCode)
import Views.MainView exposing (viewMainPage)
import Views.Other exposing (viewActivation, viewSplash)
import Views.Video exposing (viewVideoPage)


getUuid : Cmd Msg
getUuid =
    Random.generate GotUuid uuidGenerator


main : Platform.Program ProgramFlags Model Msg
main =
    Browser.application
        { init = init
        , update = update
        , subscriptions = subscriptions
        , view = view
        , onUrlRequest = UrlRequest
        , onUrlChange = UrlChange
        }


init : ProgramFlags -> Url -> Key -> ( Model, Cmd Msg )
init flags url key =
    let
        ( thePage, theCmd ) =
            case flags.deviceId of
                Nothing ->
                    ( Splash Nothing, getUuid )

                Just deviceId ->
                    case flags.userId of
                        Nothing ->
                            ( Splash (Just deviceId), getActivationCodeStatus deviceId )

                        Just userId ->
                            ( Splash (Just deviceId), getProfileData userId )

        newModel : Model
        newModel =
            { urlKey = key
            , initialRoute = parse route url
            , deviceType = getDeviceType flags.deviceType
            , currentPage = thePage
            , error = ""
            }
    in
    ( newModel, Process.sleep 1000 |> perform (always (DeferCommandMessage theCmd)) )


getDeviceType : String -> DeviceType
getDeviceType x =
    case x of
        "tizen2017" ->
            Tizen2017

        "tizen" ->
            Tizen

        "VIZIO" ->
            VIZIO

        _ ->
            Other


update : Msg -> Model -> ( Model, Cmd Msg )
update msg model =
    case msg of
        GenericJsonDecodeError err ->
            ( { model | error = errorToString err }, Cmd.none )

        UrlChange _ ->
            ( model, Cmd.none )

        UrlRequest _ ->
            ( model, Cmd.none )

        DeferCommandMessage cmd ->
            ( model, cmd )

        GotUuid uuid ->
            let
                id =
                    Uuid.toString uuid
            in
            case model.currentPage of
                Splash Nothing ->
                    ( { model | currentPage = Activation id NotAsked }
                    , Cmd.batch
                        [ Ports.setDeviceId id
                        , getActivationCode id
                        ]
                    )

                ActivationCode _ _ ->
                    ( { model | currentPage = ActivationCode id NotAsked }
                    , Cmd.batch
                        [ Ports.setDeviceId id
                        , getActivationCode id
                        ]
                    )

                _ ->
                    ( model, Cmd.none )

        GotActivationCodeStatus response ->
            case response of
                RemoteData.Failure _ ->
                    case model.currentPage of
                        Splash (Just deviceId) ->
                            ( { model | currentPage = Activation deviceId NotAsked }, getActivationCode deviceId )

                        ActivationCode deviceId authCode ->
                            ( { model | currentPage = ActivationCode deviceId authCode }, Process.sleep 1000 |> perform (\_ -> CheckActivation) )

                        _ ->
                            ( model, Cmd.none )

                RemoteData.Loading ->
                    ( model, Cmd.none )

                NotAsked ->
                    ( model, Cmd.none )

                RemoteData.Success activationCodeStatusResponse ->
                    if activationCodeStatusResponse.errorCode == 0 then
                        case activationCodeStatusResponse.device of
                            Just device ->
                                ( { model | currentPage = LoadingMain device.uID NotAsked NotAsked }, getProfileData device.uID )

                            -- TODO: this looks like punting
                            Nothing ->
                                ( model, Cmd.none )

                    else
                        case model.currentPage of
                            ActivationCode deviceId authCode ->
                                ( { model | currentPage = ActivationCode deviceId authCode }, Process.sleep 1000 |> perform (\_ -> CheckActivation) )

                            _ ->
                                ( model, Cmd.none )

        GotActivationCode response ->
            case model.currentPage of
                Activation deviceId _ ->
                    ( { model | currentPage = Activation deviceId response }, Cmd.none )

                ActivationCode deviceId _ ->
                    ( { model | currentPage = ActivationCode deviceId response }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        HlsManifestReceived manifest ->
            case model.currentPage of
                Video mv vm ->
                    let
                        newLevels =
                            initializeLevels manifest.levels
                    in
                    ( { model | currentPage = Video mv { vm | levels = newLevels, playerState = Paused, hlsState = Parsed manifest } }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        HlsMediaAttached ->
            case model.currentPage of
                Video mv vm ->
                    ( { model | currentPage = Video mv { vm | hlsState = Attached } }, pushVideoEvent (LoadManifest vm.videoSource) )

                _ ->
                    ( model, Cmd.none )

        MainViewMessage mainViewMessage ->
            case model.currentPage of
                MainView mainViewModel ->
                    let
                        ( _, subCmd ) =
                            mainViewUpdate mainViewMessage mainViewModel
                    in
                    ( { model | currentPage = MainView mainViewModel }, Cmd.map MainViewMessage subCmd )

                _ ->
                    ( model, Cmd.none )

        VideoPageMessage videoElementMessage ->
            case model.currentPage of
                Video mainViewModel videoModel ->
                    let
                        ( newVideoModel, subCmd ) =
                            videoUpdate videoElementMessage videoModel
                    in
                    ( { model | currentPage = Video mainViewModel newVideoModel }, Cmd.map VideoPageMessage subCmd )

                _ ->
                    ( model, Cmd.none )

        CheckActivation ->
            case model.currentPage of
                Splash maybeDeviceId ->
                    case maybeDeviceId of
                        Just deviceId ->
                            ( model, getActivationCodeStatus deviceId )

                        Nothing ->
                            ( model, Cmd.none )

                ActivationCode deviceId _ ->
                    ( model, getActivationCodeStatus deviceId )

                _ ->
                    ( model, Cmd.none )

        KeyPressed keyCode ->
            handleKeyPress model keyCode

        GotGif response ->
            case model.currentPage of
                MainView mainViewModel ->
                    ( { model | currentPage = MainView { mainViewModel | cat = response } }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        GotProfileData result ->
            case model.currentPage of
                LoadingMain deviceId fs _ ->
                    case result of
                        RemoteData.Success profileResponse ->
                            ( { model | currentPage = LoadingMain deviceId fs result }, getFavoriteStations profileResponse.resource.pid )

                        _ ->
                            ( { model | currentPage = LoadingMain deviceId fs result }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        GotFavoriteStations result ->
            case model.currentPage of
                LoadingMain deviceId _ (RemoteData.Success profileResponse) ->
                    case result of
                        RemoteData.Success fs ->
                            case head fs.collections of
                                Just firstCollection ->
                                    case head firstCollection.content of
                                        Just mainStation ->
                                            let
                                                userId =
                                                    profileResponse.resource.pid

                                                stationId =
                                                    mainStation.cid

                                                command =
                                                    Task.attempt InitializedMainView
                                                        (Task.map3
                                                            (\home resume shows ->
                                                                initializeEverything home resume shows mainStation profileResponse.resource deviceId model.deviceType
                                                            )
                                                            (getHomeContentTask stationId)
                                                            (getResumeWatchingTask userId)
                                                            (getShowsContentTask stationId)
                                                        )
                                            in
                                            ( model, command )

                                        Nothing ->
                                            ( model, Cmd.none )

                                Nothing ->
                                    ( model, Cmd.none )

                        _ ->
                            ( model, Cmd.none )

                LoadingMain _ _ _ ->
                    ( model, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        GotFavoriteShows result ->
            case model.currentPage of
                MainView mainViewModel ->
                    ( { model | currentPage = MainView { mainViewModel | myListData = updateFavoriteShows mainViewModel.myListData result } }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        GotWatchList result ->
            case model.currentPage of
                MainView mainViewModel ->
                    ( { model | currentPage = MainView { mainViewModel | myListData = updateWatchList mainViewModel.myListData result } }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        GotViewingHistory result ->
            case model.currentPage of
                MainView mainViewModel ->
                    ( { model | currentPage = MainView { mainViewModel | myListData = updateViewingHistory mainViewModel.myListData result } }, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        InitializedMainView result ->
            case result of
                Ok mainViewModel ->
                    goWhere model mainViewModel

                Err _ ->
                    ( model, Cmd.none )

        GotSearchContent result ->
            case model.currentPage of
                MainView mainViewModel ->
                    ( { model
                        | currentPage =
                            MainView
                                (incomingSearchData mainViewModel result)
                      }
                    , Cmd.none
                    )

                _ ->
                    ( model, Cmd.none )

        GotShowDetails result ->
            case model.currentPage of
                MainView mainViewModel ->
                    case responseToDetailsPage result of
                        Just detailsPage ->
                            case detailsPage of
                                ShowDetailsPage showDetailsPage ->
                                    ( { model | currentPage = MainView (pushStack mainViewModel detailsPage) }, replaceShowDetailsUrl model.urlKey showDetailsPage.resource.slug )

                                _ ->
                                    ( model, Cmd.none )

                        Nothing ->
                            ( model, Cmd.none )

                LoadingShow mainViewModel _ ->
                    case responseToDetailsPage result of
                        Just detailsPage ->
                            case detailsPage of
                                ShowDetailsPage showDetailsPage ->
                                    ( { model | currentPage = MainView (pushStack mainViewModel detailsPage) }, replaceShowDetailsUrl model.urlKey showDetailsPage.resource.slug )

                                _ ->
                                    ( model, Cmd.none )

                        Nothing ->
                            ( model, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        GotVideoDetails result ->
            case model.currentPage of
                MainView mainViewModel ->
                    case responseToDetailsPage result of
                        Just detailsPage ->
                            case detailsPage of
                                VideoDetailsPage videoDetailsPage ->
                                    ( { model | currentPage = MainView (pushStack mainViewModel detailsPage) }, replaceVideoDetailsUrl model.urlKey videoDetailsPage.resource.slug )

                                _ ->
                                    ( model, Cmd.none )

                        Nothing ->
                            ( model, Cmd.none )

                LoadingVideoDetails mainViewModel _ ->
                    case responseToDetailsPage result of
                        Just detailsPage ->
                            case detailsPage of
                                VideoDetailsPage videoDetailsPage ->
                                    ( { model | currentPage = MainView (pushStack mainViewModel detailsPage) }, replaceVideoDetailsUrl model.urlKey videoDetailsPage.resource.slug )

                                _ ->
                                    ( model, Cmd.none )

                        Nothing ->
                            ( model, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        GotVideoPlayback result ->
            -- TODO: gotvideoplayback and gotvideodetails are similar; if the user is already on the details page, they shouldn't need to re-fetch the resource
            case responseToDetailsPage result of
                Just detailsPage ->
                    case detailsPage of
                        VideoDetailsPage videoDetailsPage ->
                            let
                                videoSource =
                                    head videoDetailsPage.resource.hlsVideos |> Maybe.map .url |> Maybe.withDefault ""

                                newVideo =
                                    initializeVideoPlaybackModel videoDetailsPage.resource.title videoDetailsPage.resource.descriptionLong videoSource videoDetailsPage.resource.closedCaptions [ dummyLevel ]
                            in
                            case model.currentPage of
                                MainView mainViewModel ->
                                    ( { model | currentPage = Video mainViewModel newVideo }, Cmd.batch [ replaceVideoPlaybackUrl model.urlKey videoDetailsPage.resource.slug, pushVideoEvent Setup ] )

                                LoadingVideoPlayback mainViewModel _ ->
                                    ( { model | currentPage = Video mainViewModel newVideo }, Cmd.batch [ replaceVideoPlaybackUrl model.urlKey videoDetailsPage.resource.slug, pushVideoEvent Setup ] )

                                _ ->
                                    ( model, Cmd.none )

                        _ ->
                            ( model, Cmd.none )

                Nothing ->
                    ( model, Cmd.none )


goWhere : Model -> MainViewModel -> ( Model, Cmd Msg )
goWhere model mainViewModel =
    let
        ( newViewModel, newCmd ) =
            case model.initialRoute of
                Nothing ->
                    ( MainView mainViewModel, replaceHomeUrl model.urlKey )

                Just route ->
                    case route of
                        HomeRoute ->
                            ( MainView mainViewModel, replaceHomeUrl model.urlKey )

                        ShowsRoute ->
                            ( MainView { mainViewModel | currentPage = Shows }, replaceShowsUrl model.urlKey )

                        MyListRoute ->
                            ( MainView { mainViewModel | currentPage = MyList }, Cmd.batch [ replaceMyListUrl model.urlKey, myListRequests mainViewModel.profile.pid model.urlKey ] )

                        ShowDetails slug ->
                            ( LoadingShow mainViewModel NotAsked, Cmd.batch [ getShowDetails mainViewModel.station.cid slug, replaceShowDetailsUrl model.urlKey slug ] )

                        VideoDetails slug ->
                            ( LoadingVideoDetails mainViewModel NotAsked, Cmd.batch [ getVideoDetails mainViewModel.profile.pid slug, replaceVideoDetailsUrl model.urlKey slug ] )

                        VideoPlayback slug ->
                            ( LoadingVideoPlayback mainViewModel NotAsked, Cmd.batch [ getVideoPlayback mainViewModel.profile.pid slug, replaceVideoPlaybackUrl model.urlKey slug ] )
    in
    ( { model | currentPage = newViewModel }, newCmd )


handleKeyPress : Model -> String -> ( Model, Cmd Msg )
handleKeyPress model keyCode =
    case keyCode of
        "ArrowUp" ->
            case model.currentPage of
                Splash _ ->
                    ( model, Cmd.none )

                Activation _ _ ->
                    ( model, Cmd.none )

                ActivationCode _ _ ->
                    ( model, Cmd.none )

                LoadingMain _ _ _ ->
                    ( model, Cmd.none )

                LoadingShow _ _ ->
                    ( model, Cmd.none )

                LoadingVideoDetails _ _ ->
                    ( model, Cmd.none )

                LoadingVideoPlayback _ _ ->
                    ( model, Cmd.none )

                MainView mainViewModel ->
                    case mainViewModel.focusLocation of
                        Sidebar ->
                            let
                                ( newModel, newCmd ) =
                                    navigateSidebarContent mainViewModel Left model.urlKey
                            in
                            ( { model | currentPage = MainView newModel }, newCmd )

                        PageContent ->
                            ( { model | currentPage = MainView (navigatePageContentVertical mainViewModel Left) }, Cmd.none )

                Video mainViewModel videoModel ->
                    let
                        ( newVideoModel, subCmd ) =
                            handleVideoPlayerVertical Left videoModel
                    in
                    ( { model | currentPage = Video mainViewModel newVideoModel }, Cmd.map VideoPageMessage subCmd )

        "ArrowDown" ->
            case model.currentPage of
                Splash _ ->
                    ( model, Cmd.none )

                Activation _ _ ->
                    ( model, Cmd.none )

                ActivationCode _ _ ->
                    ( model, Cmd.none )

                LoadingMain _ _ _ ->
                    ( model, Cmd.none )

                LoadingShow _ _ ->
                    ( model, Cmd.none )

                LoadingVideoDetails _ _ ->
                    ( model, Cmd.none )

                LoadingVideoPlayback _ _ ->
                    ( model, Cmd.none )

                MainView mainViewModel ->
                    case mainViewModel.focusLocation of
                        Sidebar ->
                            let
                                ( newModel, newCmd ) =
                                    navigateSidebarContent mainViewModel Right model.urlKey
                            in
                            ( { model | currentPage = MainView newModel }, newCmd )

                        PageContent ->
                            ( { model | currentPage = MainView (navigatePageContentVertical mainViewModel Right) }, Cmd.none )

                Video mainViewModel videoModel ->
                    let
                        ( newVideoModel, subCmd ) =
                            handleVideoPlayerVertical Right videoModel
                    in
                    ( { model | currentPage = Video mainViewModel newVideoModel }, Cmd.map VideoPageMessage subCmd )

        "ArrowLeft" ->
            case model.currentPage of
                Splash _ ->
                    ( model, Cmd.none )

                Activation _ _ ->
                    ( model, Cmd.none )

                ActivationCode _ _ ->
                    ( model, Cmd.none )

                LoadingMain _ _ _ ->
                    ( model, Cmd.none )

                LoadingShow _ _ ->
                    ( model, Cmd.none )

                LoadingVideoDetails _ _ ->
                    ( model, Cmd.none )

                LoadingVideoPlayback _ _ ->
                    ( model, Cmd.none )

                MainView mainViewModel ->
                    case mainViewModel.focusLocation of
                        Sidebar ->
                            ( model, Cmd.none )

                        PageContent ->
                            ( { model | currentPage = MainView (navigatePageContentHorizontal mainViewModel Left) }, Cmd.none )

                Video mainViewModel videoModel ->
                    let
                        ( newVideoModel, subCmd ) =
                            handleVideoPlayerHorizontal Left videoModel
                    in
                    ( { model | currentPage = Video mainViewModel newVideoModel }, Cmd.map VideoPageMessage subCmd )

        "ArrowRight" ->
            case model.currentPage of
                Splash _ ->
                    ( model, Cmd.none )

                Activation _ _ ->
                    ( model, Cmd.none )

                ActivationCode _ _ ->
                    ( model, Cmd.none )

                LoadingMain _ _ _ ->
                    ( model, Cmd.none )

                LoadingShow _ _ ->
                    ( model, Cmd.none )

                LoadingVideoDetails _ _ ->
                    ( model, Cmd.none )

                LoadingVideoPlayback _ _ ->
                    ( model, Cmd.none )

                MainView mainViewModel ->
                    case mainViewModel.focusLocation of
                        Sidebar ->
                            ( { model | currentPage = MainView { mainViewModel | focusLocation = PageContent } }, Cmd.none )

                        -- model |> setHomePageFocus PageContent
                        PageContent ->
                            ( { model | currentPage = MainView (navigatePageContentHorizontal mainViewModel Right) }, Cmd.none )

                Video mainViewModel videoModel ->
                    let
                        ( newVideoModel, subCmd ) =
                            handleVideoPlayerHorizontal Right videoModel
                    in
                    ( { model | currentPage = Video mainViewModel newVideoModel }, Cmd.map VideoPageMessage subCmd )

        "Enter" ->
            case model.currentPage of
                Video mainViewModel videoModel ->
                    let
                        ( newVideoModel, subCmd ) =
                            handleVideoPlayerEnter videoModel
                    in
                    ( { model | currentPage = Video mainViewModel newVideoModel }, Cmd.map VideoPageMessage subCmd )

                Activation deviceId webDataAuthCode ->
                    case webDataAuthCode of
                        RemoteData.Success _ ->
                            ( { model | currentPage = ActivationCode deviceId webDataAuthCode }, Process.sleep 1000 |> perform (\_ -> CheckActivation) )

                        _ ->
                            ( { model | currentPage = Activation deviceId webDataAuthCode }, Cmd.none )

                ActivationCode deviceId webDataAuthCode ->
                    ( { model | currentPage = ActivationCode deviceId webDataAuthCode }, getUuid )

                MainView mainViewModel ->
                    case mainViewModel.focusLocation of
                        Sidebar ->
                            ( { model | currentPage = MainView mainViewModel }, Cmd.none )

                        PageContent ->
                            case mainViewModel.currentPage of
                                Stack ->
                                    let
                                        current =
                                            Stack.top mainViewModel.stack
                                    in
                                    case current of
                                        Nothing ->
                                            ( { model | currentPage = MainView mainViewModel }, Cmd.none )

                                        Just x ->
                                            case x of
                                                VideoDetailsPage videoDetailsPage ->
                                                    case videoDetailsPage.focusLocation of
                                                        MainButtons ->
                                                            let
                                                                videoSource =
                                                                    head videoDetailsPage.resource.hlsVideos |> Maybe.map .url |> Maybe.withDefault ""

                                                                newVideo =
                                                                    initializeVideoPlaybackModel videoDetailsPage.resource.title videoDetailsPage.resource.descriptionLong videoSource videoDetailsPage.resource.closedCaptions [ dummyLevel ]
                                                            in
                                                            ( { model | currentPage = Video mainViewModel newVideo }, Cmd.batch [ replaceVideoPlaybackUrl model.urlKey videoDetailsPage.resource.slug, pushVideoEvent Setup ] )

                                                        _ ->
                                                            ( { model | currentPage = MainView mainViewModel }, getCommandFromStack x mainViewModel.station mainViewModel.profile )

                                                _ ->
                                                    ( { model | currentPage = MainView mainViewModel }, getCommandFromStack x mainViewModel.station mainViewModel.profile )

                                Cats ->
                                    ( { model | currentPage = MainView { mainViewModel | cat = RemoteData.Loading } }, getRandomCatGif )

                                Home ->
                                    case current mainViewModel.homeData.rows |> .row of
                                        HeroButtonRow heroButtonItemZ ->
                                            case current heroButtonItemZ of
                                                Data.Row.Play _ ->
                                                    ( model, getVideoDetails mainViewModel.profile.pid (getCurrentFeaturedVideo mainViewModel |> .slug) )

                                                -- TODO: what about Add button?
                                                Data.Row.Add _ ->
                                                    ( model, Cmd.none )

                                        _ ->
                                            ( { model | currentPage = LoadingVideoDetails mainViewModel Loading }, getCommandFromRow (current mainViewModel.homeData.rows) mainViewModel.station mainViewModel.profile )

                                Shows ->
                                    let
                                        currentShowCategory : ShowCategory
                                        currentShowCategory =
                                            current mainViewModel.showsPageData.showCategories

                                        slug =
                                            currentShowCategory.items
                                                |> current
                                                |> .slug

                                        command =
                                            getShowDetails mainViewModel.station.cid slug
                                    in
                                    ( { model | currentPage = MainView mainViewModel }
                                    , Cmd.batch [ command, replaceShowDetailsUrl model.urlKey slug ]
                                    )

                                Search ->
                                    case mainViewModel.searchPageData.focusLocation of
                                        EnteringQuery ->
                                            let
                                                searchPageData =
                                                    mainViewModel.searchPageData

                                                currentValue =
                                                    getSearchCurrentLetterValue searchPageData

                                                newQuery =
                                                    if currentValue == "clear" then
                                                        ""

                                                    else
                                                        searchPageData.query ++ currentValue
                                            in
                                            ( { model
                                                | currentPage =
                                                    MainView
                                                        { mainViewModel
                                                            | searchPageData = { searchPageData | query = newQuery }
                                                        }
                                              }
                                            , getSearchContent newQuery
                                            )

                                        BrowsingContent ->
                                            case mainViewModel.searchPageData.rows of
                                                Just rows ->
                                                    ( { model | currentPage = MainView mainViewModel }
                                                    , getCommandFromRow (current rows) mainViewModel.station mainViewModel.profile
                                                    )

                                                Nothing ->
                                                    -- TODO: should be impossible
                                                    ( model, Cmd.none )

                                MyList ->
                                    case mainViewModel.myListData.focusLocation of
                                        FavoriteShows ->
                                            case mainViewModel.myListData.favoriteShows of
                                                Success x ->
                                                    ( model, getCommandFromRow x mainViewModel.station mainViewModel.profile )

                                                _ ->
                                                    ( model, Cmd.none )

                                        Watchlist ->
                                            case mainViewModel.myListData.watchlist of
                                                Success x ->
                                                    ( model, getCommandFromRow x mainViewModel.station mainViewModel.profile )

                                                _ ->
                                                    ( model, Cmd.none )

                                        History ->
                                            case mainViewModel.myListData.history of
                                                Success x ->
                                                    ( model, getCommandFromRow x mainViewModel.station mainViewModel.profile )

                                                _ ->
                                                    ( model, Cmd.none )

                                _ ->
                                    ( model, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        "Backspace" ->
            case model.currentPage of
                Video mainViewModel videoModel ->
                    if videoModel.focusLocation == OptionsModal then
                        ( { model | currentPage = Video mainViewModel { videoModel | focusLocation = PlayPauseButton } }, Cmd.none )

                    else if videoModel.focusLocation == OptionsModalBody then
                        -- TODO: design decide to go back to playing video or go back to modal titles
                        ( { model | currentPage = Video mainViewModel { videoModel | focusLocation = PlayPauseButton } }, Cmd.none )
                        --   ( { model | currentPage = Video mainViewModel { videoModel | focusLocation = OptionsModal } }, Cmd.none )

                    else
                        -- TODO: stop/destroy video js element if necessary
                        -- ( { model | currentPage = DetailsView mainViewModel }, pushVideoEvent DetachMedia )
                        ( { model | currentPage = MainView mainViewModel }, pushVideoEvent DetachMedia )

                MainView mainViewModel ->
                    case mainViewModel.currentPage of
                        Stack ->
                            let
                                ( _, newStack ) =
                                    Stack.pop mainViewModel.stack
                            in
                            ( { model | currentPage = MainView { mainViewModel | stack = newStack, currentPage = mainViewModel.previousPage } }, Cmd.none )

                        _ ->
                            ( model, Cmd.none )

                _ ->
                    ( model, Cmd.none )

        _ ->
            ( model, Cmd.none )


getCommandFromRow : RowHolder -> FavoriteStationsCollectionContent -> ProfileResource -> Cmd Msg
getCommandFromRow rowHolder station profile =
    case rowHolder.row of
        HeroButtonRow _ ->
            -- Play/Add
            -- This is currently handled in the update function where we have more data; we need to reach into the current focused featured item (which requires mainviewmodel)
            -- We could pass that data into this function and update all the callers in the future.
            Cmd.none

        SecondaryButtonRow _ ->
            Cmd.none

        FeaturedVideoRow videoZ ->
            videoZ |> current |> .slug |> getVideoDetails profile.pid

        BrowseButtonRow _ ->
            Cmd.none

        VideoItemRow videoZ ->
            videoZ |> current |> .slug |> getVideoDetails profile.pid

        ShowItemRow showZ ->
            showZ |> current |> .slug |> getShowDetails station.cid

        MultipleItemRow x ->
            case current x of
                ShowM show ->
                    show |> .slug |> getShowDetails station.cid

                VideoM video ->
                    video |> .slug |> getVideoDetails profile.pid


getCommandFromStack : DetailsPage -> FavoriteStationsCollectionContent -> ProfileResource -> Cmd Msg
getCommandFromStack detailsPage station profile =
    case detailsPage of
        VideoDetailsPage videoDetailsPage ->
            case videoDetailsPage.focusLocation of
                MainButtons ->
                    Cmd.none

                SecondaryButtons ->
                    Cmd.none

                SubNavNav ->
                    Cmd.none

                SubNav ->
                    case current videoDetailsPage.subNavs of
                        Details ->
                            Cmd.none

                        MultipleColumn _ multipleContentZ ->
                            case current multipleContentZ of
                                ShowM showContent ->
                                    getShowDetails station.cid showContent.slug

                                VideoM videoContent ->
                                    getVideoDetails profile.pid videoContent.slug

        ShowDetailsPage x ->
            getCommandFromRow (current x.rows) station profile


subscriptions : Model -> Sub Msg
subscriptions model =
    Sub.batch
        [ Browser.Events.onKeyDown (Decode.map KeyPressed keyDecoder)
        , Ports.mediaAttached (\_ -> HlsMediaAttached)
        , Ports.manifestParsed
            (\x ->
                case decodeValue hlsManifest x of
                    Ok manifest ->
                        HlsManifestReceived manifest

                    Err y ->
                        GenericJsonDecodeError y
            )
        ]


keyDecoder : Decoder String
keyDecoder =
    field "key" string


view : Model -> Document Msg
view model =
    { title = "PBS Video"
    , body =
        [ div
            [ style "zoom"
                (if model.deviceType == VIZIO then
                    "0.666"

                 else
                    "1.0"
                )
            ]
            [ div [ class "absolute z-0 theme-bg-dark-blue" ] [ video (class "forcer" :: id "media-video" :: videoEvents) [] ]
            , div [ class "absolute z-1 forcer  overflow-hidden white " ] [ view2 model ]
            ]
        ]
    }


videoEvents : List (Attribute Msg)
videoEvents =
    [ Attr.map VideoPageMessage (on "playing" (succeed NowPlaying))
    , Attr.map VideoPageMessage (on "pause" (succeed NowPaused))
    , Attr.map VideoPageMessage (on "durationchange" decodeDuration)
    , Attr.map VideoPageMessage (on "timeupdate" decodePosition)
    , Attr.map VideoPageMessage (on "canplaythrough" (succeed CanPlayThrough))
    , Attr.map VideoPageMessage (on "loadstart" (succeed LoadStart))
    ]



-- TODO: more events from the spec
-- , on "abort" decodeAbort
-- , on "canplay" decodeCanplay
-- , on "canplaythrough" decodeCanplaythrough
-- , on "durationchange" decodeDurationchange
-- , on "emptied" decodeEmptied
-- , on "ended" decodeEnded
-- , on "error" decodeError
-- , on "loadeddata" decodeLoadeddata
-- , on "loadedmetadata" decodeLoadedmetadata
-- , on "loadstart" decodeLoadstart
-- , on "pause" decodePause
-- , on "play" decodePlay
-- , on "playing" decodePlaying
-- , on "progress" decodeProgress
-- , on "ratechange" decodeRatechange
-- , on "seeked" decodeSeeked
-- , on "seeking" decodeSeeking
-- , on "stalled" decodeStalled
-- , on "suspend" decodeSuspend
-- , on "timeupdate" decodeTimeupdate
-- , on "volumechange" decodeVolumechange
-- , on "waiting" decodeWaiting


view2 : Model -> Html Msg
view2 model =
    div [ class "theme-bg-dark-blue" ]
        [ case model.currentPage of
            Splash _ ->
                viewSplash

            Activation _ _ ->
                viewActivation

            ActivationCode _ authCode ->
                viewActivationCode authCode

            LoadingMain _ _ _ ->
                viewLoadingPage model.error

            LoadingShow _ _ ->
                viewLoadingPage model.error

            LoadingVideoDetails _ _ ->
                viewLoadingPage model.error

            LoadingVideoPlayback _ _ ->
                viewLoadingPage model.error

            MainView mainViewModel ->
                Html.map MainViewMessage (viewMainPage mainViewModel)

            Video m videoModel ->
                Html.map VideoPageMessage (viewVideoPage m videoModel)
        ]


viewLoadingPage : String -> Html Msg
viewLoadingPage error =
    div [ class "fl w-100 pa6 f1" ]
        [ div []
            [ text "Loading Home Screen..."
            ]
        , div []
            [ div []
                [ text error
                ]
            ]
        ]
