module Msg exposing (Msg(..))

import Browser exposing (UrlRequest)
import Data.ActivationCode exposing (..)
import Data.ContentResponse exposing (..)
import Data.FavoriteStations exposing (..)
import Data.HlsManifest exposing (..)
import Data.MainView exposing (..)
import Data.Profile exposing (..)
import Data.Video exposing (..)
import Http
import Json.Decode as Decode
import RemoteData exposing (..)
import Url exposing (Url)
import Uuid exposing (..)


type Msg
    = GotGif (WebData String)
    | DeferCommandMessage (Cmd Msg)
    | GotActivationCode (WebData AuthCode)
    | GotActivationCodeStatus (WebData ActivationCodeStatusResponse)
    | GotProfileData (WebData ProfileResponse)
    | GotFavoriteStations (WebData FavoriteStations)
    | GotFavoriteShows (WebData ContentResponse)
    | GotWatchList (WebData ContentResponse)
    | GotViewingHistory (WebData ContentResponse)
    | InitializedMainView (Result Http.Error MainViewModel)
    | GotSearchContent (WebData ContentResponse)
    | GotShowDetails (WebData ContentResponse)
    | GotVideoDetails (WebData ContentResponse)
    | GotVideoPlayback (WebData ContentResponse)
    | HlsManifestReceived HlsManifest
    | GenericJsonDecodeError Decode.Error
    | HlsMediaAttached
    | KeyPressed String
    | CheckActivation
    | GotUuid Uuid
    | VideoPageMessage VideoElementMsg
    | MainViewMessage MainViewMsg
    | UrlChange Url
    | UrlRequest UrlRequest
