module Requests exposing (getActivationCode, getActivationCodeStatus, getFavoriteStations, getHomeContentTask, getProfileData, getRandomCatGif, getResumeWatchingTask, getSearchContent, getShowDetails, getShowsContentTask, getVideoDetails, getVideoPlayback, myListRequests, replaceHomeUrl, replaceMyListUrl, replaceShowDetailsUrl, replaceShowsUrl, replaceVideoDetailsUrl, replaceVideoPlaybackUrl)

import Browser.Navigation exposing (Key, replaceUrl)
import Data.ActivationCode exposing (..)
import Data.ContentResponse exposing (..)
import Data.FavoriteStations exposing (..)
import Data.Profile exposing (decodeProfile)
import Http exposing (Error, emptyBody, expectJson, get, header, request)
import Json.Decode as Decode exposing (Decoder, decodeString, field)
import Msg exposing (..)
import RemoteData exposing (WebData, fromResult)
import String exposing (join)
import Task exposing (..)
import Url exposing (..)
import Url.Builder exposing (crossOrigin, string)


profileServicesAuth : String
profileServicesAuth =
    "Basic a2lkc3NhbXN1bmd0djpLbUEzN0UyVkRkb0wxR3JCcEllWQ=="


contentServicesAuth : String
contentServicesAuth =
    "Basic a2lkc3NhbXN1bmd0djpCQ1hiQTV2SXhqOFNGNnY="


gifDecoder : Decoder String
gifDecoder =
    field "data" (field "image_url" Decode.string)


getRandomCatGif : Cmd Msg
getRandomCatGif =
    get
        { url = "https://api.giphy.com/v1/gifs/random?api_key=6vsk9muIKTR94lvHo2O9KpmwyuAHbeKL&tag=cat"
        , expect = expectJson (fromResult >> GotGif) gifDecoder
        }


contentEndpoint : String
contentEndpoint =
    "https://content.services.pbs.org"


profileEndpoint : String
profileEndpoint =
    "https://profile.services.pbs.org"


handleJsonResponse : Decoder a -> Http.Response String -> Result Error a
handleJsonResponse decoder response =
    case response of
        Http.BadUrl_ url ->
            Err (Http.BadUrl url)

        Http.Timeout_ ->
            Err Http.Timeout

        Http.BadStatus_ { statusCode } _ ->
            Err (Http.BadStatus statusCode)

        Http.NetworkError_ ->
            Err Http.NetworkError

        Http.GoodStatus_ _ body ->
            case decodeString decoder body of
                Err _ ->
                    Err (Http.BadBody body)

                Ok result ->
                    Ok result


contentServiceRequest : String -> (WebData a -> Msg) -> Decoder a -> Cmd Msg
contentServiceRequest url command decoder =
    request
        { method = "GET"
        , headers =
            [ header "Authorization" contentServicesAuth
            , header "X-PBS-PlatformVersion" "3.0.0"
            ]
        , url = url
        , expect = expectJson (fromResult >> command) decoder
        , timeout = Nothing
        , body = emptyBody
        , tracker = Nothing
        }


profileServiceRequest : String -> String -> (WebData a -> Msg) -> Decoder a -> Http.Body -> Cmd Msg
profileServiceRequest method url command decoder body =
    request
        { method = method
        , headers =
            [ header "Authorization" profileServicesAuth
            , header "X-PBS-PlatformVersion" "3.0.0"
            ]
        , url = url
        , expect = expectJson (fromResult >> command) decoder
        , timeout = Nothing
        , body = body
        , tracker = Nothing
        }


activationCodeUrl : String
activationCodeUrl =
    addTrailingSlash <| crossOrigin profileEndpoint [ "v1", "matchmaker", "authCode" ] []


matchmakerUrl : String -> String
matchmakerUrl deviceId =
    addTrailingSlash <| crossOrigin profileEndpoint [ "v1", "matchmaker", "devices", "samsung-ga-" ++ deviceId, "app", "ga" ] []


favoriteUrl : String -> String
favoriteUrl userId =
    addTrailingSlash <| crossOrigin contentEndpoint [ "v3", "samsungtv", "profile", userId, "collections", "favorite-stations" ] []


userCollectionsUrl : String -> String -> String
userCollectionsUrl userId slug =
    addTrailingSlash <| crossOrigin contentEndpoint [ "v3", "samsungtv", "profile", userId, "collections", slug ] []


favoriteShowsUrl : String -> String
favoriteShowsUrl userId =
    addTrailingSlash <| crossOrigin contentEndpoint [ "v3", "samsungtv", "profile", userId, "collections", "favorite-shows" ] []


watchListUrl : String -> String
watchListUrl userId =
    addTrailingSlash <| crossOrigin contentEndpoint [ "v3", "samsungtv", "profile", userId, "collections", "watchlist" ] []


viewingHistoryUrl : String -> String
viewingHistoryUrl userId =
    addTrailingSlash <| crossOrigin contentEndpoint [ "v3", "samsungtv", "profile", userId, "collections", "viewing-history" ] []


homeScreenUrl : String -> String
homeScreenUrl stationId =
    crossOrigin contentEndpoint [ "v3", "samsungtv", "screens", "home/" ] [ string "station_id" stationId ]


profileUrl : String -> String
profileUrl userId =
    addTrailingSlash <| crossOrigin contentEndpoint [ "v3", "samsungtv", "profile", userId ] []


showScreenUrl : String -> String
showScreenUrl stationId =
    crossOrigin contentEndpoint [ "v3", "samsungtv", "screens", "shows/" ] [ string "station_id" stationId ]


showDetailsUrl : String -> String -> String
showDetailsUrl slug stationId =
    crossOrigin contentEndpoint [ "v3", "samsungtv", "screens", "shows", slug, "/" ] [ string "station_id" stationId ]


videoDetailsUrl : String -> String -> String
videoDetailsUrl slug userId =
    crossOrigin contentEndpoint [ "v3", "samsungtv", "screens", "video-assets", slug, "/" ] [ string "user_id" userId ]


searchUrl : String -> String
searchUrl query =
    crossOrigin contentEndpoint [ "v3", "samsungtv", "screens", "search/" ] [ string "q" query ]


formUrlencoded : List ( String, String ) -> String
formUrlencoded dictionary =
    dictionary
        |> List.map
            (\( name, value ) ->
                percentEncode name
                    ++ "="
                    ++ percentEncode value
            )
        |> join "&"


getActivationCode : String -> Cmd Msg
getActivationCode deviceId =
    let
        body : Http.Body
        body =
            formUrlencoded
                [ ( "deviceId", "samsung-ga-" ++ deviceId )
                , ( "app", "ga" )
                ]
                |> Http.stringBody "application/x-www-form-urlencoded"
    in
    profileServiceRequest "POST" activationCodeUrl GotActivationCode decodeAuthCodeResponse body


getActivationCodeStatus : String -> Cmd Msg
getActivationCodeStatus deviceId =
    profileServiceRequest "GET" (matchmakerUrl deviceId) GotActivationCodeStatus decodeActivationCodeStatusResponse emptyBody


getFavoriteStations : String -> Cmd Msg
getFavoriteStations userId =
    contentServiceRequest (favoriteUrl userId) GotFavoriteStations decodeFavoriteStations


myListRequests : String -> Key -> Cmd Msg
myListRequests userId key =
    Cmd.batch
        [ contentServiceRequest (favoriteShowsUrl userId) GotFavoriteShows decodeContentResponse
        , contentServiceRequest (watchListUrl userId) GotWatchList decodeContentResponse
        , contentServiceRequest (viewingHistoryUrl userId) GotViewingHistory decodeContentResponse
        , replaceMyListUrl key
        ]


contentServiceRequestTask : String -> Decoder a -> Task Error a
contentServiceRequestTask url decoder =
    Http.task
        { method = "GET"
        , headers =
            [ header "Authorization" contentServicesAuth
            , header "X-PBS-PlatformVersion" "3.0.0"
            ]
        , url = url
        , body = emptyBody
        , resolver = Http.stringResolver <| handleJsonResponse <| decoder
        , timeout = Nothing
        }


getHomeContentTask : String -> Task Error ContentResponse
getHomeContentTask stationId =
    contentServiceRequestTask (homeScreenUrl stationId) decodeContentResponse


getResumeWatchingTask : String -> Task Error ContentResponse
getResumeWatchingTask userId =
    contentServiceRequestTask (userCollectionsUrl userId "resume-watching") decodeContentResponse


getShowsContentTask : String -> Task Error ContentResponse
getShowsContentTask stationId =
    contentServiceRequestTask (showScreenUrl stationId) decodeContentResponse


getProfileData : String -> Cmd Msg
getProfileData userId =
    contentServiceRequest (profileUrl userId) GotProfileData decodeProfile


getShowDetails : String -> String -> Cmd Msg
getShowDetails stationId slug =
    contentServiceRequest (showDetailsUrl slug stationId) GotShowDetails decodeContentResponse


getVideoDetails : String -> String -> Cmd Msg
getVideoDetails userId slug =
    contentServiceRequest (videoDetailsUrl slug userId) GotVideoDetails decodeContentResponse


getVideoPlayback : String -> String -> Cmd Msg
getVideoPlayback userId slug =
    contentServiceRequest (videoDetailsUrl slug userId) GotVideoPlayback decodeContentResponse


getSearchContent : String -> Cmd Msg
getSearchContent query =
    contentServiceRequest (searchUrl query) GotSearchContent decodeContentResponse


replaceShowDetailsUrl : Key -> String -> Cmd Msg
replaceShowDetailsUrl key slug =
    replaceUrl key ("/show-details/" ++ slug)


replaceVideoDetailsUrl : Key -> String -> Cmd Msg
replaceVideoDetailsUrl key slug =
    replaceUrl key ("/video-details/" ++ slug)


replaceVideoPlaybackUrl : Key -> String -> Cmd Msg
replaceVideoPlaybackUrl key slug =
    replaceUrl key ("/video-playback/" ++ slug)


replaceShowsUrl : Key -> Cmd Msg
replaceShowsUrl key =
    replaceUrl key "/shows"


replaceMyListUrl : Key -> Cmd Msg
replaceMyListUrl key =
    replaceUrl key "/my-list"


replaceHomeUrl : Key -> Cmd Msg
replaceHomeUrl key =
    replaceUrl key "/home"



-- The trailing slash after "home" and "shows" is because :
--
-- https://content.services.pbs.org/v3/samsungtv/screens/home?station_id=bb77b7f7-78ec-4134-aa62-3902aeca6169
--
-- 301 Moved Permanently
--
-- To this:
-- https://content.services.pbs.org/v3/samsungtv/screens/home/?station_id=bb77b7f7-78ec-4134-aa62-3902aeca6169
--
-- TODO: Why is content service like this?
--
-- Likewise, some other requests need a slash added to the end, so this is a helper function


addTrailingSlash : String -> String
addTrailingSlash input =
    input ++ "/"
