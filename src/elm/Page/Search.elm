module Page.Search exposing (getSearchCurrentLetterValue, initialSearchPageData)

import Data.Search exposing (..)
import List.Zipper exposing (Zipper, current, fromCons)
import String.Extra exposing (fromCodePoints)


getSearchCurrentLetterValue : SearchPageData -> String
getSearchCurrentLetterValue data =
    case current data.buttons of
        Letter c ->
            String.fromChar c

        Clear ->
            "clear"

        Back ->
            "back"

        ManualSearch ->
            "back"


initialSearchPageData : SearchPageData
initialSearchPageData =
    { query = ""
    , focusLocation = EnteringQuery
    , buttons = initialSearchButtons
    , rows = Nothing
    }


initialSearchButtons : Zipper SearchButton
initialSearchButtons =
    fromCons (Letter 'A')
        ((List.range 98 122
            |> fromCodePoints
            |> String.toList
            |> List.map toSearchButton
         )
            ++ extraSearchButtons
        )


extraSearchButtons : List SearchButton
extraSearchButtons =
    [ Clear, Back ]


toSearchButton : Char -> SearchButton
toSearchButton input =
    Letter input
