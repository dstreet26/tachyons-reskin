module Page.Home exposing (greet)


greet : String -> String
greet name =
    "Hello " ++ name ++ "!"
