module Page.Video exposing (handleVideoPlayerEnter, handleVideoPlayerHorizontal, handleVideoPlayerVertical, initializeLevels, initializeVideoPlaybackModel)

import Bounce
import Data.HlsManifest exposing (HlsState(..), Level, dummyLevel)
import Data.Main exposing (PlaceholderDirection(..))
import Data.Video
    exposing
        ( CaptionStylingFocusLocation(..)
        , ModalFocusLocation(..)
        , PlayerState(..)
        , VideoElementMsg
        , VideoEvent(..)
        , VideoFocusLocation(..)
        , VideoModel
        , VideoQualityAndPlaybackFocusLocation(..)
        , pushVideoEvent
        , uiTimeout
        )
import Data.VideoResource exposing (ClosedCaption)
import List.Zipper exposing (current, from)
import Utils exposing (tryZipN)


initializeVideoPlaybackModel : String -> String -> String -> List ClosedCaption -> List Level -> VideoModel
initializeVideoPlaybackModel title description videoSource captions levels =
    { playerState = VideoLoading
    , focusLocation = PlayPauseButton
    , closedCaptions = True
    , percentLoaded = 0
    , percentBuffered = 0
    , position = 0
    , duration = 0
    , videoTitle = title
    , videoDescription = description
    , videoSource = videoSource
    , captionSource = captions
    , showUi = True
    , bounce = Bounce.init
    , modalFocusLocation = Styling
    , captionStylingFocusLocation = FontSize
    , captionStyles = defaultCaptionStyles
    , videoQualityAndPlaybackFocusLocation = Quality
    , playbackSpeed = from [ 0.25, 0.5 ] 1 [ 1.25, 1.5, 1.75, 2 ]
    , levels = initializeLevels levels
    , hlsState = Uninitialized
    }


initializeLevels levels =
    from [] dummyLevel levels


defaultCaptionStyles =
    { fontSize =
        from [ "75%" ] "100%" [ "150%", "200%" ]
    , fontStyle =
        from []
            "sans-serif"
            [ "Courier New, monospace, serif"
            , "Times New Roman, serif"
            , "Arial, monospace, sans-serif"
            , "Tahoma, sans-serif"
            , "Comic Sans, serif"
            , "Brush Script, serif"
            , "Engravers Gothic, sans-serif"
            ]
    , fgColor = from [] "white" [ "black", "red", "green", "blue", "yellow", "magenta", "cyan" ]
    , bgColor = from [] "black" [ "white", "red", "green", "blue", "yellow", "magenta", "cyan" ]
    , windowColor = from [] "white" [ "white", "red", "green", "blue", "yellow", "magenta", "cyan" ]
    , fgOpacity = from [ "0", "0.25", "0.5", "0.75" ] "1" [ "flashing" ]
    , bgOpacity = from [ "0", "0.25", "0.5", "0.75" ] "1" [ "flashing" ]
    , windowOpacity = from [ "0", "0.25", "0.5", "0.75" ] "1" [ "flashing" ]
    , textShadow = from [] "unimplemented" [ "need to", "calculate", "based on", "color and", "edge type" ]
    }


handleVideoPlayerModalHorizontal : PlaceholderDirection -> VideoModel -> ( VideoModel, Cmd VideoElementMsg )
handleVideoPlayerModalHorizontal direction videoModel =
    case direction of
        Left ->
            case videoModel.modalFocusLocation of
                VideoQualityAndPlayback ->
                    if videoModel.focusLocation == OptionsModalBody then
                        handleVideoQualityAndPlaybackHorizontal direction videoModel

                    else
                        ( videoModel, Cmd.none )

                AudioTracks ->
                    ( { videoModel | modalFocusLocation = VideoQualityAndPlayback }, Cmd.none )

                Captions ->
                    ( { videoModel | modalFocusLocation = AudioTracks }, Cmd.none )

                Styling ->
                    if videoModel.focusLocation == OptionsModalBody then
                        handleCaptionStylingHorizontal direction videoModel

                    else
                        ( { videoModel | modalFocusLocation = Captions }, Cmd.none )

        Right ->
            case videoModel.modalFocusLocation of
                VideoQualityAndPlayback ->
                    if videoModel.focusLocation == OptionsModalBody then
                        handleVideoQualityAndPlaybackHorizontal direction videoModel

                    else
                        ( { videoModel | modalFocusLocation = AudioTracks }, Cmd.none )

                AudioTracks ->
                    ( { videoModel | modalFocusLocation = Captions }, Cmd.none )

                Captions ->
                    ( { videoModel | modalFocusLocation = Styling }, Cmd.none )

                Styling ->
                    if videoModel.focusLocation == OptionsModalBody then
                        handleCaptionStylingHorizontal direction videoModel

                    else
                        ( videoModel, Cmd.none )


handleVideoQualityAndPlaybackHorizontal direction videoModel =
    case videoModel.videoQualityAndPlaybackFocusLocation of
        Speed ->
            let
                newSpeed =
                    Tuple.first (tryZipN 1 direction videoModel.playbackSpeed)
            in
            ( { videoModel | playbackSpeed = newSpeed }, pushVideoEvent (SetPlaybackSpeed (current newSpeed)) )

        Quality ->
            let
                newLevels =
                    Tuple.first (tryZipN 1 direction videoModel.levels)

                idOfLevel =
                    newLevels |> List.Zipper.before |> List.length
            in
            ( { videoModel | levels = newLevels }, pushVideoEvent (SetLevel (idOfLevel - 1)) )


handleCaptionStylingHorizontal direction videoModel =
    let
        captionStyles =
            videoModel.captionStyles

        newCaptionStyles =
            case videoModel.captionStylingFocusLocation of
                FontSize ->
                    { captionStyles | fontSize = Tuple.first (tryZipN 1 direction captionStyles.fontSize) }

                FontStyle ->
                    { captionStyles | fontStyle = Tuple.first (tryZipN 1 direction captionStyles.fontStyle) }

                FgColor ->
                    { captionStyles | fgColor = Tuple.first (tryZipN 1 direction captionStyles.fgColor) }

                BgColor ->
                    { captionStyles | bgColor = Tuple.first (tryZipN 1 direction captionStyles.bgColor) }

                WindowColor ->
                    { captionStyles | windowColor = Tuple.first (tryZipN 1 direction captionStyles.windowColor) }

                FgOpacity ->
                    { captionStyles | fgOpacity = Tuple.first (tryZipN 1 direction captionStyles.fgOpacity) }

                BgOpacity ->
                    { captionStyles | bgOpacity = Tuple.first (tryZipN 1 direction captionStyles.bgOpacity) }

                WindowOpacity ->
                    { captionStyles | windowOpacity = Tuple.first (tryZipN 1 direction captionStyles.windowOpacity) }

                TextShadow ->
                    { captionStyles | textShadow = Tuple.first (tryZipN 1 direction captionStyles.textShadow) }
    in
    ( { videoModel | captionStyles = newCaptionStyles }, Cmd.none )


handleVideoPlayerModalVertical : PlaceholderDirection -> VideoModel -> ( VideoModel, Cmd VideoElementMsg )
handleVideoPlayerModalVertical direction videoModel =
    case direction of
        -- Up
        Left ->
            case videoModel.modalFocusLocation of
                VideoQualityAndPlayback ->
                    case videoModel.videoQualityAndPlaybackFocusLocation of
                        Quality ->
                            ( { videoModel | focusLocation = OptionsModal }, Cmd.none )

                        Speed ->
                            ( { videoModel | videoQualityAndPlaybackFocusLocation = Quality }, Cmd.none )

                AudioTracks ->
                    ( videoModel, Cmd.none )

                Captions ->
                    ( videoModel, Cmd.none )

                Styling ->
                    case videoModel.captionStylingFocusLocation of
                        FontSize ->
                            ( { videoModel | focusLocation = OptionsModal }, Cmd.none )

                        FontStyle ->
                            ( { videoModel | captionStylingFocusLocation = FontSize }, Cmd.none )

                        FgColor ->
                            ( { videoModel | captionStylingFocusLocation = FontStyle }, Cmd.none )

                        BgColor ->
                            ( { videoModel | captionStylingFocusLocation = FgColor }, Cmd.none )

                        WindowColor ->
                            ( { videoModel | captionStylingFocusLocation = BgColor }, Cmd.none )

                        FgOpacity ->
                            ( { videoModel | captionStylingFocusLocation = WindowColor }, Cmd.none )

                        BgOpacity ->
                            ( { videoModel | captionStylingFocusLocation = FgOpacity }, Cmd.none )

                        WindowOpacity ->
                            ( { videoModel | captionStylingFocusLocation = BgOpacity }, Cmd.none )

                        TextShadow ->
                            ( { videoModel | captionStylingFocusLocation = WindowOpacity }, Cmd.none )

        -- Down
        Right ->
            case videoModel.modalFocusLocation of
                VideoQualityAndPlayback ->
                    if videoModel.focusLocation == OptionsModalBody then
                        case videoModel.videoQualityAndPlaybackFocusLocation of
                            Quality ->
                                ( { videoModel | videoQualityAndPlaybackFocusLocation = Speed }, Cmd.none )

                            Speed ->
                                ( videoModel, Cmd.none )

                    else
                        ( { videoModel | focusLocation = OptionsModalBody }, Cmd.none )

                AudioTracks ->
                    ( videoModel, Cmd.none )

                Captions ->
                    ( videoModel, Cmd.none )

                Styling ->
                    if videoModel.focusLocation == OptionsModalBody then
                        case videoModel.captionStylingFocusLocation of
                            FontSize ->
                                ( { videoModel | captionStylingFocusLocation = FontStyle }, Cmd.none )

                            FontStyle ->
                                ( { videoModel | captionStylingFocusLocation = FgColor }, Cmd.none )

                            FgColor ->
                                ( { videoModel | captionStylingFocusLocation = BgColor }, Cmd.none )

                            BgColor ->
                                ( { videoModel | captionStylingFocusLocation = WindowColor }, Cmd.none )

                            WindowColor ->
                                ( { videoModel | captionStylingFocusLocation = FgOpacity }, Cmd.none )

                            FgOpacity ->
                                ( { videoModel | captionStylingFocusLocation = BgOpacity }, Cmd.none )

                            BgOpacity ->
                                ( { videoModel | captionStylingFocusLocation = WindowOpacity }, Cmd.none )

                            WindowOpacity ->
                                ( { videoModel | captionStylingFocusLocation = TextShadow }, Cmd.none )

                            TextShadow ->
                                ( videoModel, Cmd.none )

                    else
                        ( { videoModel | focusLocation = OptionsModalBody }, Cmd.none )


handleVideoPlayerModalEnter : VideoModel -> ( VideoModel, Cmd VideoElementMsg )
handleVideoPlayerModalEnter videoModel =
    -- TODO: button to reset styles, button to select the pre-selected track, etc
    ( videoModel, Cmd.none )


handleVideoPlayerHorizontal : PlaceholderDirection -> VideoModel -> ( VideoModel, Cmd VideoElementMsg )
handleVideoPlayerHorizontal direction videoModel =
    let
        ( newModel, newCmd ) =
            case direction of
                Left ->
                    case videoModel.playerState of
                        Buffering ->
                            ( videoModel, Cmd.none )

                        VideoLoading ->
                            case videoModel.focusLocation of
                                PlayPauseButton ->
                                    ( { videoModel | focusLocation = RestartButton }, Cmd.none )

                                RestartButton ->
                                    ( videoModel, Cmd.none )

                                Slider ->
                                    let
                                        newPosition =
                                            clamp 0 videoModel.duration (videoModel.position - 10)
                                    in
                                    ( { videoModel | position = newPosition }, Cmd.none )

                                NextVideoButton ->
                                    ( { videoModel | focusLocation = PlayPauseButton }, Cmd.none )

                                OptionsButton ->
                                    ( { videoModel | focusLocation = NextVideoButton }, Cmd.none )

                                OptionsModal ->
                                    handleVideoPlayerModalHorizontal Left videoModel

                                OptionsModalBody ->
                                    handleVideoPlayerModalHorizontal Left videoModel

                        Paused ->
                            case videoModel.focusLocation of
                                PlayPauseButton ->
                                    ( { videoModel | focusLocation = RestartButton }, Cmd.none )

                                RestartButton ->
                                    ( videoModel, Cmd.none )

                                Slider ->
                                    let
                                        newPosition =
                                            clamp 0 videoModel.duration (videoModel.position - 10)
                                    in
                                    ( { videoModel | position = newPosition }, Cmd.none )

                                NextVideoButton ->
                                    ( { videoModel | focusLocation = PlayPauseButton }, Cmd.none )

                                OptionsButton ->
                                    ( { videoModel | focusLocation = NextVideoButton }, Cmd.none )

                                OptionsModal ->
                                    handleVideoPlayerModalHorizontal Left videoModel

                                OptionsModalBody ->
                                    handleVideoPlayerModalHorizontal Left videoModel

                        Playing ->
                            case videoModel.focusLocation of
                                PlayPauseButton ->
                                    ( { videoModel | focusLocation = RestartButton }, Cmd.none )

                                RestartButton ->
                                    ( videoModel, Cmd.none )

                                Slider ->
                                    ( videoModel, Cmd.none )

                                NextVideoButton ->
                                    ( { videoModel | focusLocation = PlayPauseButton }, Cmd.none )

                                OptionsButton ->
                                    ( { videoModel | focusLocation = NextVideoButton }, Cmd.none )

                                OptionsModal ->
                                    handleVideoPlayerModalHorizontal Left videoModel

                                OptionsModalBody ->
                                    handleVideoPlayerModalHorizontal Left videoModel

                Right ->
                    case videoModel.playerState of
                        Buffering ->
                            ( videoModel, Cmd.none )

                        VideoLoading ->
                            case videoModel.focusLocation of
                                PlayPauseButton ->
                                    ( { videoModel | focusLocation = NextVideoButton }, Cmd.none )

                                RestartButton ->
                                    ( { videoModel | focusLocation = PlayPauseButton }, Cmd.none )

                                Slider ->
                                    let
                                        newPosition =
                                            clamp 0 videoModel.duration (videoModel.position + 10)
                                    in
                                    ( { videoModel | position = newPosition }, Cmd.none )

                                NextVideoButton ->
                                    ( { videoModel | focusLocation = OptionsButton }, Cmd.none )

                                OptionsButton ->
                                    ( videoModel, Cmd.none )

                                OptionsModal ->
                                    handleVideoPlayerModalHorizontal Right videoModel

                                OptionsModalBody ->
                                    handleVideoPlayerModalHorizontal Right videoModel

                        Paused ->
                            case videoModel.focusLocation of
                                PlayPauseButton ->
                                    ( { videoModel | focusLocation = NextVideoButton }, Cmd.none )

                                RestartButton ->
                                    ( { videoModel | focusLocation = PlayPauseButton }, Cmd.none )

                                Slider ->
                                    let
                                        newPosition =
                                            clamp 0 videoModel.duration (videoModel.position + 10)
                                    in
                                    ( { videoModel | position = newPosition }, Cmd.none )

                                NextVideoButton ->
                                    ( { videoModel | focusLocation = OptionsButton }, Cmd.none )

                                OptionsButton ->
                                    ( videoModel, Cmd.none )

                                OptionsModal ->
                                    handleVideoPlayerModalHorizontal Right videoModel

                                OptionsModalBody ->
                                    handleVideoPlayerModalHorizontal Right videoModel

                        Playing ->
                            case videoModel.focusLocation of
                                PlayPauseButton ->
                                    ( { videoModel | focusLocation = NextVideoButton }, Cmd.none )

                                RestartButton ->
                                    ( { videoModel | focusLocation = PlayPauseButton }, Cmd.none )

                                Slider ->
                                    ( videoModel, pushVideoEvent Pause )

                                NextVideoButton ->
                                    ( { videoModel | focusLocation = OptionsButton }, Cmd.none )

                                OptionsButton ->
                                    ( videoModel, Cmd.none )

                                OptionsModal ->
                                    handleVideoPlayerModalHorizontal Right videoModel

                                OptionsModalBody ->
                                    handleVideoPlayerModalHorizontal Right videoModel
    in
    ( { newModel | showUi = True, bounce = Bounce.push videoModel.bounce }, Cmd.batch [ newCmd, uiTimeout ] )


handleVideoPlayerVertical : PlaceholderDirection -> VideoModel -> ( VideoModel, Cmd VideoElementMsg )
handleVideoPlayerVertical direction videoModel =
    let
        ( newModel, newCmd ) =
            case direction of
                -- Up
                Left ->
                    case videoModel.playerState of
                        Buffering ->
                            ( videoModel, Cmd.none )

                        VideoLoading ->
                            ( videoModel, Cmd.none )

                        Paused ->
                            case videoModel.focusLocation of
                                PlayPauseButton ->
                                    ( { videoModel | focusLocation = Slider }, Cmd.none )

                                RestartButton ->
                                    ( { videoModel | focusLocation = Slider }, Cmd.none )

                                Slider ->
                                    ( videoModel, Cmd.none )

                                NextVideoButton ->
                                    ( { videoModel | focusLocation = Slider }, Cmd.none )

                                OptionsButton ->
                                    ( { videoModel | focusLocation = Slider }, Cmd.none )

                                OptionsModal ->
                                    handleVideoPlayerModalVertical direction videoModel

                                OptionsModalBody ->
                                    handleVideoPlayerModalVertical direction videoModel

                        Playing ->
                            case videoModel.focusLocation of
                                PlayPauseButton ->
                                    ( { videoModel | focusLocation = Slider }, Cmd.none )

                                RestartButton ->
                                    ( { videoModel | focusLocation = Slider }, Cmd.none )

                                Slider ->
                                    ( videoModel, Cmd.none )

                                NextVideoButton ->
                                    ( { videoModel | focusLocation = Slider }, Cmd.none )

                                OptionsButton ->
                                    ( { videoModel | focusLocation = Slider }, Cmd.none )

                                OptionsModal ->
                                    handleVideoPlayerModalVertical direction videoModel

                                OptionsModalBody ->
                                    handleVideoPlayerModalVertical direction videoModel

                -- Down
                Right ->
                    case videoModel.playerState of
                        Buffering ->
                            ( videoModel, Cmd.none )

                        VideoLoading ->
                            ( videoModel, Cmd.none )

                        Paused ->
                            case videoModel.focusLocation of
                                PlayPauseButton ->
                                    ( videoModel, Cmd.none )

                                RestartButton ->
                                    ( videoModel, Cmd.none )

                                Slider ->
                                    ( { videoModel | focusLocation = PlayPauseButton }, Cmd.none )

                                NextVideoButton ->
                                    ( videoModel, Cmd.none )

                                OptionsButton ->
                                    ( { videoModel | focusLocation = Slider }, Cmd.none )

                                OptionsModal ->
                                    handleVideoPlayerModalVertical Right videoModel

                                OptionsModalBody ->
                                    handleVideoPlayerModalVertical Right videoModel

                        Playing ->
                            case videoModel.focusLocation of
                                PlayPauseButton ->
                                    ( videoModel, Cmd.none )

                                RestartButton ->
                                    ( videoModel, Cmd.none )

                                Slider ->
                                    ( { videoModel | focusLocation = PlayPauseButton }, Cmd.none )

                                NextVideoButton ->
                                    ( videoModel, Cmd.none )

                                OptionsButton ->
                                    ( { videoModel | focusLocation = Slider }, Cmd.none )

                                OptionsModal ->
                                    handleVideoPlayerModalVertical Right videoModel

                                OptionsModalBody ->
                                    handleVideoPlayerModalVertical Right videoModel
    in
    ( { newModel | showUi = True, bounce = Bounce.push videoModel.bounce }, Cmd.batch [ newCmd, uiTimeout ] )


handleVideoPlayerEnter : VideoModel -> ( VideoModel, Cmd VideoElementMsg )
handleVideoPlayerEnter videoModel =
    let
        ( newModel, newCmd ) =
            case videoModel.playerState of
                Buffering ->
                    ( videoModel, Cmd.none )

                VideoLoading ->
                    case videoModel.focusLocation of
                        PlayPauseButton ->
                            ( videoModel, pushVideoEvent Play )

                        RestartButton ->
                            ( videoModel, pushVideoEvent (SeekTo 0) )

                        Slider ->
                            ( videoModel, pushVideoEvent (SeekTo videoModel.position) )

                        NextVideoButton ->
                            ( videoModel, Cmd.none )

                        OptionsButton ->
                            ( { videoModel | focusLocation = OptionsModal }, pushVideoEvent Pause )

                        OptionsModal ->
                            handleVideoPlayerModalEnter videoModel

                        OptionsModalBody ->
                            handleVideoPlayerModalEnter videoModel

                Paused ->
                    case videoModel.focusLocation of
                        PlayPauseButton ->
                            ( videoModel, pushVideoEvent Play )

                        RestartButton ->
                            ( videoModel, pushVideoEvent (SeekTo 0) )

                        Slider ->
                            ( videoModel, pushVideoEvent (SeekTo videoModel.position) )

                        NextVideoButton ->
                            ( videoModel, Cmd.none )

                        OptionsButton ->
                            ( { videoModel | focusLocation = OptionsModal }, pushVideoEvent Pause )

                        OptionsModal ->
                            handleVideoPlayerModalEnter videoModel

                        OptionsModalBody ->
                            handleVideoPlayerModalEnter videoModel

                Playing ->
                    case videoModel.focusLocation of
                        PlayPauseButton ->
                            ( videoModel, pushVideoEvent Pause )

                        RestartButton ->
                            ( videoModel, pushVideoEvent (SeekTo 0) )

                        Slider ->
                            ( videoModel, pushVideoEvent (SeekTo videoModel.position) )

                        NextVideoButton ->
                            ( videoModel, Cmd.none )

                        OptionsButton ->
                            ( { videoModel | focusLocation = OptionsModal }, pushVideoEvent Pause )

                        OptionsModal ->
                            handleVideoPlayerModalEnter videoModel

                        OptionsModalBody ->
                            handleVideoPlayerModalEnter videoModel
    in
    ( { newModel | showUi = True, bounce = Bounce.push videoModel.bounce }, Cmd.batch [ newCmd, uiTimeout ] )
