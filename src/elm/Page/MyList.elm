module Page.MyList exposing (initializeMyListData, updateFavoriteShows, updateViewingHistory, updateWatchList)

import Data.ContentResponse
    exposing
        ( ContentResponse
        )
import Data.MyList exposing (MyListData, MyListFocusLocation(..))
import Data.Row exposing (..)
import RemoteData exposing (RemoteData(..), WebData, map)


initializeMyListData : MyListData
initializeMyListData =
    { favoriteShows = NotAsked
    , watchlist = NotAsked
    , history = NotAsked
    , focusLocation = FavoriteShows
    , scrollPosition = 0
    }


updateFavoriteShows : MyListData -> WebData ContentResponse -> MyListData
updateFavoriteShows myListData result =
    { myListData | favoriteShows = map (contentResponseToShowRowBySlugWithTitle "favorite-shows" "Favorite Shows") result }


updateWatchList : MyListData -> WebData ContentResponse -> MyListData
updateWatchList myListData result =
    { myListData | watchlist = map (contentResponseToVideoRowBySlugWithTitle "watchlist" "Watchlist") result }


updateViewingHistory : MyListData -> WebData ContentResponse -> MyListData
updateViewingHistory myListData result =
    { myListData | history = map (contentResponseToVideoRowBySlugWithTitle "viewing-history" "History") result }
