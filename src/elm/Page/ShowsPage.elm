module Page.ShowsPage exposing (initializeShowsData)

import Data.ContentResponse
    exposing
        ( Collection(..)
        , ContentResponse
        , ShowContent
        , dummyShowContent
        , dummyShowItemList
        )
import Data.ShowsPage exposing (ShowCategory, ShowPageData, ShowPageLocation(..))
import List exposing (filterMap)
import List.Zipper exposing (Zipper, fromCons, fromList, withDefault)


dummyShowCategory : ShowCategory
dummyShowCategory =
    { title = "Category 1"
    , items = fromCons dummyShowContent dummyShowItemList
    }


initializeShowsData : ContentResponse -> ShowPageData
initializeShowsData showsResponse =
    { focusLocation = Categories
    , showCategories = fromList (transformIntoShowCategories showsResponse) |> withDefault dummyShowCategory
    }


transformIntoShowCategories : ContentResponse -> List ShowCategory
transformIntoShowCategories contentResponse =
    filterMap
        (\collection ->
            case collection of
                InlineShow incomingShowData ->
                    let
                        newItems : Zipper ShowContent
                        newItems =
                            incomingShowData.content
                                |> fromList
                                |> withDefault dummyShowContent
                    in
                    Just
                        { title = incomingShowData.title
                        , items = newItems
                        }

                _ ->
                    Nothing
        )
        contentResponse.collections
