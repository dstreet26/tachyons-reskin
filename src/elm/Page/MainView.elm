module Page.MainView exposing (getCurrentFeaturedVideo, incomingSearchData, initializeEverything, navigatePageContentHorizontal, navigatePageContentVertical, navigateSidebarContent, pushStack)

import Browser.Navigation exposing (Key)
import Data.ContentResponse
    exposing
        ( Collection
        , ContentResponse
        , VideoContent
        , dummyVideoContent
        , isStub
        )
import Data.DetailsPage exposing (DetailsPage(..), VideoDetailsFocusLocation(..), VideoDetailsPageData)
import Data.Device exposing (DeviceId, DeviceType)
import Data.FavoriteStations exposing (FavoriteStationsCollectionContent)
import Data.FocusLocation exposing (FocusLocation(..))
import Data.Home exposing (..)
import Data.Main exposing (AlternateDirection(..), PlaceholderDirection(..))
import Data.MainView
    exposing
        ( MainViewModel
        , Page(..)
        )
import Data.MyList exposing (..)
import Data.Profile exposing (ProfileResource)
import Data.Row
    exposing
        ( Row(..)
        , RowHolder
        , collectionsToRowHolderFeaturedVideos
        , collectionsToRowHolderResumeWatching
        , collectionsToRowHolderTheRest
        , dummyFeaturedVideosRow
        , dummyShowItemRow
        , mapToRowHolders
        )
import Data.Search exposing (..)
import Data.ShowsPage exposing (..)
import DemoData
    exposing
        ( homeBrowseButtons
        , homeHeroButtons
        , initialSettingsCards
        )
import List
import List.Zipper exposing (Zipper, current, first, fromList, mapCurrent, next, singleton, withDefault)
import Msg exposing (..)
import Page.DetailsPage exposing (navigateStackContentInner)
import Page.MyList exposing (..)
import Page.Search exposing (..)
import Page.ShowsPage exposing (..)
import RemoteData exposing (RemoteData(..), WebData, succeed)
import Requests exposing (..)
import ScrollOffsets exposing (calcRowScrollDifference)
import Stack
import Utils exposing (navigateRowHolder, navigateRowHolderZ, tryZipN)


navigateVideoDetailsPageOuter : VideoDetailsPageData -> PlaceholderDirection -> VideoDetailsPageData
navigateVideoDetailsPageOuter pageData pageDirection =
    case pageData.focusLocation of
        MainButtons ->
            case pageDirection of
                Left ->
                    { pageData | focusLocation = MainButtons }

                Right ->
                    { pageData | focusLocation = SecondaryButtons }

        SecondaryButtons ->
            case pageDirection of
                Left ->
                    { pageData | focusLocation = MainButtons }

                Right ->
                    { pageData | focusLocation = SubNavNav }

        SubNavNav ->
            case pageDirection of
                Left ->
                    { pageData | focusLocation = SecondaryButtons }

                Right ->
                    { pageData | focusLocation = SubNav }

        SubNav ->
            case pageDirection of
                Left ->
                    { pageData | focusLocation = SubNavNav }

                Right ->
                    { pageData | focusLocation = SubNav }


navigateStackContentOuter : DetailsPage -> PlaceholderDirection -> DetailsPage
navigateStackContentOuter pageData pageDirection =
    case pageData of
        VideoDetailsPage x ->
            VideoDetailsPage (navigateVideoDetailsPageOuter x pageDirection)

        ShowDetailsPage x ->
            let
                ( one, _ ) =
                    tryZipN 1 pageDirection x.rows
            in
            ShowDetailsPage { x | rows = one }


setHomePageFocus : FocusLocation -> MainViewModel -> MainViewModel
setHomePageFocus focusLocation mainViewModel =
    { mainViewModel | focusLocation = focusLocation }


navigateSidebarContent : MainViewModel -> PlaceholderDirection -> Key -> ( MainViewModel, Cmd Msg )
navigateSidebarContent mainViewModel pageDirection key =
    case mainViewModel.currentPage of
        Home ->
            case pageDirection of
                Left ->
                    ( mainViewModel, Cmd.none )

                Right ->
                    ( { mainViewModel | currentPage = Shows, previousPage = Shows }, replaceShowsUrl key )

        Shows ->
            case pageDirection of
                Left ->
                    ( { mainViewModel | currentPage = Home, previousPage = Home }, replaceHomeUrl key )

                Right ->
                    ( { mainViewModel | currentPage = Search, previousPage = Search }, Cmd.none )

        Search ->
            case pageDirection of
                Left ->
                    ( { mainViewModel | currentPage = Shows, previousPage = Shows }, replaceShowsUrl key )

                Right ->
                    ( { mainViewModel | currentPage = MyList, previousPage = MyList }, myListRequests mainViewModel.profile.pid key )

        MyList ->
            case pageDirection of
                Left ->
                    ( { mainViewModel | currentPage = Search, previousPage = Search }, Cmd.none )

                Right ->
                    ( { mainViewModel | currentPage = MyStation, previousPage = MyStation }, Cmd.none )

        MyStation ->
            case pageDirection of
                Left ->
                    ( { mainViewModel | currentPage = MyList, previousPage = MyList }, myListRequests mainViewModel.profile.pid key )

                Right ->
                    ( { mainViewModel | currentPage = Profile, previousPage = Profile }, Cmd.none )

        Profile ->
            case pageDirection of
                Left ->
                    ( { mainViewModel | currentPage = MyStation, previousPage = MyStation }, Cmd.none )

                Right ->
                    ( { mainViewModel | currentPage = Cats, previousPage = Cats }, Cmd.none )

        Cats ->
            case pageDirection of
                Left ->
                    ( { mainViewModel | currentPage = Profile, previousPage = Profile }, Cmd.none )

                Right ->
                    ( mainViewModel, Cmd.none )

        Stack ->
            ( mainViewModel, Cmd.none )


navigatePageContentHorizontal : MainViewModel -> PlaceholderDirection -> MainViewModel
navigatePageContentHorizontal mainViewModel direction =
    case mainViewModel.currentPage of
        Home ->
            let
                ( newRows, limitHit ) =
                    navigateRowHolderZ mainViewModel.homeData.rows direction

                homeData =
                    mainViewModel.homeData

                newHomeData =
                    { homeData | rows = newRows }
            in
            if limitHit && direction == Left then
                { mainViewModel | focusLocation = Sidebar }

            else
                { mainViewModel | homeData = newHomeData }

        Shows ->
            case mainViewModel.showsPageData.focusLocation of
                Categories ->
                    let
                        showsPageData =
                            mainViewModel.showsPageData

                        ( newCategories, limitHit ) =
                            tryZipN 1 direction mainViewModel.showsPageData.showCategories
                    in
                    if limitHit && direction == Left then
                        { mainViewModel | focusLocation = Sidebar }

                    else
                        { mainViewModel | showsPageData = { showsPageData | showCategories = newCategories } }

                Items ->
                    let
                        showsPageData =
                            mainViewModel.showsPageData

                        currentCategory =
                            current showsPageData.showCategories

                        showCategories =
                            showsPageData.showCategories

                        ( newItems, limitHit ) =
                            tryZipN 1 direction currentCategory.items

                        newCategories =
                            mapCurrent (\x -> { x | items = newItems }) showCategories

                        newShowPageData =
                            { showsPageData | showCategories = newCategories }
                    in
                    if limitHit && direction == Left then
                        { mainViewModel | focusLocation = Sidebar }

                    else
                        { mainViewModel
                            | showsPageData = newShowPageData
                        }

        Search ->
            case mainViewModel.searchPageData.focusLocation of
                EnteringQuery ->
                    let
                        searchPageData =
                            mainViewModel.searchPageData

                        -- buttons =
                        ( newButtons, limitHit ) =
                            tryZipN 1 direction searchPageData.buttons

                        newSearchPageData =
                            { searchPageData | buttons = newButtons }
                    in
                    if limitHit && direction == Left then
                        { mainViewModel | focusLocation = Sidebar }

                    else
                        { mainViewModel | searchPageData = newSearchPageData }

                BrowsingContent ->
                    case mainViewModel.searchPageData.rows of
                        Nothing ->
                            -- TODO: try to make impossible
                            if direction == Left then
                                { mainViewModel | focusLocation = Sidebar }

                            else
                                mainViewModel

                        Just rows ->
                            let
                                searchPageData =
                                    mainViewModel.searchPageData

                                ( newRows, limitHit ) =
                                    navigateRowHolderZ rows direction

                                newSearchPageData =
                                    { searchPageData | rows = Just newRows }
                            in
                            if limitHit && direction == Left then
                                { mainViewModel | focusLocation = Sidebar }

                            else
                                { mainViewModel | searchPageData = newSearchPageData }

        MyList ->
            case mainViewModel.myListData.focusLocation of
                FavoriteShows ->
                    case mainViewModel.myListData.favoriteShows of
                        Success x ->
                            let
                                ( newRowHolder, limitHit ) =
                                    navigateRowHolder x direction

                                myListData =
                                    mainViewModel.myListData

                                newMyListData =
                                    { myListData | favoriteShows = succeed newRowHolder }
                            in
                            if limitHit && direction == Left then
                                { mainViewModel | focusLocation = Sidebar }

                            else
                                { mainViewModel | myListData = newMyListData }

                        _ ->
                            mainViewModel

                Watchlist ->
                    case mainViewModel.myListData.watchlist of
                        Success x ->
                            let
                                ( newRowHolder, limitHit ) =
                                    navigateRowHolder x direction

                                myListData =
                                    mainViewModel.myListData

                                newMyListData =
                                    { myListData | watchlist = succeed newRowHolder }
                            in
                            if limitHit && direction == Left then
                                { mainViewModel | focusLocation = Sidebar }

                            else
                                { mainViewModel | myListData = newMyListData }

                        _ ->
                            mainViewModel

                History ->
                    case mainViewModel.myListData.history of
                        Success x ->
                            let
                                ( newRowHolder, limitHit ) =
                                    navigateRowHolder x direction

                                myListData =
                                    mainViewModel.myListData

                                newMyListData =
                                    { myListData | history = succeed newRowHolder }
                            in
                            if limitHit && direction == Left then
                                { mainViewModel | focusLocation = Sidebar }

                            else
                                { mainViewModel | myListData = newMyListData }

                        _ ->
                            mainViewModel

        MyStation ->
            case direction of
                Left ->
                    mainViewModel |> setHomePageFocus Sidebar

                Right ->
                    mainViewModel

        Profile ->
            case direction of
                Left ->
                    mainViewModel |> setHomePageFocus Sidebar

                Right ->
                    mainViewModel

        Cats ->
            case direction of
                Left ->
                    mainViewModel |> setHomePageFocus Sidebar

                Right ->
                    mainViewModel

        Stack ->
            let
                ( currentShowDetailsPageData, poppedStack ) =
                    Stack.pop mainViewModel.stack
            in
            case currentShowDetailsPageData of
                Nothing ->
                    mainViewModel

                Just x ->
                    let
                        ( newDetailsPage, limitHit ) =
                            navigateStackContentInner x direction

                        newStack =
                            Stack.push newDetailsPage poppedStack
                    in
                    if limitHit && direction == Left then
                        { mainViewModel | focusLocation = Sidebar }

                    else
                        { mainViewModel | stack = newStack }


navigatePageContentVertical : MainViewModel -> PlaceholderDirection -> MainViewModel
navigatePageContentVertical mainViewModel pageDirection =
    let
        showsPageData =
            mainViewModel.showsPageData

        searchPageData =
            mainViewModel.searchPageData

        myListData =
            mainViewModel.myListData
    in
    case mainViewModel.currentPage of
        Home ->
            let
                ( newRows, limitHit ) =
                    tryZipN 1 pageDirection mainViewModel.homeData.rows

                currentPosition =
                    mainViewModel.homeData.scrollPosition

                currentRow =
                    current mainViewModel.homeData.rows |> .row

                nextRow =
                    current newRows |> .row

                difference =
                    case pageDirection of
                        Left ->
                            calcRowScrollDifference Up currentRow nextRow

                        Right ->
                            if limitHit then
                                0

                            else
                                calcRowScrollDifference Down currentRow nextRow

                newPosition =
                    case pageDirection of
                        Left ->
                            if limitHit then
                                0

                            else
                                currentPosition - difference

                        Right ->
                            currentPosition + difference

                newScrollPosition =
                    newPosition

                homeData =
                    mainViewModel.homeData

                newHomeData =
                    { homeData | rows = newRows, scrollPosition = newScrollPosition }
            in
            { mainViewModel | homeData = newHomeData }

        Shows ->
            case pageDirection of
                Left ->
                    case showsPageData.focusLocation of
                        Categories ->
                            mainViewModel

                        Items ->
                            let
                                currentCategory =
                                    current showsPageData.showCategories

                                showCategories =
                                    showsPageData.showCategories

                                ( newItems, limitHit ) =
                                    tryZipN 5 Left currentCategory.items

                                newCategories =
                                    mapCurrent (\x -> { x | items = newItems }) showCategories
                            in
                            if limitHit then
                                { mainViewModel | showsPageData = { showsPageData | focusLocation = Categories } }

                            else
                                { mainViewModel | showsPageData = { showsPageData | showCategories = newCategories } }

                Right ->
                    case showsPageData.focusLocation of
                        Categories ->
                            let
                                newShowPageData =
                                    { showsPageData | focusLocation = Items }
                            in
                            { mainViewModel | showsPageData = newShowPageData }

                        Items ->
                            let
                                currentCategory =
                                    current showsPageData.showCategories

                                showCategories =
                                    showsPageData.showCategories

                                ( newItems, _ ) =
                                    tryZipN 5 Right currentCategory.items

                                newCategories =
                                    mapCurrent (\x -> { x | items = newItems }) showCategories
                            in
                            { mainViewModel | showsPageData = { showsPageData | showCategories = newCategories } }

        Search ->
            case pageDirection of
                Left ->
                    case searchPageData.focusLocation of
                        EnteringQuery ->
                            let
                                ( newButtons, _ ) =
                                    tryZipN 7 Left searchPageData.buttons
                            in
                            { mainViewModel | searchPageData = { searchPageData | buttons = newButtons } }

                        BrowsingContent ->
                            case searchPageData.rows of
                                Just rowHolderZ ->
                                    let
                                        ( newRows, limitHit ) =
                                            tryZipN 1 Left rowHolderZ
                                    in
                                    if limitHit then
                                        { mainViewModel | searchPageData = { searchPageData | focusLocation = EnteringQuery } }

                                    else
                                        { mainViewModel | searchPageData = { searchPageData | rows = Just newRows } }

                                Nothing ->
                                    -- TODO: shouldn't be possible to be be "BrowsingContent" and not have any rows to browse
                                    { mainViewModel | searchPageData = { searchPageData | focusLocation = EnteringQuery } }

                Right ->
                    case searchPageData.focusLocation of
                        EnteringQuery ->
                            let
                                ( newButtons, limitHit ) =
                                    tryZipN 7 Right searchPageData.buttons
                            in
                            if limitHit then
                                case searchPageData.rows of
                                    Just _ ->
                                        { mainViewModel | searchPageData = { searchPageData | focusLocation = BrowsingContent } }

                                    Nothing ->
                                        mainViewModel

                            else
                                { mainViewModel | searchPageData = { searchPageData | buttons = newButtons } }

                        BrowsingContent ->
                            case searchPageData.rows of
                                Just rowHolderZ ->
                                    let
                                        ( newRows, _ ) =
                                            tryZipN 1 Right rowHolderZ
                                    in
                                    { mainViewModel | searchPageData = { searchPageData | rows = Just newRows } }

                                Nothing ->
                                    -- TODO: shouldn't be possible to be be "BrowsingContent" and not have any rows to browse
                                    { mainViewModel | searchPageData = { searchPageData | focusLocation = EnteringQuery } }

        MyList ->
            case pageDirection of
                Right ->
                    case myListData.focusLocation of
                        FavoriteShows ->
                            { mainViewModel | myListData = { myListData | focusLocation = Watchlist, scrollPosition = 380 } }

                        Watchlist ->
                            { mainViewModel | myListData = { myListData | focusLocation = History, scrollPosition = 730 } }

                        History ->
                            mainViewModel

                Left ->
                    case myListData.focusLocation of
                        FavoriteShows ->
                            mainViewModel

                        Watchlist ->
                            { mainViewModel | myListData = { myListData | focusLocation = FavoriteShows, scrollPosition = 0 } }

                        History ->
                            { mainViewModel | myListData = { myListData | focusLocation = Watchlist, scrollPosition = 380 } }

        MyStation ->
            let
                ( one, _ ) =
                    tryZipN 1 pageDirection mainViewModel.settingsData
            in
            { mainViewModel | settingsData = one }

        Profile ->
            mainViewModel

        Cats ->
            mainViewModel

        Stack ->
            let
                ( currentShowDetailsPageData, poppedStack ) =
                    Stack.pop mainViewModel.stack

                newStack =
                    case currentShowDetailsPageData of
                        Nothing ->
                            mainViewModel.stack

                        Just x ->
                            Stack.push (navigateStackContentOuter x pageDirection) poppedStack
            in
            { mainViewModel | stack = newStack }


initializeEverything : ContentResponse -> ContentResponse -> ContentResponse -> FavoriteStationsCollectionContent -> ProfileResource -> DeviceId -> DeviceType -> MainViewModel
initializeEverything homeResponse resumeWatchingResponse showsResponse mainStation profileResource deviceId deviceType =
    { homeData = initializeHomeData homeResponse resumeWatchingResponse
    , showsPageData = initializeShowsData showsResponse
    , searchPageData = initialSearchPageData
    , settingsData = initialSettingsCards
    , myListData = initializeMyListData
    , focusLocation = PageContent
    , stack = Stack.initialise
    , cat = NotAsked
    , currentPage = Home
    , previousPage = Home
    , station = mainStation
    , profile = profileResource
    , deviceId = deviceId
    , deviceType = deviceType
    }


pushStack : MainViewModel -> DetailsPage -> MainViewModel
pushStack model detailsPage =
    { model | stack = Stack.push detailsPage model.stack, currentPage = Stack }


initializeHomeData : ContentResponse -> ContentResponse -> HomeData
initializeHomeData homeResponse resumeWatchingResponse =
    let
        filteredHome : List Collection
        filteredHome =
            List.filter (\x -> not (isStub x)) homeResponse.collections

        filteredResumeWatching =
            List.filter (\x -> not (isStub x)) resumeWatchingResponse.collections

        featuredVideos : RowHolder
        featuredVideos =
            collectionsToRowHolderFeaturedVideos filteredHome

        -- TODO: actually fetch resume watching when it's a stub
        resumeWatching : Maybe RowHolder
        resumeWatching =
            collectionsToRowHolderResumeWatching filteredResumeWatching

        theRest : List RowHolder
        theRest =
            collectionsToRowHolderTheRest homeResponse.collections

        rows : Zipper RowHolder
        rows =
            case resumeWatching of
                Nothing ->
                    (homeHeroButtons :: featuredVideos :: homeBrowseButtons :: theRest)
                        |> fromList
                        |> withDefault dummyFeaturedVideosRow

                Just resumeWatchingRowHolder ->
                    (homeHeroButtons :: featuredVideos :: homeBrowseButtons :: resumeWatchingRowHolder :: theRest)
                        |> fromList
                        |> withDefault dummyFeaturedVideosRow

        scrollPosition =
            0
    in
    { rows = rows, scrollPosition = scrollPosition }


incomingSearchData : MainViewModel -> WebData ContentResponse -> MainViewModel
incomingSearchData mainViewModel response =
    let
        newRows : List RowHolder
        newRows =
            case response of
                RemoteData.Success x ->
                    mapToRowHolders x.collections

                RemoteData.NotAsked ->
                    mapToRowHolders []

                RemoteData.Loading ->
                    mapToRowHolders []

                RemoteData.Failure _ ->
                    mapToRowHolders []

        -- mainViewModel
        newRowsZ : Zipper RowHolder
        newRowsZ =
            newRows |> fromList |> withDefault dummyShowItemRow

        currentSearchPageData : SearchPageData
        currentSearchPageData =
            mainViewModel.searchPageData

        newSearchPageData : SearchPageData
        newSearchPageData =
            { currentSearchPageData
                | rows = Just newRowsZ
            }
    in
    { mainViewModel | searchPageData = newSearchPageData }


getCurrentFeaturedVideo : MainViewModel -> VideoContent
getCurrentFeaturedVideo mainViewModel =
    let
        maybeSecond =
            mainViewModel.homeData.rows |> first |> next

        second : RowHolder
        second =
            case maybeSecond of
                Nothing ->
                    { row =
                        FeaturedVideoRow
                            (singleton
                                dummyVideoContent
                            )
                    , title = Nothing
                    , horizontalScrollPosition = Nothing
                    }

                Just x ->
                    current x
    in
    case second.row of
        FeaturedVideoRow fvr ->
            current fvr

        _ ->
            dummyVideoContent
