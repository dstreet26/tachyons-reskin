module Page.DetailsPage exposing (navigateStackContentInner, responseToDetailsPage)

import Data.ContentResponse
    exposing
        ( Collection(..)
        , ContentResponse
        , MultipleContent(..)
        , Resource(..)
        , ShowContent
        , VideoContent
        , dummyVideoContent
        )
import Data.DetailsPage
    exposing
        ( DetailsPage(..)
        , ShowDetailsPageData
        , SubNav(..)
        , VideoDetailsFocusLocation(..)
        , VideoDetailsPageData
        , secondaryButtons
        )
import Data.Main exposing (PlaceholderDirection)
import Data.Row exposing (Row(..), dummyShowItemRow, mapCollectionToRowHolder)
import Data.ShowResource exposing (ShowResource)
import Data.VideoResource exposing (VideoResource)
import DemoData exposing (playWatchButtons)
import List exposing (filterMap)
import List.Zipper exposing (Zipper, current, fromCons, fromList, mapCurrent, withDefault)
import RemoteData exposing (WebData)
import Utils exposing (tryZipN)


responseToDetailsPage : WebData ContentResponse -> Maybe DetailsPage
responseToDetailsPage result =
    case result of
        RemoteData.Success response ->
            case response.resource of
                Just x ->
                    case x of
                        VideoR y ->
                            Just (VideoDetailsPage (responseToVideoDetailsPageData y response.collections))

                        ShowR y ->
                            Just (ShowDetailsPage (responseToShowDetailsPageData y response.collections))

                Nothing ->
                    Nothing

        _ ->
            Nothing


responseToShowDetailsPageData : ShowResource -> List Collection -> ShowDetailsPageData
responseToShowDetailsPageData resource collections =
    { resource = resource
    , rows = collections |> filterMap mapCollectionToRowHolder |> fromList |> withDefault dummyShowItemRow
    }


responseToVideoDetailsPageData : VideoResource -> List Collection -> VideoDetailsPageData
responseToVideoDetailsPageData resource collections =
    { resource = resource
    , subNavs = makeSubNavs resource collections
    , heroButtonRow = playWatchButtons
    , secondaryButtonRow = secondaryButtons
    , focusLocation = MainButtons
    }


videoToMultiple : List VideoContent -> List MultipleContent
videoToMultiple input =
    input |> List.map toVideo


toVideo : VideoContent -> MultipleContent
toVideo video =
    VideoM video


toShow : ShowContent -> MultipleContent
toShow show =
    ShowM show


showToMultiple : List ShowContent -> List MultipleContent
showToMultiple input =
    input |> List.map toShow


toMultipleContentZ : Collection -> Zipper MultipleContent
toMultipleContentZ collection =
    let
        list : List MultipleContent
        list =
            case collection of
                InlineMultiple x ->
                    x.content

                -- case List.head x of
                --     Nothing ->
                --         Nothing
                --     Just (y::ys)
                --         from [] x.content
                InlineVideo x ->
                    videoToMultiple x.content

                InlineShow x ->
                    showToMultiple x.content

                _ ->
                    []
    in
    list |> fromList |> withDefault (VideoM dummyVideoContent)


makeSubNavs : VideoResource -> List Collection -> Zipper SubNav
makeSubNavs _ collections =
    fromCons Details
        (List.map
            (\collection ->
                MultipleColumn (getTitleFromCollection collection) (toMultipleContentZ collection)
            )
            collections
        )


getTitleFromCollection : Collection -> String
getTitleFromCollection collection =
    case collection of
        InlineSeason x ->
            x.title

        InlineShow x ->
            x.title

        InlineVideo x ->
            x.title

        InlineMultiple x ->
            x.title

        StubSeason x ->
            x.title

        StubShow x ->
            x.title

        StubVideo x ->
            x.title

        StubMultiple x ->
            x.title


navigateStackContentInner : DetailsPage -> PlaceholderDirection -> ( DetailsPage, Bool )
navigateStackContentInner pageData pageDirection =
    case pageData of
        VideoDetailsPage x ->
            navigateVideoDetailPageInner x pageDirection

        ShowDetailsPage x ->
            let
                rows =
                    x.rows

                currentInner =
                    x.rows |> current |> .row
            in
            case currentInner of
                HeroButtonRow y ->
                    let
                        ( newInner, limitHit ) =
                            tryZipN 1 pageDirection y

                        newRows =
                            mapCurrent (\z -> { z | row = HeroButtonRow newInner }) rows
                    in
                    ( ShowDetailsPage { x | rows = newRows }, limitHit )

                SecondaryButtonRow _ ->
                    -- FIXME:
                    ( ShowDetailsPage x, False )

                FeaturedVideoRow y ->
                    let
                        ( newInner, limitHit ) =
                            tryZipN 1 pageDirection y

                        newRows =
                            mapCurrent (\z -> { z | row = FeaturedVideoRow newInner }) rows
                    in
                    ( ShowDetailsPage { x | rows = newRows }, limitHit )

                BrowseButtonRow y ->
                    let
                        ( newInner, limitHit ) =
                            tryZipN 1 pageDirection y

                        newRows =
                            mapCurrent (\z -> { z | row = BrowseButtonRow newInner }) rows
                    in
                    ( ShowDetailsPage { x | rows = newRows }, limitHit )

                VideoItemRow y ->
                    let
                        ( newInner, limitHit ) =
                            tryZipN 1 pageDirection y

                        newRows =
                            mapCurrent (\z -> { z | row = VideoItemRow newInner }) rows
                    in
                    ( ShowDetailsPage { x | rows = newRows }, limitHit )

                ShowItemRow y ->
                    let
                        ( newInner, limitHit ) =
                            tryZipN 1 pageDirection y

                        newRows =
                            mapCurrent (\z -> { z | row = ShowItemRow newInner }) rows
                    in
                    ( ShowDetailsPage { x | rows = newRows }, limitHit )

                MultipleItemRow y ->
                    let
                        ( newInner, limitHit ) =
                            tryZipN 1 pageDirection y

                        newRows =
                            mapCurrent (\z -> { z | row = MultipleItemRow newInner }) rows
                    in
                    ( ShowDetailsPage { x | rows = newRows }, limitHit )


navigateVideoDetailPageInner : VideoDetailsPageData -> PlaceholderDirection -> ( DetailsPage, Bool )
navigateVideoDetailPageInner x pageDirection =
    case x.focusLocation of
        MainButtons ->
            let
                ( newInner, limitHit ) =
                    tryZipN 1 pageDirection x.heroButtonRow
            in
            ( VideoDetailsPage { x | heroButtonRow = newInner }, limitHit )

        SecondaryButtons ->
            let
                ( newInner, limitHit ) =
                    tryZipN 1 pageDirection x.secondaryButtonRow
            in
            ( VideoDetailsPage { x | secondaryButtonRow = newInner }, limitHit )

        SubNavNav ->
            let
                ( newInner, limitHit ) =
                    tryZipN 1 pageDirection x.subNavs
            in
            ( VideoDetailsPage { x | subNavs = newInner }, limitHit )

        SubNav ->
            -- FIXME: Select "My List" - probably inner focuslocation of Left/Right
            ( VideoDetailsPage x, False )
