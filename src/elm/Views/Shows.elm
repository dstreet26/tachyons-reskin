module Views.Shows exposing (viewShowsPage)

import Data.ContentResponse exposing (ShowContent)
import Data.ShowsPage exposing (ShowCategory, ShowPageData, ShowPageLocation(..))
import Html exposing (Html, div, text)
import Html.Attributes exposing (class, classList)
import List.Zipper exposing (Zipper, current)
import Views.Other exposing (viewBeforeAfterSelected, viewCroppedImage)


viewShowsPage : ShowPageData -> Html msg
viewShowsPage showsPageData =
    div
        [ class "fl w-100 pl6 pt5 z-2 relative"
        ]
        [ div [ class "" ]
            [ div [ class "di" ] [ text "Shows" ]
            , div [ class "di" ] [ text "All Shows" ]
            ]
        , div [ class "" ]
            [ viewShowPageButtonsRow showsPageData.showCategories True
            ]
        , div [ class "" ]
            [ viewShowPageRows (showsPageData.showCategories |> current |> .items) (showsPageData.focusLocation == Items)
            ]
        ]


viewShowPageButtonsRow : Zipper ShowCategory -> Bool -> Html msg
viewShowPageButtonsRow categories isActive =
    div [ class "mb6 f30" ]
        (viewBeforeAfterSelected categories isActive viewShowPageCategoryButton)


viewShowPageRows : Zipper ShowContent -> Bool -> Html msg
viewShowPageRows items isActive =
    div [ class "mt6 f30" ]
        (viewBeforeAfterSelected items isActive viewShowPageItem)


viewShowPageItem : Bool -> ShowContent -> Html msg
viewShowPageItem isActive showItem =
    div [ class "dib" ]
        [ div
            [ class "showPageShowItemDimensions bg-gray mr4 mb2"
            , classList
                [ ( "bw2", isActive )
                , ( "ba", isActive )
                ]
            ]
            [ viewCroppedImage showItem.imageUrl ( 300, 450 ) ]
        ]


viewShowPageCategoryButton : Bool -> ShowCategory -> Html msg
viewShowPageCategoryButton isActive showCategory =
    div
        [ class "dib pv3 ph4 b theme-bg-button-category-blue mh3 br3"
        , classList
            [ ( "bw2", isActive )
            , ( "teal", isActive )
            ]
        ]
        [ text showCategory.title ]
