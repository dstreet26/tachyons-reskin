module Views.Search exposing (viewSearchPage)

import Data.Row exposing (Row(..), RowHolder)
import Data.Search exposing (SearchButton(..), SearchPageData, SearchPageLocation(..))
import Html exposing (Html, div, text)
import Html.Attributes exposing (class, classList)
import List.Zipper exposing (Zipper)
import Views.Other exposing (viewBeforeAfterSelected)
import Views.Row exposing (viewMultipleItemRow, viewShowItemRow, viewVideoItemRow)


viewSearchPage : SearchPageData -> Html msg
viewSearchPage searchPageData =
    div
        [ class "fl w-100 pl6 pt5 z-2 relative"
        ]
        [ div [ class "" ]
            [ div [ class "bg-dark-gray white" ] [ text ("QUERY: " ++ searchPageData.query) ]
            ]
        , div [ class "" ]
            [ viewSearchButtons searchPageData.buttons (searchPageData.focusLocation == EnteringQuery)
            ]
        , viewSearchResultsMaybe searchPageData.rows (searchPageData.focusLocation == BrowsingContent)
        ]


viewSearchResultsMaybe : Maybe (Zipper RowHolder) -> Bool -> Html msg
viewSearchResultsMaybe rowsM isActive =
    case rowsM of
        Nothing ->
            div [ class "" ]
                []

        Just rows ->
            div [ class "" ]
                [ viewSearchResults rows isActive ]


viewSearchButtons : Zipper SearchButton -> Bool -> Html msg
viewSearchButtons items isActive =
    div [ class "mb6 f30 searchButtonGridWidth" ]
        (viewBeforeAfterSelected items isActive viewSearchButton)


viewSearchResults : Zipper RowHolder -> Bool -> Html msg
viewSearchResults items isActive =
    div [ class "mt6 f30" ]
        (viewBeforeAfterSelected items isActive viewSearchPageRowHolder)


viewSearchPageRowHolder : Bool -> RowHolder -> Html msg
viewSearchPageRowHolder isActive row =
    case row.row of
        HeroButtonRow _ ->
            div [] [ text "FIXME" ]

        SecondaryButtonRow _ ->
            div [] [ text "FIXME" ]

        FeaturedVideoRow _ ->
            div [] [ text "FIXME" ]

        BrowseButtonRow _ ->
            div [] [ text "FIXME" ]

        VideoItemRow videoContentZ ->
            div [] (viewVideoItemRow videoContentZ isActive)

        ShowItemRow showContentZ ->
            div [] (viewShowItemRow showContentZ isActive)

        MultipleItemRow multipleContentZ ->
            div [] (viewMultipleItemRow multipleContentZ isActive)


viewSearchButton : Bool -> SearchButton -> Html msg
viewSearchButton isActive searchButton =
    let
        title =
            case searchButton of
                Letter x ->
                    String.fromChar x

                Clear ->
                    "Clear"

                Back ->
                    "Back"

                ManualSearch ->
                    "ManualSearch"
    in
    div
        [ class "dib pt10 pv20 tc searchButtonWidth b theme-bg-button-category-blue ttu"
        , classList
            [ ( "bw2", isActive )
            , ( "teal", isActive )
            ]
        ]
        [ text title ]
