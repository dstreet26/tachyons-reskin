module Views.Settings exposing (viewSettings)

import Data.Settings
    exposing
        ( AboutOptions
        , CurrentStationOptions
        , LoggedInOptions
        , SettingCardType(..)
        , SettingsCard
        , StationItemOptions
        )
import Html exposing (Html, div, img, text)
import Html.Attributes exposing (class, classList, src)
import List.Zipper exposing (Zipper)
import Views.Other exposing (getIcon, viewBeforeAfterSelected)


viewSettings : Zipper SettingsCard -> Bool -> Html msg
viewSettings settingsCards isActive =
    div [ class "tc " ]
        (div [ class "f2 pt3 mb3" ] [ text "Settings" ]
            :: viewBeforeAfterSelected settingsCards isActive viewSettingsCard
        )


viewSettingsCard : Bool -> SettingsCard -> Html msg
viewSettingsCard isSelected settingsCard =
    case settingsCard.settingCardType of
        CurrentStation currentStationOptions ->
            viewSettingsCurrentStation currentStationOptions isSelected

        StationItem stationItemOptions ->
            viewSettingsPassportItem stationItemOptions isSelected

        LoggedIn loggedInOptions ->
            viewSettingsLoggedIn loggedInOptions isSelected

        About aboutOptions ->
            viewSettingsAbout aboutOptions isSelected


viewSettingsCurrentStation : CurrentStationOptions -> Bool -> Html msg
viewSettingsCurrentStation currentStationOptions isSelected =
    div [ class "mw6  center bg-black-50 pa3 mb3", classList [ ( "settingsCardActive", isSelected ), ( "settingsCard", not isSelected ) ] ]
        [ img
            [ class ""
            , src
                currentStationOptions.imageSource
            ]
            []
        , div [ class "f3" ] [ text "Change Station" ]
        ]


viewSettingsPassportItem : StationItemOptions -> Bool -> Html msg
viewSettingsPassportItem stationItemOptions isSelected =
    div [ class "mw6  bg-black-50 center pa3 mb3", classList [ ( "settingsCardActive", isSelected ), ( "settingsCard", not isSelected ) ] ]
        [ getIcon "passport-logo-blue" []
        , div [ class "di ttu b pl3 pr2" ] [ text stationItemOptions.name ]
        , div [ class "di pr2" ] [ text "|" ]
        , div [ class "di fw1 pr3" ] [ text "Passport" ]
        , if stationItemOptions.isActivated then
            div [ class "di ttu blue" ] [ text "Active" ]

          else
            div [ class "di ttu blue" ] [ text "Become a Member" ]
        ]


viewSettingsLoggedIn : LoggedInOptions -> Bool -> Html msg
viewSettingsLoggedIn loggedInOptions isSelected =
    div [ class "mw6  bg-black-50 center pa3 mb3", classList [ ( "settingsCardActive", isSelected ), ( "settingsCard", not isSelected ) ] ]
        [ div [ class "di  b pr2" ] [ text loggedInOptions.email ]
        ]


viewSettingsAbout : AboutOptions -> Bool -> Html msg
viewSettingsAbout aboutOptions isSelected =
    div [ class "mw6  bg-black-50 center pa3 mb3", classList [ ( "settingsCardActive", isSelected ), ( "settingsCard", not isSelected ) ] ]
        [ div [ class "di  b pr2" ] [ text aboutOptions.title ]
        ]
