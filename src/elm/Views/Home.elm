module Views.Home exposing (viewHomePage)

import Data.ContentResponse exposing (VideoContent)
import Data.FocusLocation exposing (FocusLocation(..))
import Data.Home exposing (HomeData)
import Data.MainView exposing (MainViewModel)
import Html exposing (Html, div, img)
import Html.Attributes exposing (class, src)
import Page.MainView exposing (getCurrentFeaturedVideo)
import Views.Other exposing (viewBeforeAfterSelected)
import Views.Row exposing (viewContentRow, viewHeroInfo)


viewHomePage : MainViewModel -> Html msg
viewHomePage mainViewModel =
    div [ class "" ]
        [ viewHomeHeroCircle <| getCurrentFeaturedVideo mainViewModel
        , viewHomeHeroSection <| getCurrentFeaturedVideo mainViewModel
        , viewHomeMainSection mainViewModel.homeData (mainViewModel.focusLocation == PageContent)
        ]


viewHomeMainSection : HomeData -> Bool -> Html msg
viewHomeMainSection homeData isActive =
    div [ class "fl w-100 pl6 pt4 z-2 relative wct" ]
        (viewBeforeAfterSelected homeData.rows isActive viewContentRow)


viewHomeHeroCircle : VideoContent -> Html msg
viewHomeHeroCircle selected =
    div [ class "relative" ]
        [ div [ class "bg-gray hero-circle-clip z-1 absolute" ]
            [ img [ class "h-100", src selected.imageUrl ] [] ]
        ]


viewHomeHeroSection : VideoContent -> Html msg
viewHomeHeroSection videoContent =
    div [ class "fl w-100 pl6 z-2 relative" ]
        [ div [ class "pt6 pl3" ]
            [ viewHeroInfo videoContent
            ]
        ]
