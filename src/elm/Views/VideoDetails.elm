module Views.VideoDetails exposing (viewVideoDetailsPage)

import Data.ContentResponse exposing (MultipleContent(..), ShowContent, VideoContent)
import Data.DetailsPage exposing (SubNav(..), VideoDetailsFocusLocation(..), VideoDetailsPageData)
import Data.Row exposing (HeroButton, HeroButtonItem)
import Html exposing (Html, div, p, span, text)
import Html.Attributes exposing (class, classList, style)
import List.Zipper exposing (Zipper, current)
import Svg.Attributes exposing (height, width)
import Utils exposing (calculateProgressWidth, calculateProgressbarString)
import Views.ContentResponse
    exposing
        ( formatExpireDate
        , formatPremiereDate
        , formatVideoLabel
        , formatVideoLabelR
        , viewParentAbove
        )
import Views.Other exposing (getIcon, viewBeforeAfterSelected, viewCroppedImage)
import Views.Row exposing (viewHeroButton)


viewVideoDetailsPage : VideoDetailsPageData -> Html msg
viewVideoDetailsPage pageData =
    div [ class "fl w-100 mainViewLeftPadding z-2 relative" ]
        [ viewVideoDetailsHero pageData
        , viewVideoDetailsSubNavNav pageData (pageData.focusLocation == SubNavNav)
        , viewVideoDetailsSubNav pageData (pageData.focusLocation == SubNav)
        ]


viewVideoDetailsHero : VideoDetailsPageData -> Html msg
viewVideoDetailsHero pageData =
    let
        viewImage =
            div [ class "fl videoDetailsHeroImageDimensions" ]
                [ viewCroppedImage pageData.resource.images.assetMezzanine16x9 ( 1063, 620 ) ]

        viewDetails =
            viewVideoDetailsHeroDescription pageData

        viewHeroes =
            case pageData.resource.secondsWatched of
                Just x ->
                    case calculateProgressWidth x pageData.resource.duration of
                        0 ->
                            [ viewImage, viewDetails ]

                        _ ->
                            [ viewImage, viewProgressBar x pageData.resource.duration, viewDetails ]

                Nothing ->
                    [ viewImage, viewDetails ]
    in
    div [ class "fl w-100 mt6" ]
        viewHeroes


viewProgressBar : Int -> Int -> Html msg
viewProgressBar secondsWatched duration =
    div [ class "videoDetailsProgressBar bg-white-50", style "" "" ]
        [ viewProgressBarInner secondsWatched duration
        ]


viewProgressBarInner : Int -> Int -> Html msg
viewProgressBarInner secondsWatched duration =
    div
        [ class "theme-bg-coral pt10"
        , style "width" (calculateProgressbarString secondsWatched duration)
        ]
        []


viewVideoDetailsHeroDescription : VideoDetailsPageData -> Html msg
viewVideoDetailsHeroDescription pageData =
    div [ class "fl videoDetailsDescriptionWidth ml5" ]
        [ div []
            [ div [ class "ttu f28 light-gray mb3 b lh143" ]
                [ text
                    (case pageData.resource.parent of
                        Just p ->
                            viewParentAbove p

                        Nothing ->
                            "(no parent)"
                    )
                ]
            , div [ class "f48 b mb3" ]
                [ text pageData.resource.title ]
            , div [ class "f30 mb4 lh127" ]
                [ span [ class "" ] [ text <| formatVideoLabelR pageData.resource ]
                , span [ class "mh2" ]
                    [ pageData.resource.expireDate
                        |> Maybe.map formatExpireDate
                        |> Maybe.withDefault ""
                        |> text
                    ]
                ]
            , div [ class "f30 lh127" ]
                [ p []
                    [ text pageData.resource.descriptionShort ]
                ]
            , viewHeroButtonRow pageData.heroButtonRow (pageData.focusLocation == MainButtons)
            , viewSecondaryButtonRow pageData.secondaryButtonRow (pageData.focusLocation == SecondaryButtons)
            ]
        ]


viewVideoDetailsSubNavNav : VideoDetailsPageData -> Bool -> Html msg
viewVideoDetailsSubNavNav videoDetailsPageData isActive =
    div [ class "fl w-100 f32 fw5 pt5" ]
        [ div [ class "videoDetailsSubNavLeftPadding" ]
            (viewBeforeAfterSelected videoDetailsPageData.subNavs isActive viewSubNavNavTitle)
        ]


viewSubNavNavTitle : Bool -> SubNav -> Html msg
viewSubNavNavTitle isActive subNav =
    let
        out =
            case subNav of
                Details ->
                    "Details"

                MultipleColumn str _ ->
                    str
    in
    div
        [ class "di mr5 "
        , classList
            [ ( "theme-dark-gray", not isActive )
            ]
        ]
        [ text out ]


viewVideoDetailsSubNavTitle : String -> Html msg
viewVideoDetailsSubNavTitle str =
    div [ class "f32 fw5 mb3 theme-video-details-subnav-extras-title-white" ]
        [ text str ]


viewMultipleItemRow : Zipper MultipleContent -> Bool -> List (Html msg)
viewMultipleItemRow items isActive =
    viewBeforeAfterSelected items isActive viewMultipleItem


viewMultipleItem : Bool -> MultipleContent -> Html msg
viewMultipleItem isActive mContent =
    case mContent of
        VideoM x ->
            viewVideoItem isActive x

        ShowM x ->
            viewShowItem isActive x



-- TODO: Implement


viewShowItem : Bool -> ShowContent -> Html msg
viewShowItem isActive showContent =
    div [] [ viewShowItem isActive showContent ]


viewVideoItem : Bool -> VideoContent -> Html msg
viewVideoItem isActive videoContent =
    div [ class "fl videoDetailsSubNavExtrasRowDimensions mb61", classList [ ( "outline", isActive ) ] ]
        [ div [ class "fl videoDetailsSubNavExtrasImageDimensions mr50" ]
            [ viewCroppedImage videoContent.imageUrl ( 600, 337 )
            ]
        , div [ class "di" ]
            [ div [ class "f36 lh133 b mb2" ]
                [ text videoContent.title ]
            , div [ class "f28 mb3 lh143 theme-light-gray" ]
                [ span [ class "" ] [ text <| formatVideoLabel videoContent ]
                , span [ class "" ]
                    [ text "|" ]
                , span [ class "" ]
                    [ videoContent.expireDate |> Maybe.map formatExpireDate |> Maybe.withDefault "" |> text ]
                ]
            , div [ class "f30 lh127" ]
                [ p []
                    [ text videoContent.descriptionShort ]
                ]
            , div [ class "f30 h50 b fl tc" ]
                [ div [ class "fl bn bg-white-20 br-pill videoDetailsSubNavExtrasPlusDimensions" ]
                    [ getIcon "watch-latest" []
                    ]
                , div [ class "fl ml3 pt2" ]
                    [ text "My List" ]
                ]
            ]
        ]


viewVideoDetailsSubNav : VideoDetailsPageData -> Bool -> Html msg
viewVideoDetailsSubNav videoDetailsPageData isActive =
    case current videoDetailsPageData.subNavs of
        Details ->
            div [ class "fl pt4" ]
                [ div [ class "videoDetailsSubNavExtrasLeftPadding" ]
                    [ div [ class "f32 b lh219 mb2 ttu" ] [ text "About" ]
                    , div [ class "f30 lh127 mb2" ] [ text videoDetailsPageData.resource.descriptionLong ]
                    , div [ class "f30 lh127 mb4" ]
                        [ span []
                            [ text "Aired: " ]
                        , span [ class "" ]
                            [ text <| formatPremiereDate videoDetailsPageData.resource.premiereDate ]
                        , span [ class "mh2" ]
                            [ text "|" ]
                        , case videoDetailsPageData.resource.expireDate of
                            Just expireDate ->
                                span [ class "" ]
                                    [ text ("Expires: " ++ formatPremiereDate expireDate ++ "|") ]

                            Nothing ->
                                span [ class "" ]
                                    [ text "|" ]
                        , span [ class "mh2" ]
                            [ text ("Rating: " ++ videoDetailsPageData.resource.contentRating) ]
                        ]
                    , div [ class "f32 b lh219 mb2 ttu" ] [ text "Sponsored By:" ]
                    , div [ class "f28 lh136" ] [ text "FIXME: sponsorship message here...." ]
                    ]
                ]

        MultipleColumn str multipleContentZ ->
            div [ class "fl pt4" ]
                [ div [ class "videoDetailsSubNavExtrasLeftPadding" ]
                    (viewVideoDetailsSubNavTitle str :: viewMultipleItemRow multipleContentZ isActive)
                ]


viewHeroButtonRow : Zipper HeroButtonItem -> Bool -> Html msg
viewHeroButtonRow heroButtons isActive =
    div [ class "f30 fw5 pt3" ]
        (viewBeforeAfterSelected heroButtons isActive viewHeroButton)


viewSecondaryButtonRow : Zipper HeroButton -> Bool -> Html msg
viewSecondaryButtonRow secondaryButtons isActive =
    div [ class "fl f27 fw5 mt4" ]
        (viewBeforeAfterSelected secondaryButtons isActive viewSecondaryButton)


viewSecondaryButton : Bool -> HeroButton -> Html msg
viewSecondaryButton isActive button =
    div
        [ class "fl videoDetailsButtonSecondaryDimensions tc mr3 pv3"
        , classList
            [ ( "ba", isActive )
            , ( "br1", isActive )
            , ( "b--white", isActive )
            ]
        ]
        [ getIcon button.icon [ height "40", width "30" ]
        , div [ class "db ml3" ]
            [ text button.text ]
        ]
