module Views.ContentResponse exposing (formatExpireDate, formatPremiereDate, formatVideoLabel, formatVideoLabelR, getTitleFromParent, viewParentAbove)

import Data.ContentResponse exposing (VideoContent)
import Data.Parent exposing (Parent(..))
import Data.VideoResource exposing (VideoResource)
import Data.VideoType exposing (VideoType(..))
import String exposing (fromInt, right)
import Time exposing (Posix)
import Utils exposing (formatDuration, intFromMonth)



-- formatDuration2 : Float -> String
-- formatDuration2 duration =
--     let
--         hours =
--             duration // 3600
--         minutesRem =
--             remainderBy 3600 duration
--         minutes =
--             minutesRem // 60
--         secondsRem =
--             remainderBy 60 minutesRem
--     in
--     if hours < 1 then
--         if minutes < 1 then
--             -- fromInt secondsRem ++ "s"
--             secondsRem
--         else
--             -- fromInt minutes ++ "m " ++ fromInt secondsRem ++ "s"
--             minutes ++ ":" ++ secondsRem
--     else
--          hours ++ ":" ++  minutes ++ ":" ++  secondsRem


formatPremiereDate : Posix -> String
formatPremiereDate premiereDate =
    let
        toMonth =
            Time.toMonth Time.utc premiereDate |> intFromMonth |> fromInt

        toYear =
            Time.toYear Time.utc premiereDate |> fromInt

        toDay =
            Time.toDay Time.utc premiereDate |> fromInt
    in
    toDay ++ "/" ++ toMonth ++ "/" ++ toYear


formatExpireDate : Posix -> String
formatExpireDate premiereDate =
    let
        toMonth =
            Time.toMonth Time.utc premiereDate |> intFromMonth |> fromInt

        toYear =
            Time.toYear Time.utc premiereDate |> fromInt |> right 2

        toDay =
            Time.toDay Time.utc premiereDate |> fromInt
    in
    toDay ++ "/" ++ toMonth ++ "/" ++ toYear


formatParentPrefix : Parent -> VideoType -> Posix -> String
formatParentPrefix parent videoType premiereDate =
    case parent of
        Season x ->
            case videoType of
                FullLength ->
                    "Check business rules"

                Clip ->
                    "Clip: S" ++ fromInt x.ordinal

                Preview ->
                    "Preview: S" ++ fromInt x.ordinal

        Special _ ->
            case videoType of
                FullLength ->
                    "Special"

                Clip ->
                    "Clip: Special"

                Preview ->
                    "Preview: Special"

        Show _ ->
            case videoType of
                FullLength ->
                    "Check business rules"

                Clip ->
                    "Clip"

                Preview ->
                    "Preview"

        Episode x ->
            -- This explosion of rules is supposed to directly map to the google doc for standardization
            -- This is why you see (seasonsCount = 1) instead of (seasonCount > 1) and (not displayEpisodeNumber)
            -- Likewise for (not displayEpisodeNumber) vs (displayEpisodeNumber)
            let
                episodeOrdinal =
                    fromInt x.ordinal

                seasonOrdinal =
                    fromInt x.season.ordinal

                seasonsCount =
                    x.season.show.seasonsCount

                displayEpisodeNumber =
                    x.season.show.displayEpisodeNumber
            in
            case videoType of
                FullLength ->
                    if seasonsCount == 1 then
                        if not displayEpisodeNumber then
                            formatPremiereDate premiereDate

                        else
                            "Ep" ++ episodeOrdinal

                    else if not displayEpisodeNumber then
                        formatPremiereDate premiereDate

                    else
                        "S" ++ seasonOrdinal ++ " Ep" ++ episodeOrdinal

                Clip ->
                    if seasonsCount == 1 then
                        if not displayEpisodeNumber then
                            "Clip: " ++ formatPremiereDate premiereDate

                        else
                            "Clip: " ++ "Ep" ++ episodeOrdinal

                    else if not displayEpisodeNumber then
                        "Clip: " ++ "Ep" ++ episodeOrdinal

                    else
                        "Clip: " ++ seasonOrdinal ++ " Ep" ++ episodeOrdinal

                Preview ->
                    if seasonsCount == 1 then
                        if not displayEpisodeNumber then
                            "Preview: " ++ formatPremiereDate premiereDate

                        else
                            "Preview: " ++ "Ep" ++ episodeOrdinal

                    else if not displayEpisodeNumber then
                        "Preview: " ++ "Ep" ++ episodeOrdinal

                    else
                        "Preview: " ++ seasonOrdinal ++ " Ep" ++ episodeOrdinal


formatVideoLabel : VideoContent -> String
formatVideoLabel videoItem =
    -- TODO: get clarification on franchise
    let
        runtime =
            formatDuration videoItem.duration

        out =
            formatParentPrefix videoItem.parent videoItem.videoType videoItem.premiereDate
    in
    out ++ "|" ++ runtime


formatVideoLabelR : VideoResource -> String
formatVideoLabelR videoResource =
    -- TODO: get clarification on franchise
    let
        runtime =
            formatDuration videoResource.duration

        out =
            case videoResource.parent of
                Just p ->
                    formatParentPrefix p videoResource.videoType videoResource.premiereDate

                Nothing ->
                    -- FIXME: if a videoresource doesn't have a parent, how to tell if it's a special or episode?
                    "FIXME"
    in
    out ++ "|" ++ runtime


getTitleFromParent : Parent -> String
getTitleFromParent parent =
    case parent of
        Season x ->
            x.show.title

        Special x ->
            x.show.title

        Show x ->
            x.title

        Episode x ->
            x.season.show.title


viewParentAbove : Parent -> String
viewParentAbove parent =
    case parent of
        Season _ ->
            "Season"

        Special x ->
            case x.show.franchise of
                Nothing ->
                    "Special " ++ "|" ++ x.show.title

                Just franchise ->
                    "Special From " ++ franchise.title

        Show x ->
            case x.franchise of
                Nothing ->
                    "Show " ++ x.title

                Just y ->
                    "From " ++ y.title

        Episode x ->
            case x.season.show.franchise of
                Nothing ->
                    "Episode " ++ x.season.title

                Just franchise ->
                    "Episode | From " ++ franchise.title
