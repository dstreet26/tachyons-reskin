module Views.MyList exposing (viewMyListPage)

import Data.MyList exposing (MyListData, MyListFocusLocation(..))
import Data.Row exposing (RowHolder)
import Html exposing (Html, div, text)
import Html.Attributes exposing (class)
import RemoteData exposing (WebData)
import Views.Row exposing (viewContentRow)


viewMyListPage : MyListData -> Bool -> Html msg
viewMyListPage myList isActive =
    div
        [ class "fl w-100 pt3 pl6"
        ]
        [ div [ class "" ]
            [ viewMyListRow (isActive && myList.focusLocation == FavoriteShows) myList.favoriteShows
            , viewMyListRow (isActive && myList.focusLocation == Watchlist) myList.watchlist
            , viewMyListRow (isActive && myList.focusLocation == History) myList.history
            ]
        ]


viewMyListRow : Bool -> WebData RowHolder -> Html msg
viewMyListRow isActive webData =
    case webData of
        RemoteData.NotAsked ->
            div [] [ text "notasked" ]

        RemoteData.Success x ->
            viewContentRow isActive x

        RemoteData.Loading ->
            div [] [ text "loading" ]

        RemoteData.Failure _ ->
            div [] [ text "failure" ]
