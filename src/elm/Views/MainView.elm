module Views.MainView exposing (viewMainPage)

import Data.FocusLocation exposing (FocusLocation(..))
import Data.MainView exposing (MainViewModel, MainViewMsg, Page(..))
import Html exposing (Html, div, text)
import Html.Attributes exposing (class, style)
import Stack exposing (top)
import Views.Cat exposing (viewCat)
import Views.DetailsPage exposing (viewStackPage)
import Views.Home exposing (viewHomePage)
import Views.MyList exposing (viewMyListPage)
import Views.Other exposing (viewLogo)
import Views.Search exposing (viewSearchPage)
import Views.Settings exposing (viewSettings)
import Views.Shows exposing (viewShowsPage)
import Views.Sidebar exposing (viewSidebar)


viewMainPage : MainViewModel -> Html MainViewMsg
viewMainPage model =
    div [ class "" ]
        [ viewLogo model.station.images.whiteCobrandedLogo
        , viewSidebar model
        , viewPage model
        ]


viewPage : MainViewModel -> Html MainViewMsg
viewPage model =
    let
        ( pageView, offSet ) =
            case model.currentPage of
                Home ->
                    ( viewHomePage model, model.homeData.scrollPosition |> negate |> String.fromInt )

                Shows ->
                    ( viewShowsPage model.showsPageData, "0" )

                Search ->
                    ( viewSearchPage model.searchPageData, "0" )

                MyList ->
                    ( viewMyListPage model.myListData (model.focusLocation == PageContent), model.myListData.scrollPosition |> negate |> String.fromInt )

                MyStation ->
                    ( viewSettings model.settingsData (model.focusLocation == PageContent), "0" )

                Profile ->
                    ( viewSettings model.settingsData (model.focusLocation == PageContent), "0" )

                Cats ->
                    ( viewCat model.cat model.focusLocation, "0" )

                Stack ->
                    case top model.stack of
                        Nothing ->
                            ( div [] [ text "TEST: empty stack" ], "0" )

                        Just detailsPage ->
                            ( viewStackPage detailsPage, "0" )
    in
    div [ class "transition1", style "transform" ("translateY(" ++ offSet ++ "px)") ] [ pageView ]
