module Views.Other exposing (getIcon, viewActivation, viewBeforeAfterSelected, viewCroppedImage, viewImage, viewLogo, viewSplash)

import Html exposing (Html, br, div, img, text)
import Html.Attributes exposing (class, src)
import List exposing (map)
import List.Zipper exposing (Zipper, after, before, current)
import MyIcons exposing (..)
import String exposing (fromInt)
import Svg.Attributes exposing (height, width)


viewBeforeAfterSelected : Zipper a -> Bool -> (Bool -> a -> Html msg) -> List (Html msg)
viewBeforeAfterSelected zipper isActive viewFunction =
    let
        selected =
            current zipper |> viewFunction isActive
    in
    (before zipper |> map (viewFunction False)) ++ selected :: (after zipper |> map (viewFunction False))


viewSplash : Html msg
viewSplash =
    div [ class "theme-bg-splash-blue forcer" ]
        [ div [ class "tc pt340" ]
            [ getIcon "pbs-national"
                [ width "961px", height "400px" ]
            ]
        ]


viewActivation : Html msg
viewActivation =
    div [ class "activation-background cover forcer" ]
        [ div [ class "relative" ]
            [ div [ class "theme-bg-dark-blue activation-circle-clip absolute" ]
                []
            ]
        , div [ class "relative activationOffset" ]
            [ getIcon "pbs-national"
                [ width "281px", height "117px" ]
            , div [ class "f54 mt4 mb4" ]
                [ text "Watch PBS anytime, anywhere" ]
            , div [ class "f32 mb5 fw5" ]
                [ text "Discover full-length episodes and archived seasons."
                , br []
                    []
                , text "Access your personalized watchlist and favorite shows."
                , br []
                    []
                , text "Connect with your local PBS station."
                ]
            , div [ class "b bn white tc pt20 activationButtonWidth h76 br-pill f36 theme-bg-button-watch-blue" ]
                [ text "Activate Now" ]
            ]
        ]


viewLogo : String -> Html msg
viewLogo imageSource =
    div [ class "stationLogoContainer" ]
        [ div [ class "stationLogoBackground" ] []
        , img [ class "stationLogoImage", src imageSource ] []
        ]


viewImage : String -> Html msg
viewImage source =
    img [ src source, class "w-100" ] []


viewCroppedImage : String -> ( Int, Int ) -> Html msg
viewCroppedImage url ( width, height ) =
    viewImage (url ++ ".crop." ++ fromInt width ++ "x" ++ fromInt height ++ ".jpg")


getIcon x attrs =
    case x of
        "play" ->
            play attrs

        "plus" ->
            plus attrs

        "preview" ->
            preview attrs

        "home" ->
            home attrs

        "star" ->
            star attrs

        "layers" ->
            layers attrs

        "search" ->
            search attrs

        "check" ->
            check attrs

        "map-pin" ->
            mapPin attrs

        "user" ->
            user attrs

        "watch-latest" ->
            watchLatest attrs

        "pbs-national" ->
            pbsNational3x attrs

        "restart" ->
            restart attrs

        "settings" ->
            settings attrs

        "next" ->
            next attrs

        "pause" ->
            pause attrs

        _ ->
            underConstruction attrs
