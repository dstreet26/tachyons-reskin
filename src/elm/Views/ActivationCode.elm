module Views.ActivationCode exposing (viewActivationCode)

import Data.ActivationCode exposing (AuthCode)
import Html exposing (Html, div, p, text)
import Html.Attributes exposing (class)
import QRCode
import RemoteData exposing (WebData)
import Svg.Attributes exposing (height, width)


qrCodeView : String -> Html msg
qrCodeView message =
    QRCode.fromString message
        |> Result.map (QRCode.toSvg [ width "200", height "200" ])
        |> Result.withDefault
            (text "Error while encoding to QRCode.")


viewActivationCode : WebData AuthCode -> Html msg
viewActivationCode webDataAuthCode =
    div [ class "pt6" ]
        [ div [ class "w-25 fl mt6 pt6 ph6 tc" ]
            [ div [ class "bg-white dib" ]
                [ case webDataAuthCode of
                    RemoteData.NotAsked ->
                        text "NotAsked"

                    RemoteData.Failure _ ->
                        text "Failure"

                    RemoteData.Loading ->
                        text "Loading"

                    RemoteData.Success resp ->
                        qrCodeView ("https://pbs.org/activate/?authCode=" ++ resp.authCode)
                ]
            ]
        , div [ class "w-75 fl f32 pt6 ph3 " ]
            [ p []
                [ text "1. From a mobile device or computer, visit:" ]
            , p [ class "theme-link-blue b f50" ]
                [ text "www.pbs.org/activate" ]
            , p [ class "mt4" ]
                [ text "2. Enter code:" ]
            , p [ class "f120 ma0" ]
                [ text (RemoteData.unwrap "......." (\x -> x.authCode) webDataAuthCode) ]
            , p [ class "pt3 f32 fw5 lh-copy" ]
                [ text "Once you’re activated, this screen automatically updates. This may take up to 30 seconds." ]
            , div [ class "f2 bn white tc br-pill pt3 h76 w-50 theme-bg-button-watch-blue" ]
                [ text "Click to get a new Auth Code" ]
            ]
        ]
