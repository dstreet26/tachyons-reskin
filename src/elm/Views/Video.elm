module Views.Video exposing (viewVideoPage)

import Bounce
import Data.MainView exposing (MainViewModel)
import Data.Video
    exposing
        ( CaptionStylingFocusLocation(..)
        , ModalFocusLocation(..)
        , PlayerState(..)
        , VideoElementMsg
        , VideoFocusLocation(..)
        , VideoModel
        , VideoQualityAndPlaybackFocusLocation(..)
        )
import Data.VideoResource exposing (ClosedCaption)
import Html exposing (Html, div, text)
import Html.Attributes exposing (class, classList, style)
import List.Zipper exposing (current)
import String exposing (fromFloat)
import Svg.Attributes exposing (width)
import Utils exposing (calculateProgressbarString2, formatDuration)
import Views.Other exposing (getIcon)



-- TODO: now that the video element is global, you don't need to use display:none and can use normal logic to determine which overlay(s) to show


viewVideoPage : MainViewModel -> VideoModel -> Html VideoElementMsg
viewVideoPage model videoModel =
    div [ class " absolute forcer z-3" ]
        [ div [ class "absolute forcer z-4", classList [ ( "dn", videoModel.focusLocation == OptionsModal || videoModel.focusLocation == OptionsModalBody ) ] ] [ viewOverlay videoModel ]
        , div [ class "absolute forcer z-5 pa5 o-90 bg-black ", classList [ ( "dn", videoModel.focusLocation /= OptionsModal && videoModel.focusLocation /= OptionsModalBody ) ] ] [ viewOptionsModal videoModel ]
        ]


viewOptionsModal : VideoModel -> Html VideoElementMsg
viewOptionsModal model =
    div
        [ style "width" "100%"
        , style "height" "100%"
        , class "bg-blue"
        ]
        [ div [ class "pt3 w-100 fl" ] [ viewModalTitles model ]
        , div [ class "fl w-100  f2 pa4" ] [ viewModalBody model ]
        ]


viewModalTitles : VideoModel -> Html VideoElementMsg
viewModalTitles model =
    div [ class "f1" ]
        [ div [ classList [ ( "underline", model.modalFocusLocation == VideoQualityAndPlayback ) ], class "fl pl6 pr6" ] [ text "Video" ]
        , div [ classList [ ( "underline", model.modalFocusLocation == AudioTracks ) ], class "fl pr6" ] [ text "Audio Track" ]
        , div [ classList [ ( "underline", model.modalFocusLocation == Captions ) ], class "fl pr6" ] [ text "Captions" ]
        , div [ classList [ ( "underline", model.modalFocusLocation == Styling ) ], class "fl " ] [ text "Caption Styling" ]
        ]


viewModalBody : VideoModel -> Html VideoElementMsg
viewModalBody model =
    case model.modalFocusLocation of
        VideoQualityAndPlayback ->
            viewVideoQualityAndPlayback model

        AudioTracks ->
            div [] [ text "AudioTracks (couldn't find)" ]

        Captions ->
            div []
                [ div [] [ text "Captions" ]
                , viewCaptionSources model.captionSource
                ]

        Styling ->
            viewCaptionStyling model


viewCaptionSources sources =
    let
        elements =
            List.map viewCaptionSource sources
    in
    div [] elements


viewCaptionSource : ClosedCaption -> Html VideoElementMsg
viewCaptionSource caption =
    div [] [ div [] [ text caption.url ], div [] [ text caption.profile ] ]


viewVideoQualityAndPlayback : VideoModel -> Html VideoElementMsg
viewVideoQualityAndPlayback model =
    div [ class "" ]
        [ div [ class "fl w-100" ] [ viewVideoQualityAndPlaybackControls model (model.focusLocation == OptionsModalBody) ]
        ]


viewVideoQualityAndPlaybackControls : VideoModel -> Bool -> Html VideoElementMsg
viewVideoQualityAndPlaybackControls model isActive =
    div [ class "" ]
        [ div [ class "fl w-50" ]
            [ div [ classList [ ( "underline", isActive && model.videoQualityAndPlaybackFocusLocation == Quality ) ] ] [ text "Quality" ]
            , div [ classList [ ( "underline", isActive && model.videoQualityAndPlaybackFocusLocation == Speed ) ] ] [ text "Speed" ]
            ]
        , div [ class "fl w-50 nowrap" ]
            [ div [ class "" ] [ text (model.levels |> current |> .attrs |> .resolution) ]
            , div [ class "" ] [ model.playbackSpeed |> current |> fromFloat |> text ]
            ]
        ]


viewCaptionStyling : VideoModel -> Html VideoElementMsg
viewCaptionStyling model =
    div [ class "" ]
        [ div [ class "fl w-50" ] [ viewCaptionStylingControls model (model.focusLocation == OptionsModalBody) ]
        , div [ class "fl w-50 tc pt6" ]
            [ viewCaptionPreview model
            ]
        ]


viewCaptionPreview : VideoModel -> Html VideoElementMsg
viewCaptionPreview model =
    div
        [ class "pa2"
        , style "opacity" (current model.captionStyles.windowOpacity)
        , style "background-color" (current model.captionStyles.windowColor)
        ]
        [ div
            [ class "pa2"
            , style "background-color" (current model.captionStyles.windowColor)
            , style "opacity" (current model.captionStyles.bgOpacity)
            ]
            [ div
                [ class "pa2"
                , style "font-size" (current model.captionStyles.fontSize)
                , style "font-family" (current model.captionStyles.fontStyle)
                , style "color" (current model.captionStyles.fgColor)
                , style "background-color" (current model.captionStyles.bgColor)
                , style "opacity" (current model.captionStyles.fgOpacity)
                ]
                [ text "Subtitles will appear like this." ]
            ]
        ]


viewCaptionStylingControls model isActive =
    div [ class "" ]
        [ div [ class "fl w-50" ]
            [ div [ classList [ ( "underline", isActive && model.captionStylingFocusLocation == FontSize ) ] ] [ text "Font Size" ]
            , div [ classList [ ( "underline", isActive && model.captionStylingFocusLocation == FontStyle ) ] ] [ text "Font Style" ]
            , div [ classList [ ( "underline", isActive && model.captionStylingFocusLocation == FgColor ) ] ] [ text "Foreground Color" ]
            , div [ classList [ ( "underline", isActive && model.captionStylingFocusLocation == BgColor ) ] ] [ text "Background Color" ]
            , div [ classList [ ( "underline", isActive && model.captionStylingFocusLocation == WindowColor ) ] ] [ text "Window Color" ]
            , div [ classList [ ( "underline", isActive && model.captionStylingFocusLocation == FgOpacity ) ] ] [ text "Foreground Opacity" ]
            , div [ classList [ ( "underline", isActive && model.captionStylingFocusLocation == BgOpacity ) ] ] [ text "Background Opacity" ]
            , div [ classList [ ( "underline", isActive && model.captionStylingFocusLocation == WindowOpacity ) ] ] [ text "Window Opacity" ]
            , div [ classList [ ( "underline", isActive && model.captionStylingFocusLocation == TextShadow ) ] ] [ text "Text Shadow" ]
            ]
        , div [ class "fl w-50 nowrap" ]
            [ div [ class "" ] [ text (current model.captionStyles.fontSize) ]
            , div [ class "" ] [ text (current model.captionStyles.fontStyle) ]
            , div [ class "" ] [ text (current model.captionStyles.fgColor) ]
            , div [ class "" ] [ text (current model.captionStyles.bgColor) ]
            , div [ class "" ] [ text (current model.captionStyles.windowColor) ]
            , div [ class "" ] [ text (current model.captionStyles.fgOpacity) ]
            , div [ class "" ] [ text (current model.captionStyles.bgOpacity) ]
            , div [ class "" ] [ text (current model.captionStyles.windowOpacity) ]
            , div [ class "" ] [ text (current model.captionStyles.textShadow) ]
            ]
        ]


viewOverlay : VideoModel -> Html VideoElementMsg
viewOverlay model =
    div [ class "pa5", classList [ ( "videoOverlayUiTransition", Bounce.steady model.bounce && model.playerState /= Paused ) ] ]
        [ div [ class "" ]
            [ div [ class "fl w-50" ]
                [ div [ class "f1" ]
                    [ text model.videoTitle ]
                , div [ class "f2" ]
                    [ text model.videoDescription ]
                ]
            , div [ style "padding-top" "800px", class "absolute pr5" ]
                [ div [ class "" ]
                    [ div [ class "fl w-100" ]
                        [ viewProgressBar model.position model.duration
                        ]
                    , div [ class "fl w-100 mt3 f3" ]
                        [ div [ class "fl " ]
                            [ div [] [ text <| formatDuration (round model.position) ]
                            ]
                        , div [ class " fr" ]
                            [ div [] [ text <| formatDuration (round model.duration) ]
                            ]
                        ]
                    , div [ class "fl w-100 mt3 f3" ]
                        [ div [ style "padding-left" "300px", class "fl", classList [ ( "underline", model.focusLocation == RestartButton ) ] ]
                            [ viewRestartButton
                            , div [] [ text "restart" ]
                            ]
                        , div [ style "padding-left" "300px", class "fl", classList [ ( "underline", model.focusLocation == PlayPauseButton ) ] ]
                            [ if model.playerState == Playing then
                                viewPauseButton

                              else
                                viewPlayButton
                            , div [] [ text "play/pause" ]
                            ]
                        , div [ style "padding-left" "300px", class "fl", classList [ ( "underline", model.focusLocation == NextVideoButton ) ] ]
                            [ viewNextButton
                            , div [] [ text "Next Video" ]
                            ]
                        , div [ style "padding-left" "300px", class "fl", classList [ ( "underline", model.focusLocation == OptionsButton ) ] ]
                            [ viewOptionsButton
                            , div [] [ text "Options" ]
                            ]
                        ]
                    ]
                ]
            ]
        ]


viewRestartButton =
    getIcon "restart" [ width "50" ]


viewOptionsButton =
    getIcon "settings" [ width "50" ]


viewNextButton =
    getIcon "next" [ width "50" ]


viewPlayButton =
    getIcon "play" [ width "50" ]


viewPauseButton =
    getIcon "pause" [ width "50" ]


viewProgressBar : Float -> Float -> Html msg
viewProgressBar position duration =
    div [ style "width" "1760px", class "relative dib top5 bg-white-50", style "" "" ]
        [ viewProgressBarInner position duration
        ]


viewProgressBarInner : Float -> Float -> Html msg
viewProgressBarInner position duration =
    div
        [ class "theme-bg-coral pt10"
        , style "width" (calculateProgressbarString2 position duration)
        ]
        []
