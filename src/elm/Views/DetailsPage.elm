module Views.DetailsPage exposing (viewStackPage)

import Data.DetailsPage exposing (DetailsPage(..))
import Data.Row exposing (RowHolder)
import Html exposing (Html, div, text)
import Html.Attributes exposing (class)
import List.Zipper exposing (Zipper)
import Views.Other exposing (viewBeforeAfterSelected)
import Views.Row exposing (viewContentRow)
import Views.VideoDetails exposing (viewVideoDetailsPage)


viewStackPage : DetailsPage -> Html msg
viewStackPage stackType =
    case stackType of
        VideoDetailsPage x ->
            div []
                [ viewVideoDetailsPage x
                ]

        ShowDetailsPage x ->
            div []
                [ viewShowDetailsPageTitle x.resource.title
                , viewShowDetailsPageDescription x.resource.descriptionLong
                , viewShowDetailsPageSlug x.resource.slug
                , viewShowDetailsPageRows x.rows True
                ]


viewShowDetailsPageTitle : String -> Html msg
viewShowDetailsPageTitle input =
    div [] [ text input ]


viewShowDetailsPageDescription : String -> Html msg
viewShowDetailsPageDescription input =
    div [] [ text input ]


viewShowDetailsPageSlug : String -> Html msg
viewShowDetailsPageSlug input =
    div [] [ text input ]


viewShowDetailsPageRows : Zipper RowHolder -> Bool -> Html msg
viewShowDetailsPageRows input isActive =
    div [ class "fl w-100 pl6 pt4 z-2 relative" ]
        (viewBeforeAfterSelected input isActive viewContentRow)
