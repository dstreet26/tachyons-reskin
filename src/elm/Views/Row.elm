module Views.Row exposing (viewContentRow, viewHeroButton, viewHeroInfo, viewMultipleItemRow, viewShowItemRow, viewVideoItemRow)

import Data.ContentResponse exposing (MultipleContent(..), ShowContent, VideoContent)
import Data.Row exposing (ButtonItem, HeroButtonItem(..), Row(..), RowHolder)
import Html exposing (Html, div, span, text)
import Html.Attributes exposing (class, classList, style)
import List.Zipper exposing (Zipper)
import String.Extra exposing (softEllipsis)
import Svg.Attributes exposing (width)
import Views.ContentResponse exposing (formatVideoLabel, getTitleFromParent, viewParentAbove)
import Views.Other exposing (getIcon, viewBeforeAfterSelected, viewCroppedImage)


viewFeaturedVideoRow : Zipper VideoContent -> Bool -> Html msg
viewFeaturedVideoRow featuredVideos isActive =
    div [ class "mt4 wct" ]
        (viewBeforeAfterSelected featuredVideos isActive viewFeaturedVideoItem)


viewFeaturedVideoItem : Bool -> VideoContent -> Html msg
viewFeaturedVideoItem isActive fvi =
    div [ class "dib heroImageDimensions  mr2", classList [ ( "bw2", isActive ), ( "ba", isActive ), ( "customPadding4", not isActive ), ( "bn", not isActive ) ] ]
        [ viewCroppedImage fvi.imageUrl ( 263, 154 ) ]


viewHeroButton : Bool -> HeroButtonItem -> Html msg
viewHeroButton isActive button =
    let
        ( buttonText, icon ) =
            case button of
                Play b ->
                    ( b.text, b.icon )

                Add b ->
                    ( b.text, b.icon )
    in
    div
        [ class "dib br-pill  white pt3 pb3 ph4 mr4"
        , classList
            [ ( "theme-bg-button-watch-blue", isActive )
            , ( "bw1", not isActive )
            , ( "b--white", not isActive )
            , ( "ba", not isActive )
            ]
        ]
        [ getIcon icon [ width "20" ]
        , div [ class "di b ml3" ]
            [ text buttonText ]
        ]


viewHeroButtonRow : Zipper HeroButtonItem -> Bool -> Html msg
viewHeroButtonRow heroButtons isActive =
    div [ class "mb6 f30 wct" ]
        (viewBeforeAfterSelected heroButtons isActive viewHeroButton)


viewBrowseButtonRow : Zipper ButtonItem -> Bool -> Html msg
viewBrowseButtonRow buttonItems isActive =
    div [ class "" ]
        (viewBeforeAfterSelected buttonItems isActive viewBrowseButton)


viewBrowseButton : Bool -> ButtonItem -> Html msg
viewBrowseButton isActive item =
    div [ class "dib pv3 ph4 b theme-bg-button-category-blue mh3 br3 wct", classList [ ( "bw2", isActive ), ( "teal", isActive ) ] ]
        [ text item.title ]


viewHeroInfo : VideoContent -> Html msg
viewHeroInfo selected =
    div []
        [ div [ class "ttu f26 light-gray mb3" ]
            [ text <| viewHeroAbove selected ]
        , div [ class "f1 b f64 mb3" ]
            [ text <| softEllipsis 50 <| getTitleFromParent selected.parent ]
        , div [ class "lh133 f30 mb2" ]
            [ text <| softEllipsis 50 selected.title ]
        , div [ class "lh133 f30  mb4" ]
            [ viewHeroVideoLabel selected ]
        ]


viewHeroVideoLabel : VideoContent -> Html msg
viewHeroVideoLabel videoItem =
    span []
        [ text <| formatVideoLabel videoItem ]


viewHeroAbove : VideoContent -> String
viewHeroAbove videoItem =
    if videoItem.flags.isNew then
        "new episode"

    else
        viewParentAbove videoItem.parent


getSpringValue : RowHolder -> String
getSpringValue rowHolder =
    case rowHolder.horizontalScrollPosition of
        Nothing ->
            "0"

        Just x ->
            x |> negate |> String.fromInt


viewContentRow : Bool -> RowHolder -> Html msg
viewContentRow isActive rowHolder =
    case rowHolder.row of
        HeroButtonRow x ->
            viewHeroButtonRow x isActive

        SecondaryButtonRow _ ->
            -- viewSecondaryRow x isActive
            -- FIXME:
            div [] []

        FeaturedVideoRow x ->
            viewFeaturedVideoRow x isActive

        BrowseButtonRow x ->
            div [ class "w-100 fl f26 mt5 mb4 pt4" ]
                [ div [ class " mr3 light-gray fl pt3 b" ]
                    [ text (Maybe.withDefault "" rowHolder.title) ]
                , viewBrowseButtonRow x isActive
                ]

        VideoItemRow x ->
            div [ class "mt4 w-100 fl transition1", style "transform" ("translateX(" ++ getSpringValue rowHolder ++ "px)") ]
                [ div [ class "f32 b mb3" ]
                    [ text (Maybe.withDefault "" rowHolder.title) ]
                , div [ class "f26 nowrap" ]
                    (viewVideoItemRow x isActive)
                ]

        ShowItemRow x ->
            div [ class "mt4 w-100 fl transition1", style "transform" ("translateX(" ++ getSpringValue rowHolder ++ "px)") ]
                [ div [ class "f32 b mb3" ]
                    [ text (Maybe.withDefault "" rowHolder.title) ]
                , div [ class "f26 nowrap" ]
                    (viewShowItemRow x isActive)
                ]

        MultipleItemRow x ->
            div [ class "mt4 w-100 fl" ]
                [ div [ class "f32 b mb3" ]
                    [ text (Maybe.withDefault "" rowHolder.title) ]
                , div [ class "f26 nowrap" ]
                    (viewMultipleItemRow x isActive)
                ]


viewVideoItemRow : Zipper VideoContent -> Bool -> List (Html msg)
viewVideoItemRow videoItems isActive =
    viewBeforeAfterSelected videoItems isActive viewVideoItem


viewVideoItem : Bool -> VideoContent -> Html msg
viewVideoItem isActive videoItem =
    div [ class "dib" ]
        [ div [ class "carouselVideoItemDimensions  mr4 mb2", classList [ ( "bw2", isActive ), ( "ba", isActive ), ( "customPadding4", not isActive ), ( "bn", not isActive ) ] ]
            [ viewCroppedImage videoItem.imageUrl ( 500, 281 ) ]
        , div [ class "theme-video-show-gray b ttu lh154" ]
            [ text <| softEllipsis 35 <| getTitleFromParent videoItem.parent ]
        , div [ class "f30 lh133" ]
            [ text <| softEllipsis 39 videoItem.title ]
        , viewVideoLabel videoItem
        ]


viewVideoLabel : VideoContent -> Html msg
viewVideoLabel videoItem =
    div [ class "theme-video-label-gray lh154" ]
        [ text <| formatVideoLabel videoItem ]


viewShowItemRow : Zipper ShowContent -> Bool -> List (Html msg)
viewShowItemRow showItems isActive =
    viewBeforeAfterSelected showItems isActive viewShowItem


viewShowItem : Bool -> ShowContent -> Html msg
viewShowItem isActive showItem =
    div [ class "dib" ]
        [ div
            [ class "carouselShowItemDimensions bg-gray mr4 mb2 fl"
            , classList
                [ ( "bw2", isActive )
                , ( "ba", isActive )
                ]
            ]
            [ viewCroppedImage showItem.imageUrl ( 300, 450 ) ]
        ]


viewMultipleItemRow : Zipper MultipleContent -> Bool -> List (Html msg)
viewMultipleItemRow items isActive =
    viewBeforeAfterSelected items isActive viewMultipleItem


viewMultipleItem : Bool -> MultipleContent -> Html msg
viewMultipleItem isActive mContent =
    case mContent of
        VideoM x ->
            viewVideoItem isActive x

        ShowM x ->
            viewShowItem isActive x
