module Views.Sidebar exposing (viewSidebar)

import Data.FocusLocation exposing (FocusLocation(..))
import Data.MainView exposing (MainViewModel, MainViewSidebarItem, Page(..))
import Html exposing (Html, div, text)
import Html.Attributes exposing (class, classList)
import Svg.Attributes exposing (height, width)
import Views.Other exposing (getIcon)


viewSidebar : MainViewModel -> Html msg
viewSidebar mainViewModel =
    if mainViewModel.focusLocation == Sidebar then
        viewSidebarOpen mainViewModel

    else
        viewSidebarClosed mainViewModel


viewSidebarOpen : MainViewModel -> Html msg
viewSidebarOpen mainViewModel =
    div [ class "fixed theme-bg-pbs-blue f32 forcer-y dt br b--white-20 bw3 sidebarOpenDimensions z-3" ]
        [ div [ class "dtc v-mid" ]
            (viewSidebarItems mainViewModel True viewSidebarTitleOpen)
        ]


viewSidebarTitleOpen : Bool -> MainViewSidebarItem -> Html msg
viewSidebarTitleOpen isActive page =
    div
        [ class "pv4 pl4"
        , classList
            [ ( "b--theme-teal", isActive )
            , ( "br", isActive )
            , ( "bw3", isActive )
            , ( "mr-6", isActive )
            , ( "pr5", not isActive )
            ]
        ]
        [ getIcon page.iconUrl [ height "30", width "30" ]
        , div
            [ class "di v-mid pl5 "
            , classList
                [ ( "white-80", not isActive )
                , ( "b", isActive )
                ]
            ]
            [ text page.title ]
        ]


viewSidebarClosed : MainViewModel -> Html msg
viewSidebarClosed mainViewModel =
    div [ class "fixed f3 forcer-y dt br b--white-20 bw3 z-3" ]
        [ div [ class "dtc v-mid" ]
            (viewSidebarItems mainViewModel True viewSidebarTitleClosed)
        ]


viewSidebarItems : MainViewModel -> Bool -> (Bool -> MainViewSidebarItem -> Html msg) -> List (Html msg)
viewSidebarItems model isActive viewFunction =
    [ viewFunction (isActive && model.currentPage == Home) { title = "Home", iconUrl = "home" }
    , viewFunction (isActive && model.currentPage == Shows) { title = "Shows", iconUrl = "layers" }
    , viewFunction (isActive && model.currentPage == Search) { title = "Search", iconUrl = "search" }
    , viewFunction (isActive && model.currentPage == MyList) { title = "My List", iconUrl = "check" }
    , viewFunction (isActive && model.currentPage == MyStation) { title = "My Station", iconUrl = "map-pin" }
    , viewFunction (isActive && model.currentPage == Profile) { title = "Profile", iconUrl = "user" }
    , viewFunction (isActive && model.currentPage == Cats) { title = "Cats", iconUrl = "star" }
    ]


viewSidebarTitleClosed : Bool -> MainViewSidebarItem -> Html msg
viewSidebarTitleClosed isActive page =
    div
        [ class "pv4 pl3 pr3"
        , classList
            [ ( "b--theme-teal", isActive )
            , ( "br", isActive )
            , ( "bw3", isActive )
            , ( "mr-6", isActive )
            ]
        ]
        [ getIcon page.iconUrl [ height "30", width "30" ]
        ]
