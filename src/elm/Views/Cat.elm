module Views.Cat exposing (viewCat)

import Data.FocusLocation exposing (FocusLocation(..))
import Html exposing (Html, div, text)
import Html.Attributes exposing (class, classList)
import RemoteData exposing (WebData)
import Views.Other exposing (viewImage)


viewCat : WebData String -> FocusLocation -> Html msg
viewCat loadingState focusLocation =
    let
        isActive =
            focusLocation == PageContent
    in
    case loadingState of
        RemoteData.Failure _ ->
            catFailure isActive

        RemoteData.Loading ->
            viewWrapper
                [ viewButton isActive
                , div [ class "h5 f1 pt5 bg-black white" ] [ text "Loading..." ]
                ]

        RemoteData.NotAsked ->
            viewWrapper
                [ viewButton isActive
                , div [ class "h5 f1 pt5 bg-black white" ] [ text "Not Asked" ]
                ]

        RemoteData.Success url ->
            viewCatWithButton url isActive


viewWrapper : List (Html msg) -> Html msg
viewWrapper =
    div [ class "w-50 center pt4 tc" ]


viewButton : Bool -> Html msg
viewButton isActive =
    div
        [ class "bg-black-50 white f2 pa3 mv3"
        , classList
            [ ( "underline", isActive )
            , ( "settingsCardActive", isActive )
            , ( "settingsCard", not isActive )
            ]
        ]
        [ text "More Cats" ]


catFailure : Bool -> Html msg
catFailure isActive =
    viewWrapper
        [ div
            [ class "bg-black-50 white f2 pa4 mv4 "
            ]
            [ text "Could Not load a cat, maybe the API key expired..." ]
        , div
            [ class "bg-black-50 white f2 pa3 mv3"
            , classList [ ( "underline", isActive ), ( "settingsCardActive", isActive ), ( "settingsCard", not isActive ) ]
            ]
            [ text "Try Again" ]
        ]


viewCatWithButton : String -> Bool -> Html msg
viewCatWithButton url isActive =
    viewWrapper
        [ div [] [ text url ]
        , viewButton isActive
        , viewImage url
        ]
