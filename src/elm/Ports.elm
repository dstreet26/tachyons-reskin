port module Ports exposing (manifestParsed, mediaAttached, setDeviceId, videoEventStream)

import Json.Encode exposing (Value)


port setDeviceId : String -> Cmd msg


port videoEventStream : Value -> Cmd msg


port mediaAttached : (() -> msg) -> Sub msg


port manifestParsed : (Value -> msg) -> Sub msg
