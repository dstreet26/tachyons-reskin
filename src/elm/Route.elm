module Route exposing (Route(..), route)

import Url.Parser exposing ((</>), Parser, map, oneOf, s, string)


type Route
    = HomeRoute
    | ShowsRoute
    | MyListRoute
    | ShowDetails String
    | VideoDetails String
    | VideoPlayback String


route : Parser (Route -> a) a
route =
    oneOf
        [ map HomeRoute (s "home")
        , map ShowsRoute (s "shows")
        , map MyListRoute (s "my-list")
        , map ShowDetails (s "show-details" </> string)
        , map VideoDetails (s "video-details" </> string)
        , map VideoPlayback (s "video-playback" </> string)
        ]
