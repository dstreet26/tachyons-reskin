module Utils exposing (calculateProgressWidth, calculateProgressbarString, calculateProgressbarString2, formatDuration, intFromMonth, navigateRowHolder, navigateRowHolderZ, tryZipN)

import Data.Main exposing (PlaceholderDirection(..))
import Data.Row exposing (Row(..), RowHolder)
import List.Zipper exposing (Zipper, before, current, mapCurrent, next, previous)
import String exposing (fromInt)
import Time exposing (Month(..))


intFromMonth : Month -> Int
intFromMonth m =
    case m of
        Jan ->
            1

        Feb ->
            2

        Mar ->
            3

        Apr ->
            4

        May ->
            5

        Jun ->
            6

        Jul ->
            7

        Aug ->
            8

        Sep ->
            9

        Oct ->
            10

        Nov ->
            11

        Dec ->
            12


navigateRowHolderZ : Zipper RowHolder -> PlaceholderDirection -> ( Zipper RowHolder, Bool )
navigateRowHolderZ rowHolders direction =
    let
        currentRowHolder =
            current rowHolders

        ( newRowHolder, limitHit ) =
            navigateRowHolder currentRowHolder direction

        newRowHolders =
            mapCurrent
                (\_ ->
                    newRowHolder
                )
                rowHolders
    in
    ( newRowHolders, limitHit )



-- TODO: combine this with css values (elm-css)


videoItemWidth =
    500


showItemWidth =
    300


navigateRowHolder : RowHolder -> PlaceholderDirection -> ( RowHolder, Bool )
navigateRowHolder rowHolder direction =
    case rowHolder.row of
        HeroButtonRow x ->
            let
                ( newZipper, limitHit ) =
                    tryZipN 1 direction x
            in
            ( { rowHolder | row = HeroButtonRow newZipper }, limitHit )

        SecondaryButtonRow x ->
            let
                ( newZipper, limitHit ) =
                    tryZipN 1 direction x
            in
            ( { rowHolder | row = SecondaryButtonRow newZipper }, limitHit )

        FeaturedVideoRow x ->
            let
                ( newZipper, limitHit ) =
                    tryZipN 1 direction x
            in
            ( { rowHolder | row = FeaturedVideoRow newZipper }, limitHit )

        BrowseButtonRow x ->
            let
                ( newZipper, limitHit ) =
                    tryZipN 1 direction x
            in
            ( { rowHolder | row = BrowseButtonRow newZipper }, limitHit )

        VideoItemRow x ->
            let
                ( newZipper, limitHit ) =
                    tryZipN 1 direction x

                countLeft =
                    newZipper |> before |> List.length

                value =
                    countLeft * videoItemWidth
            in
            ( { rowHolder
                | row = VideoItemRow newZipper
                , horizontalScrollPosition = Just value
              }
            , limitHit
            )

        ShowItemRow x ->
            let
                ( newZipper, limitHit ) =
                    tryZipN 1 direction x

                countLeft =
                    newZipper |> before |> List.length

                value =
                    countLeft * showItemWidth
            in
            ( { rowHolder | row = ShowItemRow newZipper, horizontalScrollPosition = Just value }, limitHit )

        MultipleItemRow x ->
            let
                ( newZipper, limitHit ) =
                    tryZipN 1 direction x
            in
            ( { rowHolder | row = MultipleItemRow newZipper }, limitHit )


tryZipN : Int -> PlaceholderDirection -> Zipper a -> ( Zipper a, Bool )
tryZipN n direction input =
    if n < 1 then
        ( input, False )

    else
        case direction of
            Left ->
                case previous input of
                    Nothing ->
                        ( input, True )

                    Just x ->
                        tryZipN (n - 1) Left x

            Right ->
                case next input of
                    Nothing ->
                        ( input, True )

                    Just x ->
                        tryZipN (n - 1) Right x


calculateProgressWidth : Int -> Int -> Int
calculateProgressWidth completed total =
    round (100 * (toFloat completed / toFloat total))


calculateProgressbarString : Int -> Int -> String
calculateProgressbarString completed total =
    fromInt (calculateProgressWidth completed total) ++ "%"


calculateProgressWidth2 : Float -> Float -> Float
calculateProgressWidth2 completed total =
    100 * (completed / total)


calculateProgressbarString2 : Float -> Float -> String
calculateProgressbarString2 completed total =
    String.fromFloat (calculateProgressWidth2 completed total) ++ "%"


formatDuration : Int -> String
formatDuration duration =
    let
        hours =
            duration // 3600

        minutesRem =
            remainderBy 3600 duration

        minutes =
            minutesRem // 60

        secondsRem =
            remainderBy 60 minutesRem
    in
    if hours < 1 then
        if minutes < 1 then
            fromInt secondsRem ++ "s"

        else
            fromInt minutes ++ "m " ++ fromInt secondsRem ++ "s"

    else
        fromInt hours ++ "h " ++ fromInt minutes ++ "m " ++ fromInt secondsRem ++ "s"
